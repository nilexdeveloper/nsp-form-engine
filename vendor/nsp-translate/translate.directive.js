angular.module('formEngine').directive('nspT', ['feService', function (feService) {
  return {
    restrict: 'AE',
    scope: false,
    link: function (scope, element, attrs) {
      if (!scope.t) {
        scope.t = feService.translate;
      }
    }
  };
}]);