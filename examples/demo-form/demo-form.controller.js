(function() {
    'use strict';

    angular
        .module('demo')
        .controller('DemoFormController', DemoFormController);

    DemoFormController.$inject = ['$scope'];

    /* @ngInject */
    function DemoFormController($scope) {
        var vm = this;
        vm.title = 'DemoFormControllerTest';
        vm.schema = {
            type: "object",
            properties: {
                "text": {
                    "type": "string"
                },
                "number": {
                    "type": "number"
                },
                "textarea": {
                    "type": "string"
                },
                "password": {
                    "type": "string"
                },
                "checkboxes": {
                    "type": "array",
                    "default": [
                        "guitar"
                    ],
                    "items": {
                        "type": "string",
                        "enum": [
                            "guitar",
                            "bassGuitar",
                            "drums"
                        ]
                    }
                },
                "checkbox": {
                    "type": "boolean"
                },
                "radios": {
                    "type": "string",
                    "default": [
                        "medium"
                    ],
                    "items": {
                        "type": "string",
                        "enum": [
                            "low",
                            "medium",
                            "high"
                        ]
                    }
                },
                "select": {
                    "type": "string",
                    "items": {
                        "type": "string",
                        "enum": [
                            "icecream",
                            "chocolate",
                            "lollipop"
                        ]
                    }
                },
                "base_858_enduserid_939": {
                    "type": "object",
                    "nspType": "NSPKendoAutoCompleteProperty",
                    "dbType": "string",
                    "id": 858,
                    "isCustomProperty": false,
                    "tableStorageDetailsName": "[dbo].[SYS_DEF_COLUMN]",
                    "tableRepositoryName": "SYS_DEF_TABLE",
                    "isReference": true,
                    "referenceTableId": 111,
                    "fieldName": "EndUserId",
                    "machineName": "EndUserId",
                    "groupingIdentification": 112,
                    "fieldStorageName": "SYS_DEF_COLUMN",
                    "isEavProperty": false,
                    "generateInRuntime": false,
                    "isTranslatableValue": false,
                    "dataValueField": "Id"
                },
                "base_117_productid_115541": {
                    "type": "object",
                    "nspType": "NSPKendoComboBoxProperty",
                    "dbType": "int",
                    "id": 117,
                    "isCustomProperty": false,
                    "tableStorageDetailsName": "app.Columns",
                    "tableRepositoryName": "app.Tables",
                    "isReference": true,
                    "referenceTableId": 17,
                    "fieldName": "ProductId",
                    "machineName": "ProductId",
                    "fieldStorageName": "app.Columns",
                    "isEavProperty": false,
                    "generateInRuntime": false,
                    "isTranslatableValue": false,
                    "dataValueField": "Id"
                },
                "base_118_vendorid_115541": {
                    "type": "object",
                    "nspType": "NSPKendoAutoCompleteProperty",
                    "dbType": "int",
                    "id": 118,
                    "isCustomProperty": false,
                    "tableStorageDetailsName": "app.Columns",
                    "tableRepositoryName": "app.Tables",
                    "isReference": true,
                    "referenceTableId": 18,
                    "fieldName": "VendorId",
                    "machineName": "VendorId",
                    "fieldStorageName": "app.Columns",
                    "isEavProperty": false,
                    "generateInRuntime": false,
                    "isTranslatableValue": false,
                    "dataValueField": "Id"
                },
                "base_113_departmentid_115541": {
                    "type": "object",
                    "nspType": "NSPKendoDropDownListProperty",
                    "dbType": "int",
                    "id": 113,
                    "isCustomProperty": false,
                    "tableStorageDetailsName": "app.Columns",
                    "tableRepositoryName": "app.Tables",
                    "isReference": true,
                    "referenceTableId": 11,
                    "fieldName": "DepartmentId",
                    "machineName": "DepartmentId",
                    "fieldStorageName": "app.Columns",
                    "isEavProperty": false,
                    "generateInRuntime": false,
                    "isTranslatableValue": false,
                    "dataValueField": "Id"
                },
                "purchase-date": {
                    "type": "object",
                    "nspType": "NSPKendoDatePickerProperty",
                    "dbType": "DateTime",
                    "id": 214,
                    "isCustomProperty": false,
                    "tableStorageDetailsName": "cmdb.CiTypeProperties",
                    "tableRepositoryName": "app.Tables",
                    "isReference": false,
                    "fieldName": "Purchase date",
                    "machineName": "Purchase date",
                    "entityTypeId": 4168,
                    "fieldStorageName": "cmdb.CiTypeProperties",
                    "isEavProperty": true,
                    "eavStorageValues": "cmdb.CiPropertyValues",
                    "generateInRuntime": true,
                    "isTranslatableValue": false,
                    "translatablePropertyName": "ValueTranslateKey",
                    "translatableEnableInJs": true,
                    "translatableSystemKey": false
                },
                "purchase-time": {
                    "type": "object",
                    "nspType": "NSPKendoDatePickerProperty",
                    "dbType": "DateTime",
                    "id": 214,
                    "isCustomProperty": false,
                    "tableStorageDetailsName": "cmdb.CiTypeProperties",
                    "tableRepositoryName": "app.Tables",
                    "isReference": false,
                    "fieldName": "Purchase date",
                    "machineName": "Purchase date",
                    "entityTypeId": 4168,
                    "fieldStorageName": "cmdb.CiTypeProperties",
                    "isEavProperty": true,
                    "eavStorageValues": "cmdb.CiPropertyValues",
                    "generateInRuntime": true,
                    "isTranslatableValue": false,
                    "translatablePropertyName": "ValueTranslateKey",
                    "translatableEnableInJs": true,
                    "translatableSystemKey": false
                },
                "purchase-date-time": {
                    "type": "object",
                    "nspType": "NSPKendoDatePickerProperty",
                    "dbType": "DateTime",
                    "id": 214,
                    "isCustomProperty": false,
                    "tableStorageDetailsName": "cmdb.CiTypeProperties",
                    "tableRepositoryName": "app.Tables",
                    "isReference": false,
                    "fieldName": "Purchase date",
                    "machineName": "Purchase date",
                    "entityTypeId": 4168,
                    "fieldStorageName": "cmdb.CiTypeProperties",
                    "isEavProperty": true,
                    "eavStorageValues": "cmdb.CiPropertyValues",
                    "generateInRuntime": true,
                    "isTranslatableValue": false,
                    "translatablePropertyName": "ValueTranslateKey",
                    "translatableEnableInJs": true,
                    "translatableSystemKey": false
                },
                "purchase-price": {
                    "type": "string",
                    "nspType": "NSPKendoNumericTextBoxProperty",
                    "dbType": "decimal",
                    "id": 215,
                    "isCustomProperty": false,
                    "tableStorageDetailsName": "cmdb.CiTypeProperties",
                    "tableRepositoryName": "app.Tables",
                    "isReference": false,
                    "fieldName": "Purchase price",
                    "machineName": "Purchase price",
                    "entityTypeId": 4168,
                    "fieldStorageName": "cmdb.CiTypeProperties",
                    "isEavProperty": true,
                    "eavStorageValues": "cmdb.CiPropertyValues",
                    "generateInRuntime": true,
                    "isTranslatableValue": false,
                    "translatablePropertyName": "ValueTranslateKey",
                    "translatableEnableInJs": true,
                    "translatableSystemKey": false
                },
                "description": {
                    "type": "string",
                    "nspType": "NSPKendoEditorProperty",
                    "dbType": "string",
                    "id": 109,
                    "isCustomProperty": false,
                    "tableStorageDetailsName": "app.Columns",
                    "tableRepositoryName": "app.Tables",
                    "isReference": false,
                    "fieldName": "Description",
                    "machineName": "Description",
                    "fieldStorageName": "app.Columns",
                    "isEavProperty": false,
                    "generateInRuntime": false,
                    "isTranslatableValue": false
                },
                "template-picker-test": {
                    "type": "array",
                    "items": {
                        "type": "object"
                    }
                },
                "ticketDescription": {
                    "type": "object"
                },
                "kendoImageUpload": {
                    "type": "object"
                },
                "kendoMultiSelect": {
                    "type": "array",
                    "items": {
                        "type": "object"
                    }
                }
            }
        };
        vm.form = [{
            "type": "accordion",
            "key": "accordion",
            "isTranslatedLabels": true,
            "title": "Common",
            "titleKey": "Common",
            "collapsed": false,
            "items": [{
                "nspType": "NSPKendoAutoCompleteProperty",
                "type": "kendo-auto-complete",
                "required": true,
                "key": "base_858_enduserid_939",
                "machineName": "EndUserId",
                "isTranslatedLabels": true,
                "titleKey": "Common.Requester",
                "title": "Requester",
                "width": 100,
                "placeholder": "",
                "browse": {
                    "url": "/web/browse-windows/users/fe-browse-users.html",
                    "title": "Select",
                    "titleKey": "Common.SelectUser",
                    "caption": "Browse",
                    "captionKey": "NSPFormEngine.BrowseCaptionKeyDefault",
                    "icon": "icon-search"
                },
                "kendoOptions": {
                    "filter": "contains",
                    "dataTextField": "Text",
                    "dataValueField": "Id",
                    "autoBind": false,
                    "dataSource": {
                        "serverFiltering": true,
                        "transport": {
                            "read": {
                                "url": "http://localhost:1800/NSPFormEngine/NSPDynamicDataSource?referenceTableId=111&tableRepositoryName=SYS_DEF_TABLE&numOfRows=20",
                                "dataType": "json"
                            }
                        }
                    },
                    "optionLabel": "Select ...",
                    "optionLabelKey": "NSPFormEngine.OptionLabelKeyDefault"
                }
            }, {
                "key": "kendoImageUpload",
                "title": "Kendo Image Upload",
                "titleKey": "Kendo Image Upload",
                "type": "kendo-image-upload",
                "required": true,
                "width": 100
            }, {
                "key": "kendoMultiSelect",
                "title": "Kendo Multi Select",
                "titleKey": "Kendo Multi Select",
                "type": "kendo-multi-select",
                "required": true,
                "width": 100,
                "kendoOptions": {
                    "dataTextField": "name",
                    "dataValueField": "value",
                    "dataSource": {
                        "transport": {
                            "read": {
                                "url": "http://localhost:1800/TabNavigation/GetDropDownDataOfBaseMultiSelect?instanceId=0&entityTypeId=112&dataObject=CC&defaultValue=&userIds=null"
                            }
                        }
                    }   
                }
            }, /*{
                "nspType": "NSPTicketDescriptionProperty",
                "type": "ticket-description",
                "required": true,
                "key": "ticketDescription",
                "isTranslatedLabels": true,
                "titleKey": "Common.TicketDescription",
                "title": "Ticket Description",
                "width": 100,
                "placeholder": "",
                "editorOptions": {
                    "encoded": true
                },
                "attachmentsOptions": {
                    "url": "/SelfServicePortal/UploadFiles", // Upload handler
                    "paramname": "files", // POST parameter name used on serverside to reference file
                    "allowedfileextensions": ['.jpg','.jpeg','.png','.gif'], // file extensions allowed. Empty array means no restrictions
                    "maxfilesize": 5, // max file size in MBs
                    "maxfiles": 25,   // max number of files
                    "data": {         // Additional POST parameters
                        "param1": "value1"
                    }
                }
            },*/ {
                "key": "text",
                "isTranslatedLabels": true,
                "title": "Text",
                "titleKey": "Common.Text",
                "type": "text",
                "notitle": false,
                "required": true,
                "description": "Lorem ipsum sit dolorem.",
                "descriptionKey": "Common.Lorem",
                "width": 33.33,
                "feedback": true,
                // "placeholder": "Enter...",
                // "placeholderKey": "Common.Enter",
                "htmlClass": "htmlclass",
                "fieldHtmlClass": "fieldclass",
                "labelHtmlClass": "labelclass"
                    // "validationMessage": {
                    //     "emailValid": "Email not valid"
                    // },
                    // "ngModelOptions": {
                    //     "updateOn": "blur"
                    // },
                    // "$validators": {
                    //     emailValid: function(value) {
                    //         var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    //         return value ? re.test(value) : true;
                    //     }
                    // }
            }, {
                "key": "number",
                "isTranslatedLabels": true,
                "title": "Number",
                "titleKey": "Common.Number",
                "type": "number",
                "required": true,
                "width": 33.33,
                "condition": "model.text"
            }, {
                "key": "password",
                "isTranslatedLabels": true,
                "title": "Password",
                "titleKey": "Common.Password",
                "type": "password",
                "required": true,
                "width": 33.33
            }, {
                "key": "textarea",
                "isTranslatedLabels": true,
                "title": "Textarea",
                "titleKey": "Textarea",
                "type": "textarea",
                "required": true,
                "width": 100
            }, {
                "key": "checkboxes",
                "isTranslatedLabels": true,
                "type": "checkboxes",
                "title": "Checkboxes",
                "titleKey": "Common.Checkboxes",
                "required": true,
                "titleMap": [{
                    "value": "guitar",
                    "title": "Guitar",
                    "titleKey": "Common.Guitar"
                }, {
                    "value": "bassGuitar",
                    "title": "Bass Guitar",
                    "titleKey": "Common.Bass Guitar"
                }, {
                    "value": "drums",
                    "title": "Drums",
                    "titleKey": "Common.Drums"
                }]
            }, {
                "key": "checkbox",
                "isTranslatedLabels": true,
                "type": "checkbox",
                "title": "Checkbox",
                "titleKey": "Common.Checkbox"
            }, {
                "key": "radios",
                "isTranslatedLabels": true,
                "type": "radios",
                "title": "Radios",
                "titleKey": "Common.Radios",
                "required": true,
                "titleMap": [{
                    "value": "low",
                    "title": "Low",
                    "titleKey": "Common.Low"
                }, {
                    "value": "medium",
                    "title": "Medium",
                    "titleKey": "Common.Medium"
                }, {
                    "value": "high",
                    "title": "high",
                    "titleKey": "Common.high"
                }]
            }, {
                "key": "select",
                "isTranslatedLabels": true,
                "type": "select",
                "title": "Select",
                "titleKey": "Common.Select",
                "required": true,
                "titleMap": [{
                    "value": "icecream",
                    "name": "Ice cream"
                }, {
                    "value": "chocolate",
                    "name": "Chocolate"
                }, {
                    "value": "lollipop",
                    "name": "Lollipop"
                }]
            }, {
                "type": "help",
                "title": "Select",
                "titleKey": "Common.Select",
                "encoded": false,
                "helpvalue": '&lt;a href="#"&gt;test&lt;/a&gt;'
            }]
        }, {
            "type": "accordion",
            "key": "accordion",
            "isTranslatedLabels": true,
            "title": "Kendo",
            "titleKey": "Kendo",
            "collapsed": false,
            "items": [{
                "nspType": "NSPKendoEditorProperty",
                "type": "kendo-editor",
                "required": false,
                "key": "description",
                "isTranslatedLabels": true,
                "titleKey": "Columns.Description",
                "title": "Description",
                "width": 100,
                "placeholder": "",
                "kendoOptions": {
                    "encoded": true
                }
            }, {
                "nspType": "NSPKendoComboBoxProperty",
                "type": "kendo-combo-box",
                "required": false,
                "key": "base_117_productid_115541",
                "isTranslatedLabels": true,
                "titleKey": "Columns.ProductId",
                "title": "Product",
                "width": 50,
                // "placeholder": "kendo-combo-box",
                // "placeholderKey": "kendo-combo-box-key",
                "browse": {
                    "url": "/examples/demo-form/browse.html",
                    "title": "Select",
                    "titleKey": "Common.Select",
                    "caption": "Browse",
                    "captionKey": "NSPFormEngine.BrowseCaptionKeyDefault",
                    "icon": "icon-search"
                },
                "kendoOptions": {
                    "filter": "contains",
                    "placeholder": "kendo-combo-box",
                    "placeholderKey": "kendo-combo-box",
                    "dataTextField": "Text",
                    "dataValueField": "Id",
                    "autoBind": false,
                    "dataSource": {
                        "serverFiltering": true,
                        "transport": {
                            "read": {
                                "url": "http://localhost:1800/NSPFormEngine/NSPDynamicDataSource?referenceTableId=17&tableRepositoryName=app.Tables&numOfRows=20",
                                "dataType": "json"
                            }
                        }
                    },
                    "optionLabel": "Select ...",
                    "optionLabelKey": "NSPFormEngine.OptionLabelKeyDefault"
                }
            }, {
                "nspType": "NSPKendoAutoCompleteProperty",
                "type": "kendo-auto-complete",
                "required": false,
                "key": "base_118_vendorid_115541",
                "isTranslatedLabels": true,
                "titleKey": "Columns.VendorId",
                "title": "Vendor",
                "width": 50,
                "placeholder": "kendo-auto-complete",
                "placeholderKey": "kendo-auto-complete",
                "browse": {
                    "url": "/examples/demo-form/browse.html",
                    "title": "Select",
                    "titleKey": "Common.Select",
                    "caption": "Browse",
                    "captionKey": "NSPFormEngine.BrowseCaptionKeyDefault",
                    "icon": "icon-search"
                },
                "kendoOptions": {
                    "filter": "contains",
                    "placeholder": "kendo-auto-complete",
                    "placeholderKey": "kendo-auto-complete",
                    "dataTextField": "Text",
                    "dataValueField": "Id",
                    "autoBind": false,
                    "dataSource": {
                        "serverFiltering": true,
                        "transport": {
                            "read": {
                                "url": "http://localhost:1800/NSPFormEngine/NSPDynamicDataSource?referenceTableId=18&tableRepositoryName=app.Tables&numOfRows=20",
                                "dataType": "json"
                            }
                        }
                    },
                    "optionLabel": "Select ...",
                    "optionLabelKey": "NSPFormEngine.OptionLabelKeyDefault"
                }
            }, {
                "nspType": "NSPKendoDropDownListProperty",
                "type": "kendo-drop-down-list",
                "required": true,
                "key": "base_113_departmentid_115541",
                "isTranslatedLabels": true,
                "titleKey": "Columns.DepartmentId",
                "title": "Department",
                "width": 50,
                "placeholder": "",
                "kendoOptions": {
                    "filter": "contains",
                    "placeholder": "",
                    "dataTextField": "Text",
                    "dataValueField": "Id",
                    "autoBind": false,
                    "dataSource": {
                        "serverFiltering": false,
                        "transport": {
                            "read": {
                                "url": "http://localhost:1800/NSPFormEngine/NSPDynamicLovDataSource?referenceTableId=11&tableRepositoryName=app.Tables&numOfRows=1000000",
                                "dataType": "json"
                            }
                        }
                    },
                    "optionLabel": "Select ...",
                    "optionLabelKey": "NSPFormEngine.OptionLabelKeyDefault",
                    "useEntityLifecycleCallback": false
                }
            }, {
                "nspType": "NSPKendoDatePickerProperty",
                "type": "kendo-date-picker",
                "required": true,
                "key": "purchase-date",
                "isTranslatedLabels": true,
                "titleKey": "CiTypeProperties.PurchaseDate",
                "title": "Purchase date",
                "width": 50,
                "placeholder": "",
                "kendoOptions": {
                    "format": "d"
                }
            }, {
                "nspType": "NSPKendoTimePickerProperty",
                "type": "kendo-time-picker",
                "required": true,
                "key": "purchase-time",
                "isTranslatedLabels": true,
                "titleKey": "CiTypeProperties.PurchaseTime",
                "title": "Purchase time",
                "width": 50,
                "placeholder": "",
                "kendoOptions": {
                    "format": "t"
                }
            }, {
                "nspType": "NSPKendoDateTimePickerProperty",
                "type": "kendo-date-time-picker",
                "required": true,
                "key": "purchase-date-time",
                "isTranslatedLabels": true,
                "titleKey": "CiTypeProperties.PurchaseDateTime",
                "title": "Purchase date time",
                "width": 50,
                "placeholder": "",
                "kendoOptions": {
                    "format": "g"
                }
            }, {
                "nspType": "NSPKendoNumericTextBoxProperty",
                "type": "kendo-numeric-text-box",
                "required": false,
                "key": "purchase-price",
                "isTranslatedLabels": true,
                "titleKey": "CiTypeProperties.PurchasePrice",
                "title": "Purchase price",
                "width": 50,
                "placeholder": "",
                "kendoOptions": {
                    "format": "#.##",
                    "decimals": 2,
                    "min": 0,
                    "spinners": false
                }
            }, {
                "key": "template-picker-test",
                "type": "template-picker",
                "title": "Subject",
                "titleKey": "Common.Subject",
                "required": true,
                "width": 50,
                "currentLang": 1, //will be replaced by the current lang id
                "window": {
                    "url": "/src/components/web/template-picker/fe-template-picker-form.html",
                    "placeholdersUrl": "https://api.myjson.com/bins/3yizs'",
                    "entityTypeId": 200, // replace with actual entity type id
                    "tableId": 200, // replace with actual tableId,
                    "languageListUrl": "https://api.myjson.com/bins/29cws"
                }
            }]
        }, {
            "type": "actions",
            "isTranslatedLabels": true,
            "items": [{
                "type": 'submit',
                "title": 'Ok',
                "titleKey": 'Common.Submit'
            }, {
                "key": "cancelButton",
                "isTranslatedLabels": true,
                "type": 'button',
                "title": 'Cancel',
                "titleKey": 'Common.Cancel',
                "onClick": "cancel()"
            }]
        }];

        vm.model = {
            "kendoImageUpload": {
                id: "377",
                name: "zoki.png",
                preview: "http://localhost/zoki.png"
            },
            "template-picker-test": [],
            "kendoMultiSelect": [{
                "value": 11633,
                "name": "genti test"
            }, {
                "value": 97,
                "name": "Dejan Petrusic (dejan@testexa.localaaa)"
            }]
        };
        vm.onSubmit = onSubmit;
        vm.pretty = pretty;

        function onSubmit(form) {
            $scope.$broadcast('schemaFormValidate');
            console.log(vm.model);
        }

        function pretty() {
            return typeof vm.model === 'string' ? vm.model : JSON.stringify(vm.model, undefined, 2);
        }
    }
})();