(function () {
    'use strict';

    angular.module('demo').config(config);

    /** @ngInject */
    function config($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.when('/', '/app/home');
        $urlRouterProvider.when('', '/app/home');

        $stateProvider
            .state('app', {
                url: '/app',
                abstract: true,
                template: '<ui-view/>'
            })
            .state('app.home', {
                url: '/home',
                templateUrl: "examples/home/home.html"
            })
            .state('app.demo', {
                url: '/demo',
                templateUrl: "examples/demo-form/demo-form.html",
                controller: "DemoFormController",
                controllerAs: 'vm'
            }).state('app.ci', {
                url: '/ci',
                templateUrl: "examples/demo-form/demo-form.html",
                controller: "CiFormController",
                controllerAs: 'vm'
            });
    }

})();
