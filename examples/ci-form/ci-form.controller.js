(function() {
    'use strict';

    angular
        .module('demo')
        .controller('CiFormController', CiFormController);

    CiFormController.$inject = [];

    /* @ngInject */
    function CiFormController() {
        var vm = this;

        var response = {
            "id": 6,
            "tableStorageDetailsName": "app.Columns",
            "tableRepositoryName": "app.Tables",
            "tableId": 19,
            "rowId": 0,
            "tableRepositoryId": 0,
            "title": "Form for CI",
            "titleKey": "Forms.FormForCi",
            "entityTypeId": 4168,
            "schema": {
                "type": "object",
                "tableId": 19,
                "rowId": 0,
                "tableRepositoryId": 0,
                "properties": {
                    "base_108_name_115541": {
                        "type": "string",
                        "nspType": "NSPTextProperty",
                        "dbType": "string",
                        "id": 108,
                        "isCustomProperty": false,
                        "tableStorageDetailsName": "app.Columns",
                        "tableRepositoryName": "app.Tables",
                        "isReference": false,
                        "fieldName": "Name",
                        "machineName": "Name",
                        "fieldStorageName": "app.Columns",
                        "isEavProperty": false,
                        "generateInRuntime": false,
                        "isTranslatableValue": false
                    },
                    "base_109_description_115541": {
                        "type": "string",
                        "nspType": "NSPKendoEditorProperty",
                        "dbType": "string",
                        "id": 109,
                        "isCustomProperty": false,
                        "tableStorageDetailsName": "app.Columns",
                        "tableRepositoryName": "app.Tables",
                        "isReference": false,
                        "fieldName": "Description",
                        "machineName": "Description",
                        "fieldStorageName": "app.Columns",
                        "isEavProperty": false,
                        "generateInRuntime": false,
                        "isTranslatableValue": false
                    },
                    "base_111_entitytypeid_115541": {
                        "type": "object",
                        "nspType": "NSPKendoAutoCompleteProperty",
                        "dbType": "int",
                        "id": 111,
                        "isCustomProperty": false,
                        "tableStorageDetailsName": "app.Columns",
                        "tableRepositoryName": "app.Tables",
                        "isReference": true,
                        "referenceTableId": 12,
                        "fieldName": "EntityTypeId",
                        "machineName": "EntityTypeId",
                        "fieldStorageName": "app.Columns",
                        "isEavProperty": false,
                        "generateInRuntime": false,
                        "isTranslatableValue": false,
                        "dataValueField": "Id"
                    },
                    "base_113_departmentid_115541": {
                        "type": "object",
                        "nspType": "NSPKendoDropDownListProperty",
                        "dbType": "int",
                        "id": 113,
                        "isCustomProperty": false,
                        "tableStorageDetailsName": "app.Columns",
                        "tableRepositoryName": "app.Tables",
                        "isReference": true,
                        "referenceTableId": 11,
                        "fieldName": "DepartmentId",
                        "machineName": "DepartmentId",
                        "fieldStorageName": "app.Columns",
                        "isEavProperty": false,
                        "generateInRuntime": false,
                        "isTranslatableValue": false,
                        "dataValueField": "Id"
                    },
                    "base_114_serialnumber_115541": {
                        "type": "string",
                        "nspType": "NSPTextProperty",
                        "dbType": "string",
                        "id": 114,
                        "isCustomProperty": false,
                        "tableStorageDetailsName": "app.Columns",
                        "tableRepositoryName": "app.Tables",
                        "isReference": false,
                        "fieldName": "SerialNumber",
                        "machineName": "SerialNumber",
                        "fieldStorageName": "app.Columns",
                        "isEavProperty": false,
                        "generateInRuntime": false,
                        "isTranslatableValue": false
                    },
                    "base_115_impactid_115541": {
                        "type": "object",
                        "nspType": "NSPKendoDropDownListProperty",
                        "dbType": "int",
                        "id": 115,
                        "isCustomProperty": false,
                        "tableStorageDetailsName": "app.Columns",
                        "tableRepositoryName": "app.Tables",
                        "isReference": true,
                        "referenceTableId": 1,
                        "fieldName": "ImpactId",
                        "machineName": "ImpactId",
                        "fieldStorageName": "app.Columns",
                        "isEavProperty": false,
                        "generateInRuntime": false,
                        "isTranslatableValue": false,
                        "dataValueField": "Id"
                    },
                    "base_116_cistateid_115541": {
                        "type": "object",
                        "nspType": "NSPKendoDropDownListProperty",
                        "dbType": "int",
                        "id": 116,
                        "isCustomProperty": false,
                        "tableStorageDetailsName": "app.Columns",
                        "tableRepositoryName": "app.Tables",
                        "isReference": true,
                        "referenceTableId": 1058,
                        "fieldName": "CiStateId",
                        "machineName": "CiStateId",
                        "fieldStorageName": "app.Columns",
                        "isEavProperty": false,
                        "generateInRuntime": false,
                        "isTranslatableValue": false,
                        "dataValueField": "id"
                    },
                    "base_117_productid_115541": {
                        "type": "object",
                        "nspType": "NSPKendoAutoCompleteProperty",
                        "dbType": "int",
                        "id": 117,
                        "isCustomProperty": false,
                        "tableStorageDetailsName": "app.Columns",
                        "tableRepositoryName": "app.Tables",
                        "isReference": true,
                        "referenceTableId": 17,
                        "fieldName": "ProductId",
                        "machineName": "ProductId",
                        "fieldStorageName": "app.Columns",
                        "isEavProperty": false,
                        "generateInRuntime": false,
                        "isTranslatableValue": false,
                        "dataValueField": "Id"
                    },
                    "base_118_vendorid_115541": {
                        "type": "object",
                        "nspType": "NSPKendoAutoCompleteProperty",
                        "dbType": "int",
                        "id": 118,
                        "isCustomProperty": false,
                        "tableStorageDetailsName": "app.Columns",
                        "tableRepositoryName": "app.Tables",
                        "isReference": true,
                        "referenceTableId": 18,
                        "fieldName": "VendorId",
                        "machineName": "VendorId",
                        "fieldStorageName": "app.Columns",
                        "isEavProperty": false,
                        "generateInRuntime": false,
                        "isTranslatableValue": false,
                        "dataValueField": "Id"
                    },
                    "base_120_locationid_115541": {
                        "type": "object",
                        "nspType": "NSPKendoAutoCompleteProperty",
                        "dbType": "int",
                        "id": 120,
                        "isCustomProperty": false,
                        "tableStorageDetailsName": "app.Columns",
                        "tableRepositoryName": "SYS_DEF_TABLE",
                        "isReference": true,
                        "referenceTableId": 241,
                        "fieldName": "LocationId",
                        "machineName": "LocationId",
                        "fieldStorageName": "app.Columns",
                        "isEavProperty": false,
                        "generateInRuntime": false,
                        "isTranslatableValue": false,
                        "dataValueField": "Id"
                    },
                    "base_121_managedbyid_115541": {
                        "type": "object",
                        "nspType": "NSPKendoAutoCompleteProperty",
                        "dbType": "int",
                        "id": 121,
                        "isCustomProperty": false,
                        "tableStorageDetailsName": "app.Columns",
                        "tableRepositoryName": "SYS_DEF_TABLE",
                        "isReference": true,
                        "referenceTableId": 269,
                        "fieldName": "ManagedById",
                        "machineName": "ManagedById",
                        "fieldStorageName": "app.Columns",
                        "isEavProperty": false,
                        "generateInRuntime": false,
                        "isTranslatableValue": false,
                        "dataValueField": "Id"
                    },
                    "base_122_usedbyid_115541": {
                        "type": "object",
                        "nspType": "NSPKendoAutoCompleteProperty",
                        "dbType": "int",
                        "id": 122,
                        "isCustomProperty": false,
                        "tableStorageDetailsName": "app.Columns",
                        "tableRepositoryName": "SYS_DEF_TABLE",
                        "isReference": true,
                        "referenceTableId": 269,
                        "fieldName": "UsedById",
                        "machineName": "UsedById",
                        "fieldStorageName": "app.Columns",
                        "isEavProperty": false,
                        "generateInRuntime": false,
                        "isTranslatableValue": false,
                        "dataValueField": "Id"
                    },
                    "base_1162_organizationid_115541": {
                        "type": "object",
                        "nspType": "NSPKendoDropDownListProperty",
                        "dbType": "int",
                        "id": 1162,
                        "isCustomProperty": false,
                        "tableStorageDetailsName": "app.Columns",
                        "tableRepositoryName": "SYS_DEF_TABLE",
                        "isReference": true,
                        "referenceTableId": 65,
                        "fieldName": "OrganizationId",
                        "machineName": "OrganizationId",
                        "fieldStorageName": "app.Columns",
                        "isEavProperty": false,
                        "generateInRuntime": false,
                        "isTranslatableValue": false,
                        "dataValueField": "Id"
                    },
                    "cmdb-CiPropertyValues_212_procurement-type_115541": {
                        "type": "object",
                        "nspType": "NSPKendoAutoCompleteProperty",
                        "dbType": "int",
                        "id": 212,
                        "isCustomProperty": false,
                        "tableStorageDetailsName": "cmdb.CiTypeProperties",
                        "tableRepositoryName": "app.Tables",
                        "isReference": true,
                        "referenceTableId": 5,
                        "fieldName": "Procurement type",
                        "machineName": "Procurement type",
                        "entityTypeId": 4168,
                        "fieldStorageName": "cmdb.CiTypeProperties",
                        "isEavProperty": true,
                        "eavStorageValues": "cmdb.CiPropertyValues",
                        "generateInRuntime": true,
                        "isTranslatableValue": false,
                        "translatablePropertyName": "ValueTranslateKey",
                        "translatableEnableInJs": true,
                        "translatableSystemKey": false,
                        "dataValueField": "Id"
                    },
                    "cmdb-CiPropertyValues_213_invoice-number_115541": {
                        "type": "string",
                        "nspType": "NSPTextProperty",
                        "dbType": "string",
                        "id": 213,
                        "isCustomProperty": false,
                        "tableStorageDetailsName": "cmdb.CiTypeProperties",
                        "tableRepositoryName": "app.Tables",
                        "isReference": false,
                        "fieldName": "Invoice number",
                        "machineName": "Invoice number",
                        "entityTypeId": 4168,
                        "fieldStorageName": "cmdb.CiTypeProperties",
                        "isEavProperty": true,
                        "eavStorageValues": "cmdb.CiPropertyValues",
                        "generateInRuntime": true,
                        "isTranslatableValue": false,
                        "translatablePropertyName": "ValueTranslateKey",
                        "translatableEnableInJs": true,
                        "translatableSystemKey": false
                    },
                    "cmdb-CiPropertyValues_214_purchase-date_115541": {
                        "type": "string",
                        "nspType": "NSPKendoDatePickerProperty",
                        "dbType": "DateTime",
                        "id": 214,
                        "isCustomProperty": false,
                        "tableStorageDetailsName": "cmdb.CiTypeProperties",
                        "tableRepositoryName": "app.Tables",
                        "isReference": false,
                        "fieldName": "Purchase date",
                        "machineName": "Purchase date",
                        "entityTypeId": 4168,
                        "fieldStorageName": "cmdb.CiTypeProperties",
                        "isEavProperty": true,
                        "eavStorageValues": "cmdb.CiPropertyValues",
                        "generateInRuntime": true,
                        "isTranslatableValue": false,
                        "translatablePropertyName": "ValueTranslateKey",
                        "translatableEnableInJs": true,
                        "translatableSystemKey": false
                    },
                    "cmdb-CiPropertyValues_215_purchase-price_115541": {
                        "type": "string",
                        "nspType": "NSPKendoNumericTextBoxProperty",
                        "dbType": "decimal",
                        "id": 215,
                        "isCustomProperty": false,
                        "tableStorageDetailsName": "cmdb.CiTypeProperties",
                        "tableRepositoryName": "app.Tables",
                        "isReference": false,
                        "fieldName": "Purchase price",
                        "machineName": "Purchase price",
                        "entityTypeId": 4168,
                        "fieldStorageName": "cmdb.CiTypeProperties",
                        "isEavProperty": true,
                        "eavStorageValues": "cmdb.CiPropertyValues",
                        "generateInRuntime": true,
                        "isTranslatableValue": false,
                        "translatablePropertyName": "ValueTranslateKey",
                        "translatableEnableInJs": true,
                        "translatableSystemKey": false
                    },
                    "cmdb-CiPropertyValues_216_residual-value_115541": {
                        "type": "string",
                        "nspType": "NSPKendoNumericTextBoxProperty",
                        "dbType": "decimal",
                        "id": 216,
                        "isCustomProperty": false,
                        "tableStorageDetailsName": "cmdb.CiTypeProperties",
                        "tableRepositoryName": "app.Tables",
                        "isReference": false,
                        "fieldName": "Residual value",
                        "machineName": "Residual value",
                        "entityTypeId": 4168,
                        "fieldStorageName": "cmdb.CiTypeProperties",
                        "isEavProperty": true,
                        "eavStorageValues": "cmdb.CiPropertyValues",
                        "generateInRuntime": true,
                        "isTranslatableValue": false,
                        "translatablePropertyName": "ValueTranslateKey",
                        "translatableEnableInJs": true,
                        "translatableSystemKey": false
                    },
                    "cmdb-CiPropertyValues_217_inventory-number_115541": {
                        "type": "string",
                        "nspType": "NSPTextProperty",
                        "dbType": "string",
                        "id": 217,
                        "isCustomProperty": false,
                        "tableStorageDetailsName": "cmdb.CiTypeProperties",
                        "tableRepositoryName": "app.Tables",
                        "isReference": false,
                        "fieldName": "Inventory number",
                        "machineName": "Inventory number",
                        "entityTypeId": 4168,
                        "fieldStorageName": "cmdb.CiTypeProperties",
                        "isEavProperty": true,
                        "eavStorageValues": "cmdb.CiPropertyValues",
                        "generateInRuntime": true,
                        "isTranslatableValue": false,
                        "translatablePropertyName": "ValueTranslateKey",
                        "translatableEnableInJs": true,
                        "translatableSystemKey": false
                    },
                    "cmdb-CiPropertyValues_218_last-audit-date_115541": {
                        "type": "string",
                        "nspType": "NSPKendoDatePickerProperty",
                        "dbType": "DateTime",
                        "id": 218,
                        "isCustomProperty": false,
                        "tableStorageDetailsName": "cmdb.CiTypeProperties",
                        "tableRepositoryName": "app.Tables",
                        "isReference": false,
                        "fieldName": "Last Audit Date",
                        "machineName": "Last audit date",
                        "entityTypeId": 4168,
                        "fieldStorageName": "cmdb.CiTypeProperties",
                        "isEavProperty": true,
                        "eavStorageValues": "cmdb.CiPropertyValues",
                        "generateInRuntime": true,
                        "isTranslatableValue": false,
                        "translatablePropertyName": "ValueTranslateKey",
                        "translatableEnableInJs": true,
                        "translatableSystemKey": false
                    },
                    "cmdb-CiPropertyValues_243_inventory-number-type_115541": {
                        "type": "object",
                        "nspType": "NSPKendoDropDownListProperty",
                        "dbType": "int",
                        "id": 243,
                        "isCustomProperty": false,
                        "tableStorageDetailsName": "cmdb.CiTypeProperties",
                        "tableRepositoryName": "app.Tables",
                        "isReference": true,
                        "referenceTableId": 2119,
                        "fieldName": "Inventory number type",
                        "machineName": "Inventory number type",
                        "entityTypeId": 4168,
                        "fieldStorageName": "cmdb.CiTypeProperties",
                        "isEavProperty": true,
                        "eavStorageValues": "cmdb.CiPropertyValues",
                        "generateInRuntime": true,
                        "isTranslatableValue": false,
                        "translatablePropertyName": "ValueTranslateKey",
                        "translatableEnableInJs": true,
                        "translatableSystemKey": false,
                        "dataValueField": "Id"
                    }
                }
            },
            "form": [{
                "nspType": "NSPAccordionProperty",
                "type": "accordion",
                "key": "box0_1_ci_115541",
                "titleKey": "CiTypes.Ci",
                "title": "CI",
                "placeholder": "",
                "collapsed": false,
                "items": [{
                    "nspType": "NSPTextProperty",
                    "type": "text",
                    "required": true,
                    "key": "base_108_name_115541",
                    "titleKey": "Columns.Name",
                    "title": "Name",
                    "width": 25,
                    "placeholder": ""
                }, {
                    "nspType": "NSPKendoEditorProperty",
                    "type": "kendo-editor",
                    "required": false,
                    "key": "base_109_description_115541",
                    "titleKey": "Columns.Description",
                    "title": "Description",
                    "width": 100,
                    "placeholder": "",
                    "kendoOptions": {
                        "encoded": true
                    }
                }, {
                    "nspType": "NSPKendoAutoCompleteProperty",
                    "type": "kendo-auto-complete",
                    "required": true,
                    "key": "base_111_entitytypeid_115541",
                    "titleKey": "Columns.CiTypeId",
                    "title": "Type",
                    "width": 25,
                    "readonly": true,
                    "placeholder": "",
                    "kendoOptions": {
                        "filter": "contains",
                        "placeholder": "",
                        "dataTextField": "Text",
                        "dataValueField": "Id",
                        "autoBind": false,
                        "dataSource": {
                            "serverFiltering": true,
                            "transport": {
                                "read": {
                                    "url": "/NSPFormEngine/NSPDynamicDataSource?referenceTableId=12&tableRepositoryName=app.Tables&numOfRows=20",
                                    "dataType": "json"
                                }
                            }
                        },
                        "optionLabel": "Select ...",
                        "optionLabelKey": "NSPFormEngine.OptionLabelKeyDefault"
                    }
                }, {
                    "nspType": "NSPKendoDropDownListProperty",
                    "type": "kendo-drop-down-list",
                    "required": false,
                    "key": "base_113_departmentid_115541",
                    "titleKey": "Columns.DepartmentId",
                    "title": "Department",
                    "width": 25,
                    "placeholder": "",
                    "kendoOptions": {
                        "filter": "contains",
                        "placeholder": "",
                        "dataTextField": "Text",
                        "dataValueField": "Id",
                        "autoBind": true,
                        "dataSource": {
                            "serverFiltering": false,
                            "transport": {
                                "read": {
                                    "url": "/NSPFormEngine/NSPDynamicLovDataSource?referenceTableId=11&tableRepositoryName=app.Tables&numOfRows=1000000",
                                    "dataType": "json"
                                }
                            }
                        },
                        "optionLabel": "Select ...",
                        "optionLabelKey": "NSPFormEngine.OptionLabelKeyDefault",
                        "useEntityLifecycleCallback": false
                    }
                }, {
                    "nspType": "NSPTextProperty",
                    "type": "text",
                    "required": false,
                    "key": "base_114_serialnumber_115541",
                    "titleKey": "Columns.SerialNumber",
                    "title": "Serial Number",
                    "width": 25,
                    "placeholder": ""
                }, {
                    "nspType": "NSPKendoDropDownListProperty",
                    "type": "kendo-drop-down-list",
                    "required": false,
                    "key": "base_1162_organizationid_115541",
                    "titleKey": "Columns.OrganizationId",
                    "title": "Organization",
                    "width": 25,
                    "placeholder": "",
                    "kendoOptions": {
                        "filter": "contains",
                        "placeholder": "",
                        "dataTextField": "Text",
                        "dataValueField": "Id",
                        "dataSource": {
                            "transport": {
                                "read": {
                                    "url": "/NSPFormEngine/NSPDynamicDataSource?referenceTableId=65&tableRepositoryName=SYS_DEF_TABLE&numOfRows=1000000",
                                    "dataType": "json"
                                }
                            }
                        },
                        "optionLabel": "Select ...",
                        "optionLabelKey": "NSPFormEngine.OptionLabelKeyDefault",
                        "useEntityLifecycleCallback": false
                    }
                }, {
                    "nspType": "NSPKendoDropDownListProperty",
                    "type": "kendo-drop-down-list",
                    "required": false,
                    "key": "base_115_impactid_115541",
                    "titleKey": "Columns.ImpactId",
                    "title": "Impact",
                    "width": 25,
                    "placeholder": "",
                    "kendoOptions": {
                        "filter": "contains",
                        "placeholder": "",
                        "dataTextField": "Text",
                        "dataValueField": "Id",
                        "dataSource": {
                            "transport": {
                                "read": {
                                    "url": "/NSPFormEngine/NSPDynamicLovDataSource?referenceTableId=1&tableRepositoryName=app.Tables&numOfRows=1000000",
                                    "dataType": "json"
                                }
                            }
                        },
                        "optionLabel": "Select ...",
                        "optionLabelKey": "NSPFormEngine.OptionLabelKeyDefault",
                        "useEntityLifecycleCallback": false
                    }
                }, {
                    "nspType": "NSPKendoDropDownListProperty",
                    "type": "kendo-drop-down-list",
                    "required": true,
                    "key": "base_116_cistateid_115541",
                    "titleKey": "Columns.CiStateId",
                    "title": "State",
                    "width": 25,
                    "placeholder": "",
                    "kendoOptions": {
                        "filter": "contains",
                        "placeholder": "",
                        "dataTextField": "name",
                        "dataValueField": "id",
                        "dataSource": {
                            "transport": {
                                "read": {
                                    "url": "/CmdbCiView/GetAllowedStatesList",
                                    "dataType": "json"
                                }
                            }
                        },
                        "optionLabel": "Select ...",
                        "optionLabelKey": "NSPFormEngine.OptionLabelKeyDefault",
                        "useEntityLifecycleCallback": true
                    }
                }, {
                    "nspType": "NSPKendoAutoCompleteProperty",
                    "type": "kendo-auto-complete",
                    "required": false,
                    "key": "base_117_productid_115541",
                    "titleKey": "Columns.ProductId",
                    "title": "Product",
                    "width": 25,
                    "placeholder": "",
                    "browse": {
                        "url": "/Scripts/src/app/views/form-engine/browse-windows/browse-products-grid.html",
                        "title": "Select",
                        "titleKey": "Common.Select",
                        "caption": "Browse",
                        "captionKey": "NSPFormEngine.BrowseCaptionKeyDefault",
                        "icon": "icon-search"
                    },
                    "kendoOptions": {
                        "filter": "contains",
                        "placeholder": "",
                        "dataTextField": "Text",
                        "dataValueField": "Id",
                        "autoBind": false,
                        "dataSource": {
                            "serverFiltering": true,
                            "transport": {
                                "read": {
                                    "url": "/NSPFormEngine/NSPDynamicDataSource?referenceTableId=17&tableRepositoryName=app.Tables&numOfRows=20",
                                    "dataType": "json"
                                }
                            }
                        },
                        "optionLabel": "Select ...",
                        "optionLabelKey": "NSPFormEngine.OptionLabelKeyDefault"
                    }
                }, {
                    "nspType": "NSPKendoAutoCompleteProperty",
                    "type": "kendo-auto-complete",
                    "required": false,
                    "key": "base_118_vendorid_115541",
                    "titleKey": "Columns.VendorId",
                    "title": "Vendor",
                    "width": 25,
                    "placeholder": "",
                    "browse": {
                        "url": "/Scripts/src/app/views/form-engine/browse-windows/browse-vendors-grid.html",
                        "title": "Select",
                        "titleKey": "Common.Select",
                        "caption": "Browse",
                        "captionKey": "NSPFormEngine.BrowseCaptionKeyDefault",
                        "icon": "icon-search"
                    },
                    "kendoOptions": {
                        "filter": "contains",
                        "placeholder": "",
                        "dataTextField": "Text",
                        "dataValueField": "Id",
                        "autoBind": false,
                        "dataSource": {
                            "serverFiltering": true,
                            "transport": {
                                "read": {
                                    "url": "/NSPFormEngine/NSPDynamicDataSource?referenceTableId=18&tableRepositoryName=app.Tables&numOfRows=20",
                                    "dataType": "json"
                                }
                            }
                        },
                        "optionLabel": "Select ...",
                        "optionLabelKey": "NSPFormEngine.OptionLabelKeyDefault"
                    }
                }, {
                    "nspType": "NSPKendoAutoCompleteProperty",
                    "type": "kendo-auto-complete",
                    "required": false,
                    "key": "base_120_locationid_115541",
                    "titleKey": "Columns.LocationId",
                    "title": "Location",
                    "width": 25,
                    "placeholder": "",
                    "kendoOptions": {
                        "filter": "contains",
                        "placeholder": "",
                        "dataTextField": "Text",
                        "dataValueField": "Id",
                        "autoBind": false,
                        "dataSource": {
                            "serverFiltering": true,
                            "transport": {
                                "read": {
                                    "url": "/NSPFormEngine/NSPDynamicDataSource?referenceTableId=241&tableRepositoryName=SYS_DEF_TABLE&numOfRows=20",
                                    "dataType": "json"
                                }
                            }
                        },
                        "optionLabel": "Select ...",
                        "optionLabelKey": "NSPFormEngine.OptionLabelKeyDefault"
                    }
                }, {
                    "nspType": "NSPKendoAutoCompleteProperty",
                    "type": "kendo-auto-complete",
                    "required": false,
                    "key": "base_121_managedbyid_115541",
                    "titleKey": "Columns.ManagedById",
                    "title": "Managed By",
                    "width": 25,
                    "placeholder": "",
                    "browse": {
                        "url": "/Scripts/build/app/views/form-engine/browse-windows/browse-users-grid.html",
                        "title": "Select",
                        "titleKey": "Common.Select",
                        "caption": "Browse",
                        "captionKey": "NSPFormEngine.BrowseCaptionKeyDefault",
                        "icon": "icon-search"
                    },
                    "kendoOptions": {
                        "filter": "contains",
                        "placeholder": "",
                        "dataTextField": "Text",
                        "dataValueField": "Id",
                        "autoBind": false,
                        "dataSource": {
                            "serverFiltering": true,
                            "transport": {
                                "read": {
                                    "url": "/NSPFormEngine/NSPDynamicDataSource?referenceTableId=269&tableRepositoryName=SYS_DEF_TABLE&numOfRows=20",
                                    "dataType": "json"
                                }
                            }
                        },
                        "optionLabel": "Select ...",
                        "optionLabelKey": "NSPFormEngine.OptionLabelKeyDefault"
                    }
                }, {
                    "nspType": "NSPKendoAutoCompleteProperty",
                    "type": "kendo-auto-complete",
                    "required": false,
                    "key": "base_122_usedbyid_115541",
                    "titleKey": "Columns.UsedById",
                    "title": "Used By",
                    "width": 25,
                    "placeholder": "",
                    "browse": {
                        "url": "/Scripts/build/app/views/form-engine/browse-windows/browse-users-grid.html",
                        "title": "Select",
                        "titleKey": "Common.Select",
                        "caption": "Browse",
                        "captionKey": "NSPFormEngine.BrowseCaptionKeyDefault",
                        "icon": "icon-search"
                    },
                    "kendoOptions": {
                        "filter": "contains",
                        "placeholder": "",
                        "dataTextField": "Text",
                        "dataValueField": "Id",
                        "autoBind": false,
                        "dataSource": {
                            "serverFiltering": true,
                            "transport": {
                                "read": {
                                    "url": "/NSPFormEngine/NSPDynamicDataSource?referenceTableId=269&tableRepositoryName=SYS_DEF_TABLE&numOfRows=20",
                                    "dataType": "json"
                                }
                            }
                        },
                        "optionLabel": "Select ...",
                        "optionLabelKey": "NSPFormEngine.OptionLabelKeyDefault"
                    }
                }]
            }, {
                "nspType": "NSPActionsProperty",
                "type": "actions",
                "key": "10_cioptions_115541",
                "titleKey": "FormPropertyBoxes.CiOptions",
                "title": "Options",
                "placeholder": "",
                "collapsed": false,
                "items": [{
                    "nspType": "NSPSubmitButtonProperty",
                    "type": "submit",
                    "key": "submitbutton_115541",
                    "titleKey": "FormElements.Save",
                    "title": "Save",
                    "placeholder": ""
                }, {
                    "nspType": "NSPButtonProperty",
                    "type": "button",
                    "key": "cancelbutton_115541",
                    "titleKey": "FormElements.Cancel",
                    "title": "Cancel",
                    "onClick": "cancel()",
                    "placeholder": ""
                }]
            }, {
                "nspType": "NSPAccordionProperty",
                "type": "accordion",
                "key": "box1_4168_asset_115541",
                "titleKey": "CiTypes.Asset",
                "title": "Asset",
                "placeholder": "",
                "collapsed": false,
                "items": [{
                    "nspType": "NSPKendoAutoCompleteProperty",
                    "type": "kendo-auto-complete",
                    "required": false,
                    "key": "cmdb-CiPropertyValues_212_procurement-type_115541",
                    "titleKey": "CiTypeProperties.ProcurementType",
                    "title": "Procurement type",
                    "width": 25,
                    "placeholder": "",
                    "kendoOptions": {
                        "filter": "contains",
                        "placeholder": "",
                        "dataTextField": "Text",
                        "dataValueField": "Id",
                        "autoBind": false,
                        "dataSource": {
                            "serverFiltering": true,
                            "transport": {
                                "read": {
                                    "url": "/NSPFormEngine/NSPDynamicDataSource?referenceTableId=5&tableRepositoryName=app.Tables&numOfRows=20",
                                    "dataType": "json"
                                }
                            }
                        },
                        "optionLabel": "Select ...",
                        "optionLabelKey": "NSPFormEngine.OptionLabelKeyDefault"
                    }
                }, {
                    "nspType": "NSPTextProperty",
                    "type": "text",
                    "required": false,
                    "key": "cmdb-CiPropertyValues_213_invoice-number_115541",
                    "titleKey": "CiTypeProperties.InvoiceNumber",
                    "title": "Invoice number",
                    "width": 25,
                    "placeholder": ""
                }, {
                    "nspType": "NSPKendoDatePickerProperty",
                    "type": "kendo-date-picker",
                    "required": false,
                    "key": "cmdb-CiPropertyValues_214_purchase-date_115541",
                    "titleKey": "CiTypeProperties.PurchaseDate",
                    "title": "Purchase date",
                    "width": 25,
                    "placeholder": "",
                    "kendoOptions": {
                        "format": "d"
                    }
                }, {
                    "nspType": "NSPKendoNumericTextBoxProperty",
                    "type": "kendo-numeric-text-box",
                    "required": false,
                    "key": "cmdb-CiPropertyValues_215_purchase-price_115541",
                    "titleKey": "CiTypeProperties.PurchasePrice",
                    "title": "Purchase price",
                    "width": 25,
                    "placeholder": "",
                    "kendoOptions": {
                        "format": "#.##",
                        "decimals": 2,
                        "min": 0,
                        "spinners": false
                    }
                }, {
                    "nspType": "NSPKendoNumericTextBoxProperty",
                    "type": "kendo-numeric-text-box",
                    "required": false,
                    "key": "cmdb-CiPropertyValues_216_residual-value_115541",
                    "titleKey": "CiTypeProperties.ResidualValue",
                    "title": "Residual value",
                    "width": 25,
                    "placeholder": "",
                    "kendoOptions": {
                        "format": "#.##",
                        "decimals": 2,
                        "min": 0,
                        "spinners": false
                    }
                }, {
                    "nspType": "NSPTextProperty",
                    "type": "text",
                    "required": false,
                    "key": "cmdb-CiPropertyValues_217_inventory-number_115541",
                    "titleKey": "CiTypeProperties.InventoryNumber",
                    "title": "Inventory number",
                    "width": 25,
                    "placeholder": ""
                }, {
                    "nspType": "NSPKendoDatePickerProperty",
                    "type": "kendo-date-picker",
                    "required": false,
                    "key": "cmdb-CiPropertyValues_218_last-audit-date_115541",
                    "titleKey": "CiTypeProperties.LastAuditDate",
                    "title": "Last Audit Date",
                    "width": 25,
                    "placeholder": "",
                    "kendoOptions": {
                        "format": "d"
                    }
                }, {
                    "nspType": "NSPKendoDropDownListProperty",
                    "type": "kendo-drop-down-list",
                    "required": false,
                    "key": "cmdb-CiPropertyValues_243_inventory-number-type_115541",
                    "titleKey": "CiTypeProperties.InventoryNumberType",
                    "title": "Inventory number type",
                    "width": 25,
                    "placeholder": "",
                    "kendoOptions": {
                        "filter": "contains",
                        "placeholder": "",
                        "dataTextField": "Text",
                        "dataValueField": "Id",
                        "autoBind": true,
                        "dataSource": {
                            "serverFiltering": false,
                            "transport": {
                                "read": {
                                    "url": "/NSPFormEngine/NSPDynamicLovDataSource?referenceTableId=2119&tableRepositoryName=app.Tables&numOfRows=1000000",
                                    "dataType": "json"
                                }
                            }
                        },
                        "optionLabel": "Select ...",
                        "optionLabelKey": "NSPFormEngine.OptionLabelKeyDefault",
                        "useEntityLifecycleCallback": false
                    }
                }]
            }],
            "model": {
                "base_108_name_115541": "",
                "base_109_description_115541": "",
                "base_111_entitytypeid_115541": {
                    "Id": 4168,
                    "Text": "Asset"
                },
                "base_113_departmentid_115541": {
                    "Id": "",
                    "Text": ""
                },
                "base_114_serialnumber_115541": "",
                "base_115_impactid_115541": {
                    "Id": "",
                    "Text": ""
                },
                "base_116_cistateid_115541": {
                    "id": "",
                    "name": ""
                },
                "base_117_productid_115541": {
                    "Id": "",
                    "Text": ""
                },
                "base_118_vendorid_115541": {
                    "Id": "",
                    "Text": ""
                },
                "base_120_locationid_115541": {
                    "Id": "",
                    "Text": ""
                },
                "base_121_managedbyid_115541": {
                    "Id": "",
                    "Text": ""
                },
                "base_122_usedbyid_115541": {
                    "Id": "",
                    "Text": ""
                },
                "base_1162_organizationid_115541": {
                    "Id": "",
                    "Text": ""
                },
                "cmdb-CiPropertyValues_212_procurement-type_115541": {
                    "Id": "",
                    "Text": ""
                },
                "cmdb-CiPropertyValues_213_invoice-number_115541": "",
                "cmdb-CiPropertyValues_214_purchase-date_115541": {
                    "text": "",
                    "value": ""
                },
                "cmdb-CiPropertyValues_215_purchase-price_115541": "",
                "cmdb-CiPropertyValues_216_residual-value_115541": "",
                "cmdb-CiPropertyValues_217_inventory-number_115541": "",
                "cmdb-CiPropertyValues_218_last-audit-date_115541": {
                    "text": "",
                    "value": ""
                },
                "cmdb-CiPropertyValues_243_inventory-number-type_115541": {
                    "Id": "",
                    "Text": ""
                }
            },
            "submitUrl": "CmdbCi/SaveCi"
        };

        vm.schema = response.schema;
        vm.form = response.form;
        vm.model = response.model;
        
    }
})();