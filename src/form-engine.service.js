(function() {
    'use strict';

    angular
        .module('formEngine')
        .factory('feService', feService);

    feService.$inject = ['$http', '$sce'];

    /* @ngInject */
    function feService($http, $sce) {
        var factory = {
            updateDependants: updateDependants,
            autoCompleteParameterMap: autoCompleteParameterMap,
            propertyExists: propertyExists,
            translate: translate,
            getContrast: getContrast,
            rgbToHex: rgbToHex,
            stripTags: stripTags,
            hierarchicalDataSource: hierarchicalDataSource,
            getMimeTypeName: getMimeTypeName,
            decodeHtml: decodeHtml,
            htmlEncode: htmlEncode,
            htmlDecode: htmlDecode,
            secondsToHoursAndMinutes: secondsToHoursAndMinutes,
            getBrowser: getBrowser
        },
        tempDocument = null, // A temporary document used for stripTags method. Value is assigned when stripTags is executed the first time.
        mimeTypes = {
            "image/jpeg": 'Common.JPEGimage',
            "image/png": 'Common.PNGimage',
            "image/bmp": 'Common.BMPimage',
            "image/tiff": 'Common.TIFFfile',
            "application/msword": 'Common.MicrosoftWord',
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document": 'Common.MicrosoftWord',
            "application/vnd.ms-excel": 'Common.MicrosoftExcel',
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet": 'Common.MicrosoftExcel',
            "application/vnd.ms-powerpoint": 'Common.MicrosoftPowerPoint',
            "application/vnd.openxmlformats-officedocument.presentationml.presentation": 'Common.MicrosoftPowerPoint',
            "text/plain": 'Common.TextFile',
            "application/pdf": 'Common.PDF',
            "application/zip": 'Common.ZipArchive',
            "application/x-rar-compressed": 'Common.RARArchive',
            "video/x-ms-wmv": 'Common.MicrosoftWindowsMediaVideo',
            "application/mp4": 'Common.MPEG4',
            "video/mp4": 'Common.MPEG-4Video',
            "audio/mp3": 'Common.MP3',
            "video/quicktime": 'Common.QuickTimeVideo',
            "text/csv": 'Common.Comma-SeperatedValues',
            "application/vnd.oasis.opendocument.text": 'Common.OpenDocumentText',
            "text/tab-separated-values": 'Common.TabSeperatedValues',
            "application/xml": 'Common.XML',
            "message/rfc822": 'Common.EmailMessage'
        };

        return factory;

        function propertyExists(obj, key) {
            return key.split(".").every(function(x) {
                if (typeof obj != "object" || obj === null || !(x in obj))
                    return false;
                obj = obj[x];
                return true;
            });
        }

        function updateDependants(settings, model, sfFormElement) {
            var dependants = settings.kendoOptions.dependants,
                dataValueField = settings.kendoOptions.dataValueField,
                dataTextField = settings.kendoOptions.dataTextField,
                filterCondition = null,
                sfFormScope = sfFormElement.scope(),
                sfFormAttribute = sfFormElement.attr('sf-form'),
                sfModelAttribute = sfFormElement.attr('sf-model'),
                sfSchemaAttribute = sfFormElement.attr('sf-schema'),
                sfFormName = sfFormElement.attr('name');

            angular.forEach(dependants, function(dependant, key) {

                var dependantElement = $('#' + dependant.key),
                    dependantScope = dependantElement ? dependantElement.scope() : null,
                    dependantWidget = kendo.widgetInstance(dependantElement),
                    updatedSfFormScope = null;

                filterCondition = {
                    fieldId: dependant.filterFieldId,
                    fieldName: dependant.filterFieldName,
                    isStatic: dependant.filterFieldIsStatic,
                    filterValue: ''
                };

                if (model && model[dataTextField] && model[dataTextField]) {
                    filterCondition.filterValue = model[dataValueField];
                }

                updatedSfFormScope = updateDependantScope(sfFormScope[sfFormAttribute], dependant.key, filterCondition);

                if (dependantScope) {
                    dependantScope.settings = updatedSfFormScope.dependant;
                }

                sfFormScope[sfFormAttribute] = updatedSfFormScope.form;

                if (sfFormScope[sfFormName].$dirty) {
                    sfFormScope[sfModelAttribute][dependant.key] = undefined;
                    if (dependantWidget && dependantWidget.options.dataSource) {
                        dependantScope.clearHandler(false);
                    }
                }

            });
        }

        function updateDependantScope(formData, key, filterCondition) {
            var dependant = null;
            function iterateForm(data, keyValue) {
                for (var i = 0; i < data.length; i++) {
                    var current = data[i];
                    if (current.key && current.key.slice(-1)[0] === key) {
                        dependant = data[i];
                        if (dependant.kendoOptions.dataSource && dependant.kendoOptions.dataSource.transport.read) {
                            if (!dependant.kendoOptions.dataSource.transport.read.data) {
                                dependant.kendoOptions.dataSource.transport.read.data = {};
                            }
                            if(filterCondition.filterValue){
                                dependant.kendoOptions.dataSource.transport.read.data.dependencyFilterConditions = JSON.stringify([filterCondition]);
                            }else{
                                dependant.kendoOptions.dataSource.transport.read.data.dependencyFilterConditions = [];
                            }                            
                        }
                        if (dependant.browse) {
                            dependant.browse.filterCondition = filterCondition;
                        }
                        break;
                    }
                    if (current.items && current.items.length) {
                        iterateForm(current.items, keyValue);
                    }
                }
            }
            iterateForm(formData, key);
            dependant.kendoOptions.dataSource.transport.parameterMap = function(data, type) {
                return autoCompleteParameterMap(dependant, data);
            };
            return {
                form: formData,
                dependant: dependant
            };
        }

        function autoCompleteParameterMap(form, data) {
            var result = data;
            if (data && data.filter && data.filter.filters.length) {
              result.searchText = data.filter.filters[0].value;
              result.filterCriteria = angular.toJson(data.filter);
            } else {
              result.searchText = "";
              result.getAll = true;
            }
            if (propertyExists(form, 'kendoOptions.dataSource.pageSize')) {
              result.pageSize = form.kendoOptions.dataSource.pageSize;
            }
            return result;
        }

        function translate(text, key) {
            var result = key;
            if (NSP && NSP.Translate && NSP.Translate.t(key)) {
                result = NSP.Translate.t(key);
            }
            return result;
        }

        function getContrast(hexcolor) {
            if (hexcolor) {
                var r = parseInt(hexcolor.substr(0, 2), 16);
                var g = parseInt(hexcolor.substr(2, 2), 16);
                var b = parseInt(hexcolor.substr(4, 2), 16);
                var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
                return (yiq >= 192) ? '#565656' : '#ffffff';
            }
        }

        function rgbToHex(rgb) {
            rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);

            function hex(x) {
                return ("0" + parseInt(x).toString(16)).slice(-2);
            }

            return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
        }

        function stripTags(htmlString) {

            // tempDocument is cached for performance and created only when required
            if (!tempDocument) {
                // creates a temporary document object used for striping html
                tempDocument = document.implementation.createHTMLDocument("New");
            }

            // htmlString is inserted into tempDocument body and text is pulled out from native innerText
            var tmp = tempDocument.body;
            tmp.innerHTML = htmlString;
            var result = tmp.textContent || tmp.innerText || "";

            // Fix for Edge putting string 'null' instead of actual null
            return result == 'null' ? "" : result;
        }

        function hierarchicalDataSource(data, idField, foreignKey, rootLevel, children) {
            var hash = {};
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                var id = item[idField];
                var parentId = item[foreignKey];

                hash[id] = hash[id] || [];
                hash[parentId] = hash[parentId] || [];

                item[children ? children : 'items'] = hash[id];
                hash[parentId].push(item);
            }
            return hash[rootLevel];
        }

        function getMimeTypeName(type) {
            return mimeTypes.hasOwnProperty(type) ? translate(mimeTypes[type], mimeTypes[type]) : type;
        }

        function decodeHtml(html) {
            var txt = document.createElement("textarea");
            txt.innerHTML = html;
            return txt.value;
        }

        function htmlEncode(value) {
            return $('<div/>').text(value).html();
        }

        function htmlDecode(value){
          return $sce.trustAsHtml($('<div/>').html(value).text());
        }

        function secondsToHoursAndMinutes(seconds) {
            var result = "",
                hours = seconds ? Math.floor(seconds / 3600) : 0,
                minutes = seconds ? Math.floor((seconds - (hours * 3600)) / 60) : 0;
            if (hours.toString().length === 1) {
                hours = '0' + hours;
            }
            if (minutes.toString().length === 1) {
                minutes = '0' + minutes;
            }
            result = hours + ":" + minutes;
            return result;
        }

        function getBrowser() {
            var ua = navigator.userAgent,
                tem,
                M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
            if (/trident/i.test(M[1])) {
                tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
                return 'IE ' + (tem[1] || '');
            }
            if (M[1] === 'Chrome') {
                tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
                if (tem !== null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
            }
            M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
            if ((tem = ua.match(/version\/(\d+)/i)) !== null) M.splice(1, 1, tem[1]);
            return M.join(' ');
        }


    }
})();