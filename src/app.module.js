angular.module('mainApp', [
    'kendo.directives',
    'schemaForm',
    'ui.router',
    'formEngine',
    'nspCore',
    'demo'
]);