(function() {
    'use strict';
    angular.module('formEngine').directive('feKendoMultiSelect', feKendoMultiSelect);

    feKendoMultiSelect.$inject = ['$templateCache', '$http', '$q', '$timeout', 'feService'];

    function feKendoMultiSelect($templateCache, $http, $q, $timeout, feService) {
        var directive = {
                link: link,
                templateUrl: '/web/kendo-multi-select/fe-kendo-multi-select.template.html',
                restrict: 'AE',
                require: 'ngModel',
                scope: {
                    options: '=feOptions',
                    ngModel: '=ngModel'
                }
            };

        return directive;

        function link(scope, element, attrs, ngModelCtrl) {

            var defaultOptions = {
                    kendoOptions: {
                        autoBind: false,
                        dataTextField: "Text",
                        dataValueField: "Id",
                        minLength: 3,
                        dataSource: {
                            data: scope.ngModel,
                            serverFiltering: true
                        }
                    }
                };

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = feService.translate;
            scope.onChange = onChange;
            scope.widgetModel = angular.copy(scope.ngModel);

            if (feService.propertyExists(scope.settings, 'kendoOptions.dataSource.transport.read') &&
                feService.propertyExists(scope.settings, 'kendoOptions.dataSource.serverFiltering') &&
                scope.settings.kendoOptions.dataSource.serverFiltering &&
                !feService.propertyExists(scope.settings, 'kendoOptions.dataSource.transport.parameterMap')) {
                scope.settings.kendoOptions.dataSource.transport.parameterMap = function(data, type) {
                    return feService.autoCompleteParameterMap(scope.settings, data);
                };
            }

            if (scope.ngModel && scope.ngModel.length) {
                
            } else {
                ngModelCtrl.$setViewValue(undefined);
                scope.ngModel = undefined;
            }

            if (scope.settings.kendoOptions && !scope.settings.kendoOptions.template && scope.settings.kendoOptions.templateCache) {
                scope.settings.kendoOptions.template = $templateCache.get(scope.settings.kendoOptions.templateCache);
            }

            if (scope.settings.kendoOptions && scope.settings.readonly === true) {
                scope.settings.kendoOptions.enable = false;
            }

            scope.$on('kendoWidgetCreated', function(event) {
                scope.widget.bind('change', onChange);
                scope.widget.bind('dataBound', initialDataBound);
            });

            scope.$on('$destroy', function() {
                if (scope.settings.condition) {
                    ngModelCtrl.$setViewValue(undefined);
                }
            });

            function onChange(event) {
                var widgetValue = scope.widget.value(),
                    dataItems = scope.widget.dataItems(),
                    modelValue = [];

                if (widgetValue && dataItems && dataItems.length) {
                    angular.forEach(dataItems, function(item, key) {
                        var dataItem = {};
                        dataItem[scope.settings.kendoOptions.dataValueField] = item[scope.settings.kendoOptions.dataValueField];
                        dataItem[scope.settings.kendoOptions.dataTextField] = item[scope.settings.kendoOptions.dataTextField];
                        if (scope.settings.kendoOptions.dataAdditionalFields) {
                            angular.forEach(scope.settings.kendoOptions.dataAdditionalFields, function(additionalField, key) {
                                dataItem[additionalField] = item[additionalField];
                            });
                        }
                        modelValue.push(dataItem);
                    });
                } else {
                    modelValue = undefined;
                }
                ngModelCtrl.$setViewValue(modelValue);
                scope.$emit('schemaFormValidate');
            }

            function initialDataBound() {
                scope.widget.unbind('dataBound', initialDataBound);
                scope.widget.dataSource.data(scope.ngModel);
                scope.widget.value(scope.ngModel);
            }
        }
    }
})();