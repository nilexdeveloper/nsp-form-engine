(function() {
    'use strict';
    angular.module('formEngine').directive('feTicketDescription', feTicketDescription);

    function feTicketDescription($templateCache, $http, $q, $timeout, $rootScope, feService) {
        var directive = {
            link: link,
            templateUrl: '/web/ticket-description/fe-ticket-description.template.html',
            restrict: 'AE',
            require: 'ngModel',
            scope: {
                options: '=feOptions',
                ngModel: '=ngModel'
            }
        };

        return directive;

        function link(scope, element, attrs, ngModelCtrl) {

            var defaultOptions = {
                editorOptions: {
                    encoded: true,
                    resizable: {
                        min: 150
                    }
                }
            };

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = feService.translate;
            scope.editorValue = "";
            scope.attachments = [];

            if (scope.settings.editorOptions && scope.settings.readonly === true) {
                scope.settings.editorOptions.enable = false;
            }

            if (scope.settings.key) {
                scope.settings.attachmentsOptions.fallback_id = "ticketDescriptionFileUpload" + scope.settings.key.slice(-1)[0];
            }

            if (scope.ngModel) {
                scope.editorValue = scope.ngModel.text;
                ngModelCtrl.$setViewValue({
                    text: scope.settings.editorOptions.encoded ? feService.htmlEncode(scope.ngModel.text) : scope.ngModel.text,
                    attachments: scope.ngModel.attachments
                });
            }

            scope.$on('kendoWidgetCreated', function(event) {
                scope.widget.bind('change', change);
            });

            scope.$watch('attachments', function(newValue) {
                ngModelCtrl.$setViewValue({
                    text: scope.widget.textarea.val(),
                    attachments: newValue
                });
            });

            function change(event) {
                if (scope.editorValue || scope.attachments.length) {
                    ngModelCtrl.$setViewValue({
                        text: scope.widget.textarea.val(),
                        attachments: scope.attachments
                    });
                } else {
                    ngModelCtrl.$setViewValue(undefined);
                }
            }

        }
    }
})();