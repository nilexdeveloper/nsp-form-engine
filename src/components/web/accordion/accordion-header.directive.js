angular.module('formEngine')
    .directive('accordionHeader', ['feService', function(feService) {
        return {
            restrict: 'AE',
            scope: {
                options: "="
            },
            templateUrl: '/web/accordion/accordion-header.template.html',
            link: function(scope, element, attrs, ngModel) {

                var defaultOptions = {
                        title: '',
                        titleKey: '',
                        collapsed: false
                    },
                    fieldsetElement = element.closest('fieldset');

                scope.settings = angular.merge({}, defaultOptions, scope.options);

                scope.toggle = function($event) {
                    fieldsetElement.toggleClass('collapsed');
                    scope.settings.collapsed = !scope.settings.collapsed;
                };
            }
        };
    }]);