(function() {
    'use strict';

    angular
        .module('formEngine')
        .directive('feKendoDateTimePicker', feKendoDateTimePicker);

    feKendoDateTimePicker.$inject = ['$templateCache', '$compile', 'feService'];

    /* @ngInject */
    function feKendoDateTimePicker($templateCache, $compile, feService) {
        var directive = {
            link: link,
            restrict: 'AE',
            require: 'ngModel',
            scope: {
                options: '=feOptions',
                ngModel: '=ngModel'
            }
        };
        return directive;

        function link(scope, element, attrs, ngModelCtrl) {
            var templates = {
                    'kendo-date-picker': '/web/kendo-date-time-picker/templates/fe-kendo-date-picker.template.html',
                    'kendo-time-picker': '/web/kendo-date-time-picker/templates/fe-kendo-time-picker.template.html',
                    'kendo-date-time-picker': '/web/kendo-date-time-picker/templates/fe-kendo-date-time-picker.template.html'
                },
                compiledTemplate = '',
                formats = {
                    'kendo-date-picker': 'd',
                    'kendo-time-picker': 't',
                    'kendo-date-time-picker': 'g'
                },
                defaultOptions = {
                    template: templates[scope.options.type] ? templates[scope.options.type] : "",
                    valuePrefix: "#",
                    validationMessage: {
                        formatNotValid: feService.translate('Format not allowed', 'Common.FormatNotAllowed') + ". " + feService.translate('Allowed format', 'Common.AllowedFormat') + ': ' + kendo.culture().calendar.patterns[formats[scope.options.type]]
                    },
                    kendoOptions: {
                        format: formats[scope.options.type]
                    }
                };

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = feService.translate;
            scope.widgetText = scope.ngModel ? scope.ngModel.text : "";
            scope.options.validationMessage = scope.settings.validationMessage;

            if (scope.settings.placeholder && scope.settings.kendoOptions && !scope.settings.kendoOptions.placeholder) {
                if (scope.settings.isTranslatedLabels) {
                    scope.settings.kendoOptions.placeholder = scope.settings.placeholder;
                } else {
                    scope.settings.kendoOptions.placeholder = translate(scope.settings.placeholder, scope.settings.placeholderKey);
                }
            }

            if (scope.ngModel && !angular.isUndefined(scope.ngModel.value) && scope.ngModel.value !== null && scope.ngModel.value !== '' && scope.ngModel.text !== '') {
                scope.widgetText = kendo.toString(kendo.parseDate(scope.ngModel.value), scope.settings.kendoOptions.format);
            } else {
                ngModelCtrl.$setViewValue(undefined);
                scope.ngModel = undefined;
            }

            scope.$watch('widgetText', function(newValue, oldValue) {
                var inputValue = $(scope.widget.element[0]).val();
                if (scope.settings.required && ngModelCtrl.$dirty) {
                    if (newValue) {
                        ngModelCtrl.$setValidity('tv4-302', true);
                    } else {
                        ngModelCtrl.$setValidity('tv4-302', false);
                    }
                }
                if (newValue !== oldValue) {
                    change(inputValue);
                }
            });

            scope.$on('kendoWidgetCreated', function(event) {
                scope.widget.bind('close', function(event) {
                    var value = kendo.toString(this.value(), scope.settings.kendoOptions.format);
                    if (value !== scope.widgetText && ngModelCtrl.$dirty) {
                        scope.widgetText = value;
                        change(value);
                    }
                });
            });

            element.html($templateCache.get(templates[scope.settings.type]));
            $compile(element.contents())(scope);

            function change(widgetValue) {
                var modelValue;
                if (!angular.isUndefined(widgetValue) && widgetValue !== null && widgetValue !== '') {
                    if (validDate(widgetValue)) {
                        modelValue = {
                            text: kendo.toString(widgetValue, scope.settings.kendoOptions.format),
                            value: scope.settings.valuePrefix + kendo.parseDate(widgetValue, scope.settings.kendoOptions.format).toISOString()
                        };
                        ngModelCtrl.$setValidity('formatNotValid', true);
                    } else {
                        ngModelCtrl.$setValidity('formatNotValid', false);
                    }
                } else {
                    ngModelCtrl.$setValidity('formatNotValid', true);
                }
                ngModelCtrl.$setViewValue(modelValue);
                scope.$emit('schemaFormValidate');
            }

            function validDate(value) {
                if (!angular.isUndefined(value) && value !== null && kendo.parseDate(value, scope.settings.kendoOptions.format)) {
                    return true;
                }
            }

        }

    }
})();