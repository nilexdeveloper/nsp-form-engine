(function() {
    'use strict';
    angular.module('formEngine').directive('feTrustAsHtml', feTrustAsHtml);

    feTrustAsHtml.$inject = ['$compile', 'feService'];

    function feTrustAsHtml($compile, feService) {
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;
        function link(scope, element, attrs, ngModel) {
            scope.decode = feService.htmlDecode;
            var data = attrs.feTrustAsHtml;
            var isEncoded = scope.$eval(attrs.encoded);
            if(isEncoded === false){
                attrs.$set('ngBindHtml', data);
            }else{
                attrs.$set('ngBindHtml', 'decode(' + data + ')');
            }
            element.removeAttr('fe-trust-as-html');
            $compile(element)(scope);
        }
    }

})();