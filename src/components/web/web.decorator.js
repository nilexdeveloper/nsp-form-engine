/**
 * Angular Schema Form - Web Decorators
 */

(function() {
    'use strict';

    angular.module('schemaForm').config(config);

    config.$inject = ['schemaFormDecoratorsProvider', 'sfBuilderProvider', 'sfPathProvider'];

    function config(decoratorsProvider, sfBuilderProvider, sfPathProvider) {

        var base = '/web/',
            simpleTransclusion  = sfBuilderProvider.builders.simpleTransclusion,
            ngModelOptions      = sfBuilderProvider.builders.ngModelOptions,
            ngModel             = sfBuilderProvider.builders.ngModel,
            sfField             = sfBuilderProvider.builders.sfField,
            condition           = sfBuilderProvider.builders.condition,
            array               = sfBuilderProvider.builders.array;


        // Tabs is so bootstrap specific that it stays here.
        var tabs = function(args) {
            if (args.form.tabs && args.form.tabs.length > 0) {
                var tabContent = args.fieldFrag.querySelector('.tab-content');
                args.form.tabs.forEach(function(tab, index) {
                    var div = document.createElement('div');
                    div.className = 'tab-pane';
                    div.setAttribute('ng-disabled', 'form.readonly');
                    div.setAttribute('ng-show', 'selected.tab === ' + index);
                    div.setAttribute('ng-class', '{active: selected.tab === ' + index + '}');
                    var childFrag = args.build(tab.items, args.path + '.tabs[' + index + '].items', args.state);
                    div.appendChild(childFrag);
                    tabContent.appendChild(div);
                });
            }
        };

        var defaults = [sfField, ngModel, ngModelOptions, condition];

        decoratorsProvider.defineDecorator('feDecorator', {
            'default': {
                template: base + 'default/default.decorator.html',
                builder: defaults,
                replace: false
            },
            textarea: {
                template: base + 'textarea/textarea.decorator.html',
                builder: defaults,
                replace: false
            },
            fieldset: {
                template: base + 'fieldset/fieldset.decorator.html',
                builder: [sfField, simpleTransclusion, condition],
                replace: false
            },
            array: {
                template: base + 'array/array.decorator.html',
                builder: [sfField, ngModelOptions, ngModel, array, condition],
                replace: false
            },
            tabarray: {
                template: base + 'tabarray/tabarray.decorator.html',
                builder: [sfField, ngModelOptions, ngModel, array, condition],
                replace: false
            },
            tabs: {
                template: base + 'tabs/tabs.decorator.html',
                builder: [sfField, ngModelOptions, tabs, condition],
                replace: false
            },
            section: {
                template: base + 'section/section.decorator.html',
                builder: [sfField, simpleTransclusion, condition],
                replace: false
            },
            conditional: {
                template: base + 'section/section.decorator.html',
                builder: [sfField, simpleTransclusion, condition],
                replace: false
            },
            actions: {
                template: base + 'actions/actions.decorator.html',
                builder: defaults,
                replace: false
            },
            select: {
                template: base + 'select/select.decorator.html',
                builder: defaults,
                replace: false
            },
            checkbox: {
                template: base + 'checkbox/checkbox.decorator.html',
                builder: defaults,
                replace: false
            },
            checkboxes: {
                template: base + 'checkboxes/checkboxes.decorator.html',
                builder: [sfField, ngModelOptions, ngModel, array, condition],
                replace: false
            },
            number: {
                template: base + 'default/default.decorator.html',
                builder: defaults,
                replace: false
            },
            password: {
                template: base + 'default/default.decorator.html',
                builder: defaults,
                replace: false
            },
            submit: {
                template: base + 'submit/submit.decorator.html',
                builder: defaults,
                replace: false
            },
            button: {
                template: base + 'submit/submit.decorator.html',
                builder: defaults,
                replace: false
            },
            radios: {
                template: base + 'radios/radios.decorator.html',
                builder: defaults,
                replace: false
            },
            'radios-inline': {
                template: base + 'radios-inline/radios-inline.decorator.html',
                builder: defaults,
                replace: false
            },
            radiobuttons: {
                template: base + 'radio-buttons/radio-buttons.decorator.html',
                builder: defaults,
                replace: false
            },
            help: {
                template: base + 'help/help.decorator.html',
                builder: defaults,
                replace: false
            },
            accordion: {
                template: base + 'accordion/accordion.decorator.html',
                builder: defaults,
                replace: false
            },
            'kendo-combo-box': {
                template: base + 'kendo-combo-box/kendo-combo-box.decorator.html',
                builder: defaults,
                replace: false
            },
            // Note: kendo-auto-complete is using kendo-combo-box decorator and directives
            'kendo-auto-complete': {
                template: base + 'kendo-combo-box/kendo-combo-box.decorator.html',
                builder: defaults,
                replace: false
            },
            'kendo-drop-down-list': {
                template: base + 'kendo-drop-down-list/kendo-drop-down-list.decorator.html',
                builder: defaults,
                replace: false
            },
            'kendo-multi-select': {
                template: base + 'kendo-multi-select/kendo-multi-select.decorator.html',
                builder: defaults,
                replace: false
            },
            // Note: kendo-date-picker is using kendo-date-time-picker decorator and directives
            'kendo-date-picker': {
                template: base + 'kendo-date-time-picker/kendo-date-time-picker.decorator.html',
                builder: defaults,
                replace: false
            },
            // Note: kendo-time-picker is using kendo-date-time-picker decorator and directives
            'kendo-time-picker': {
                template: base + 'kendo-date-time-picker/kendo-date-time-picker.decorator.html',
                builder: defaults,
                replace: false
            },
            'kendo-date-time-picker': {
                template: base + 'kendo-date-time-picker/kendo-date-time-picker.decorator.html',
                builder: defaults,
                replace: false
            },
            'kendo-numeric-text-box': {
                template: base + 'kendo-numeric-text-box/kendo-numeric-text-box.decorator.html',
                builder: defaults,
                replace: false
            },
            'kendo-editor': {
                template: base + 'kendo-editor/kendo-editor.decorator.html',
                builder: defaults,
                replace: false
            },
            'ticket-description': {
                template: base + 'ticket-description/ticket-description.decorator.html',
                builder: defaults,
                replace: false
            },
            'template-picker': {
                template: base + 'template-picker/template-picker.decorator.html',
                builder: defaults,
                replace: false
            },
            'kendo-image-upload': {
                template: base + 'kendo-image-upload/kendo-image-upload.decorator.html',
                builder: defaults,
                replace: false
            }
        }, []);
    }
})();