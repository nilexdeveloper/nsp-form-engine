/**
 * This file overrides sfErrorMessage from Angular Schema Form and
 * it enables translation of error messages (using NSP.Translate.t method).
 */

angular.module('schemaForm').provider('sfErrorMessage', function() {

  // The codes are tv4 error codes.
  // Not all of these can actually happen in a field, but for
  // we never know when one might pop up so it's best to cover them all.

  // TODO: Humanize these.
  var defaultMessages = {
    'default': NSP.Translate.t('SchemaFormValidate'),
    0: NSP.Translate.t('SchemaFormInvalidType'),
    1: NSP.Translate.t('SchemaFormNoEnumMatch'),
    10: NSP.Translate.t('SchemaFormDataDoesNotMatchAnyOf'),
    11: NSP.Translate.t('SchemaFormDataDoesNotMatchOneOf'),
    12: NSP.Translate.t('SchemaFormDataIsValidAgainstOneOf'),
    13: NSP.Translate.t('SchemaFormDataMatchesSchemFromNot'),
    // Numeric errors
    100: NSP.Translate.t('SchemaFormNotMultipleOf'),
    101: NSP.Translate.t('SchemaFormValueIsLessThan'),
    102: NSP.Translate.t('SchemaFormValueIsEqualThan'),
    103: NSP.Translate.t('SchemaFormValueIsGreaterThan'),
    104: NSP.Translate.t('SchemaFormValueIsEqualTo'),
    105: NSP.Translate.t('SchemaFormNotAValidNumber'),
    // String errors
    200: NSP.Translate.t('SchemaFormStringTooShort'),
    201: NSP.Translate.t('SchemaFormStringTooLong'),
    202: NSP.Translate.t('SchemaFormStringDoesNotMatchPattern'),
    // Object errors
    300: NSP.Translate.t('SchemaFormTooFewProperties'),
    301: NSP.Translate.t('SchemaFormTooManyProperties'),
    302: NSP.Translate.t('SchemaFormRequired'),
    303: NSP.Translate.t('SchemaFormAdditionalPropertiesNotAllowed'),
    304: NSP.Translate.t('SchemaFormDependencyFailed'),
    // Array errors
    400: NSP.Translate.t('SchemaFormArrayTooShort'),
    401: NSP.Translate.t('SchemaFormArrayTooLong'),
    402: NSP.Translate.t('SchemaFormArrayItemsNotUnique'),
    403: NSP.Translate.t('SchemaFormAdditionalItemsNotAllowed'),
    // Format errors
    500: NSP.Translate.t('SchemaFormValidationFailed'),
    501: NSP.Translate.t('SchemaFormKeywordFailed'),
    // Schema structure
    600: NSP.Translate.t('SchemaFormCurcular'),
    // Non-standard validation options
    1000: NSP.Translate.t('SchemaFormUnknowProperty')
  };

  // In some cases we get hit with an angular validation error
  defaultMessages.number    = defaultMessages[105];
  defaultMessages.required  = defaultMessages[302];
  defaultMessages.min       = defaultMessages[101];
  defaultMessages.max       = defaultMessages[103];
  defaultMessages.maxlength = defaultMessages[201];
  defaultMessages.minlength = defaultMessages[200];
  defaultMessages.pattern   = defaultMessages[202];

  this.setDefaultMessages = function(messages) {
    defaultMessages = messages;
  };

  this.getDefaultMessages = function() {
    return defaultMessages;
  };

  this.setDefaultMessage = function(error, msg) {
    defaultMessages[error] = msg;
  };

  this.$get = ['$interpolate', function($interpolate) {

    var service = {};
    service.defaultMessages = defaultMessages;

    /**
     * Interpolate and return proper error for an eror code.
     * Validation message on form trumps global error messages.
     * and if the message is a function instead of a string that function will be called instead.
     * @param {string} error the error code, i.e. tv4-xxx for tv4 errors, otherwise it's whats on
     *                       ngModel.$error for custom errors.
     * @param {Any} value the actual model value.
     * @param {Any} viewValue the viewValue
     * @param {Object} form a form definition object for this field
     * @param  {Object} global the global validation messages object (even though its called global
     *                         its actually just shared in one instance of sf-schema)
     * @return {string} The error message.
     */
    service.interpolate = function(error, value, viewValue, form, global) {
      global = global || {};
      var validationMessage = form.validationMessage || {};

      // Drop tv4 prefix so only the code is left.
      if (error.indexOf('tv4-') === 0) {
        error = error.substring(4);
      }

      // First find apropriate message or function
      var message = validationMessage['default'] || global['default'] || '';

      [validationMessage, global, defaultMessages].some(function(val) {
        if (angular.isString(val) || angular.isFunction(val)) {
          message = val;
          return true;
        }
        if (val && val[error]) {
          message = val[error];
          return true;
        }
      });

      var context = {
        error: error,
        value: value,
        viewValue: viewValue,
        form: form,
        schema: form.schema,
        title: form.title || (form.schema && form.schema.title)
      };
      if (angular.isFunction(message)) {
        return message(context);
      } else {
        return $interpolate(message)(context);
      }
    };

    return service;
  }];

});
