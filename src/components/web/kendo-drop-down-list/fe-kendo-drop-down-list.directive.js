(function() {
    'use strict';
    angular.module('formEngine').directive('feKendoDropDownList', feKendoDropDownList);

    feKendoDropDownList.$inject = ['$templateCache', '$http', '$q', '$timeout', 'feService'];

    function feKendoDropDownList($templateCache, $http, $q, $timeout, feService) {
        var directive = {
                link: link,
                templateUrl: '/web/kendo-drop-down-list/fe-kendo-drop-down-list.template.html',
                restrict: 'AE',
                require: 'ngModel',
                scope: {
                    options: '=feOptions',
                    ngModel: '=ngModel'
                }
            };

        return directive;

        function link(scope, element, attrs, ngModelCtrl) {

            var defaultOptions = {
                    kendoOptions: {
                        optionLabel: feService.translate('Select...', 'SelectDotDotDot'),
                        enable: true,
                        autoBind: false
                    }
                },
                sfFormElement = element.closest('[sf-form]'),
                sfFormScope = sfFormElement.scope(),
                dataValueField = 'id',
                dataTextField = 'title';

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = feService.translate;
            scope.widgetModel = angular.copy(scope.ngModel);
            scope.changeHandler = changeHandler;
            scope.clearHandler = clearHandler;
            scope.setValue = setValue;
            dataValueField = scope.settings.kendoOptions.dataValueField;
            dataTextField = scope.settings.kendoOptions.dataTextField;

            if (feService.propertyExists(scope.settings, 'kendoOptions.dataSource.transport.read') &&
                feService.propertyExists(scope.settings, 'kendoOptions.dataSource.serverFiltering') &&
                scope.settings.kendoOptions.dataSource.serverFiltering &&
                !feService.propertyExists(scope.settings, 'kendoOptions.dataSource.transport.parameterMap')) {
                scope.settings.kendoOptions.dataSource.transport.parameterMap = function(data, type) {
                    return feService.autoCompleteParameterMap(scope.settings, data);
                };
            }

            if (scope.ngModel && !angular.isUndefined(scope.ngModel[dataValueField]) && scope.ngModel[dataValueField] !== null && scope.ngModel[dataValueField].toString()) {
                scope.settings.kendoOptions.value = scope.ngModel[dataValueField];
                scope.settings.kendoOptions.text = scope.ngModel[dataTextField];
            } else {
                ngModelCtrl.$setViewValue(undefined);
                scope.ngModel = undefined;
            }

            if (scope.settings.kendoOptions && !scope.settings.kendoOptions.template && scope.settings.kendoOptions.templateCache) {
                scope.settings.kendoOptions.template = $templateCache.get(scope.settings.kendoOptions.templateCache);
            }

            if (scope.settings.kendoOptions && scope.settings.readonly === true) {
                scope.settings.kendoOptions.enable = false;
            }

            if (scope.settings.kendoOptions && scope.settings.kendoOptions.optionLabelKey) {
                scope.settings.kendoOptions.optionLabel = feService.translate(scope.settings.kendoOptions.optionLabel, scope.settings.kendoOptions.optionLabelKey);
            }

            scope.$on('kendoWidgetCreated', function(event) {
                scope.widget.bind('change', changeHandler);
                if (!scope.settings.kendoOptions.filter) {
                    scope.widget.bind('dataBound', dropDownSearchFieldOption);
                }
                if (feService.propertyExists(scope.settings, 'kendoOptions.dataSource.transport.read')) {
                    scope.widget.bind('dataBound', initialDataBound);
                }                
            });

            scope.$on('$destroy', function() {
                if (scope.settings.condition) {
                    ngModelCtrl.$setViewValue(undefined);
                }
            });

            scope.$on('setValue', function(event, dataItem) {
                setValue(dataItem);
            });

            feService.updateDependants(scope.settings, scope.ngModel, sfFormElement);

            function changeHandler() {
                var widgetValue = scope.widget.value(),
                    dataItem = scope.widget.dataItem(),
                    modelValue = scope.ngModel ? angular.copy(scope.ngModel) : {};

                if (widgetValue && dataItem) {
                    modelValue[dataValueField] = dataItem[dataValueField];
                    modelValue[dataTextField] = dataItem[dataTextField];
                    if (scope.settings.kendoOptions.dataAdditionalFields) {
                        angular.forEach(scope.settings.kendoOptions.dataAdditionalFields, function(additionalField, key) {
                            modelValue[additionalField] = dataItem[additionalField];
                        });
                    }
                } else {
                    modelValue = undefined;
                }
                ngModelCtrl.$setViewValue(modelValue);
                if (scope.settings.kendoOptions.dependants && scope.settings.kendoOptions.dependants.length) {
                    feService.updateDependants(scope.settings, modelValue, sfFormElement);
                }
            }

            function setValue(dataItem) {
                var widgetData = scope.widget.dataSource.data(),
                    itemExists = false;

                angular.forEach(widgetData, function(item, key) {
                    if (item[dataValueField] === dataItem[dataValueField]) {
                        itemExists = true;
                    }
                });

                if (!itemExists) {
                    scope.widget.dataSource.add(dataItem);
                }

                ngModelCtrl.$setViewValue(dataItem);
                scope.widget.value(dataItem[dataValueField]);
                scope.$emit('schemaFormValidate');
                feService.updateDependants(scope.settings, ngModelCtrl.$viewValue, sfFormElement);
            }

            function dropDownSearchFieldOption(event) {
                var total = event.sender.dataSource.total();
                scope.widget.setOptions({
                    filter: total > 10 ? "contains" : "none"
                });
                scope.widget.unbind('dataBound', dropDownSearchFieldOption);
                if (total > 10) {
                    scope.widget.refresh();
                }
            }

            function initialDataBound() {
                var data = scope.widget.dataSource.data(),
                    modelValueFound = false;
                if (scope.ngModel && scope.ngModel[dataValueField] && scope.ngModel[dataValueField]) {
                    if (data && data.length) {
                        for (var i = 0; i < data.length; i++) {
                            var dataItem = data[i];
                            if (dataItem[dataValueField] === scope.ngModel[dataValueField]) {
                                modelValueFound = true;
                                break;
                            }
                        }
                    }
                    if (!modelValueFound) {
                        scope.widget.dataSource.insert(0, scope.ngModel);
                        scope.widget.value(scope.ngModel[dataValueField]);
                    }
                }
                scope.widget.unbind('dataBound', initialDataBound);
            }

            function clearHandler() {
                ngModelCtrl.$setViewValue(undefined);
                scope.widgetModel = null;
                scope.widget.value('');
                feService.updateDependants(scope.settings, ngModelCtrl.$viewValue, sfFormElement);
            }

        }
    }

})();