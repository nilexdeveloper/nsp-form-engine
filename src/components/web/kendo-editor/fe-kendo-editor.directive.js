(function() {
    'use strict';
    angular.module('formEngine').directive('feKendoEditor', feKendoEditor);

    feKendoEditor.$inject = ['$templateCache', '$http', '$q', '$timeout', 'feService'];

    function feKendoEditor($templateCache, $http, $q, $timeout, feService) {
        var directive = {
                link: link,
                templateUrl: '/web/kendo-editor/fe-kendo-editor.template.html',
                restrict: 'AE',
                require: 'ngModel',
                scope: {
                    options: '=feOptions',
                    ngModel: '=ngModel'
                }
            };

        return directive;

        function link(scope, element, attrs, ngModelCtrl) {

            var defaultOptions = {
                    kendoOptions: {
                        encoded: true,
                        resizable: {
                            min: 150
                        }
                    }
                };

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = feService.translate;

            if (scope.settings.kendoOptions && scope.settings.readonly === true) {
                scope.settings.kendoOptions.enable = false;
            }

            if (scope.ngModel) {
                scope.widgetValue = scope.ngModel;
                if (scope.settings.kendoOptions.encoded) {
                    ngModelCtrl.$setViewValue(feService.htmlEncode(scope.ngModel));
                }
            }

            scope.$on('kendoWidgetCreated', function(event) {
                scope.widget.bind('change', change);
            });

            function change(event) {
                if (scope.widgetValue) {
                    ngModelCtrl.$setViewValue(scope.widget.textarea.val());
                } else {
                    ngModelCtrl.$setViewValue(undefined);
                }
            }

        }
    }

})();