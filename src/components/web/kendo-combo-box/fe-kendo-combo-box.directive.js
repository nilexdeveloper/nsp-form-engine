(function() {
    'use strict';
    angular.module('formEngine').directive('feKendoComboBox', feKendoComboBox);

    feKendoComboBox.$inject = ['$templateCache', '$http', '$q', '$timeout', 'feService', 'kendoWindowService'];

    function feKendoComboBox($templateCache, $http, $q, $timeout, feService, kendoWindowService) {
        var directive = {
                link: link,
                templateUrl: '/web/kendo-combo-box/fe-kendo-combo-box.template.html',
                restrict: 'AE',
                require: 'ngModel',
                scope: {
                    options: '=feOptions',
                    ngModel: '=ngModel'
                }
            };

        return directive;

        function link(scope, element, attrs, ngModelCtrl) {
            var defaultOptions = {
                    allowNewValue: false,
                    kendoOptions: {
                        enable: true,
                        filter: 'contains',
                        text: '',
                        autoBind: false
                    }
                },
                sfFormElement = element.closest('[sf-form]'),
                sfFormScope = sfFormElement.scope(),
                dataValueField = 'id',
                dataTextField = 'title';

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = feService.translate;
            scope.widgetModel = angular.copy(scope.ngModel);
            scope.changeHandler = changeHandler;
            scope.clearHandler = clearHandler;
            scope.setValue = setValue;
            scope.clearButton = false;
            dataValueField = scope.settings.kendoOptions.dataValueField;
            dataTextField = scope.settings.kendoOptions.dataTextField;

            if (feService.propertyExists(scope.settings, 'kendoOptions.dataSource.transport.read') &&
                feService.propertyExists(scope.settings, 'kendoOptions.dataSource.serverFiltering') &&
                scope.settings.kendoOptions.dataSource.serverFiltering &&
                !feService.propertyExists(scope.settings, 'kendoOptions.dataSource.transport.parameterMap')) {
                scope.settings.kendoOptions.dataSource.transport.parameterMap = function(data, type) {
                    return feService.autoCompleteParameterMap(scope.settings, data);
                };
            }

            if (scope.ngModel && !angular.isUndefined(scope.ngModel[dataValueField]) && scope.ngModel[dataValueField] !== null && scope.ngModel[dataValueField].toString()) {
                scope.settings.kendoOptions.value = scope.ngModel[dataValueField];
                scope.settings.kendoOptions.text = scope.ngModel[dataTextField];
                scope.clearButton = true;
            } else {
                ngModelCtrl.$setViewValue(undefined);
                scope.ngModel = undefined;
            }

            if (scope.settings.kendoOptions && !scope.settings.kendoOptions.template && scope.settings.kendoOptions.templateCache) {
                scope.settings.kendoOptions.template = $templateCache.get(scope.settings.kendoOptions.templateCache);
            }

            if (scope.settings.kendoOptions && scope.settings.readonly === true) {
                scope.settings.kendoOptions.enable = false;
            }

            if (scope.settings.placeholder && scope.settings.kendoOptions && !scope.settings.kendoOptions.placeholder) {
                if (scope.settings.isTranslatedLabels) {
                    scope.settings.kendoOptions.placeholder = scope.settings.placeholder;
                } else {
                    scope.settings.kendoOptions.placeholder = translate(scope.settings.placeholder, scope.settings.placeholderKey);
                }
            }

            scope.$on('kendoWidgetCreated', function(event) {
                scope.widget.bind('change', changeHandler);
                if (feService.propertyExists(scope.settings, 'kendoOptions.dataSource.transport.read')) {
                    scope.widget.bind('dataBound', initialDataBound);
                }
                element.find('.form-control.k-input').on('focusout', focusOutHandler);
                element.find('.form-control.k-input').on('keyup', function() {
                    var value = $(this).val();
                    if (scope.settings.required) {
                        ngModelCtrl.$setValidity('tv4-302', value ? true : false);
                    }
                    scope.clearButton = value ? true : false;
                    $timeout(function(){
                        scope.$digest();
                    }, 0);
                });
            });

            scope.$on('$destroy', function() {
                if (scope.settings.condition) {
                    ngModelCtrl.$setViewValue(undefined);
                }
            });

            scope.$on('setValue', function(event, dataItem) {
                setValue(dataItem);
            });

            feService.updateDependants(scope.settings, scope.ngModel, sfFormElement);

            function changeHandler() {
                var widgetValue = scope.widget.value(),
                    inputValue = element.find('.form-control.k-input').val(),
                    dataItem = scope.widget.dataItem(),
                    modelValue = scope.ngModel ? angular.copy(scope.ngModel) : {};

                if (widgetValue && dataItem) {
                    modelValue[dataValueField] = dataItem[dataValueField];
                    modelValue[dataTextField] = dataItem[dataTextField];
                    if (scope.settings.kendoOptions.dataAdditionalFields) {
                        angular.forEach(scope.settings.kendoOptions.dataAdditionalFields, function(additionalField, key) {
                            modelValue[additionalField] = dataItem[additionalField];
                        });
                    }
                    scope.clearButton = true;
                } else if (scope.settings.allowNewValue && (widgetValue || inputValue)) {
                    modelValue[dataValueField] = 0;
                    modelValue[dataTextField] = inputValue;
                    scope.clearButton = true;
                } else {
                    scope.clearButton = false;
                    modelValue = undefined;
                    scope.widget.value('');
                }
                ngModelCtrl.$setViewValue(modelValue);
                if (scope.settings.kendoOptions.dependants && scope.settings.kendoOptions.dependants.length) {
                    feService.updateDependants(scope.settings, modelValue, sfFormElement);
                }
                scope.$emit('schemaFormValidate');
            }

            function setValue(dataItem) {
                var widgetData = scope.widget.dataSource.data(),
                    itemExists = false;

                angular.forEach(widgetData, function(item, key) {
                    if (item[dataValueField] === dataItem[dataValueField]) {
                        itemExists = true;
                    }
                });

                if (!itemExists) {
                    scope.widget.dataSource.add(dataItem);
                }

                ngModelCtrl.$setViewValue(dataItem);
                scope.widget.value(dataItem[dataValueField]);
                scope.$emit('schemaFormValidate');
                feService.updateDependants(scope.settings, ngModelCtrl.$viewValue, sfFormElement);
                scope.clearButton = true;
            }

            function initialDataBound() {
                var data = scope.widget.dataSource.data(),
                    modelValueFound = false;
                if (scope.ngModel && scope.ngModel[dataValueField] && scope.ngModel[dataValueField]) {
                    if (data && data.length) {
                        for (var i = 0; i < data.length; i++) {
                            var dataItem = data[i];
                            if (dataItem[dataValueField] === scope.ngModel[dataValueField]) {
                                modelValueFound = true;
                                break;
                            }
                        }
                    }
                    if (!modelValueFound) {
                        scope.widget.dataSource.insert(0, scope.ngModel);
                        scope.widget.value(scope.ngModel[dataValueField]);
                    }
                }
                scope.widget.unbind('dataBound', initialDataBound);
            }

            function clearHandler(validate) {
                ngModelCtrl.$setViewValue(undefined);
                scope.widgetModel = null;
                scope.widget.value('');
                scope.clearButton = false;
                feService.updateDependants(scope.settings, ngModelCtrl.$viewValue, sfFormElement);
                if (validate) {
                    scope.$emit('schemaFormValidate');
                }
            }

            function focusOutHandler(event) {
                var inputValue = element.find('.form-control.k-input').val(),
                    modelText = ngModelCtrl.$viewValue ? ngModelCtrl.$viewValue[dataTextField] : "";
                if (inputValue && !modelText) {
                    scope.widget.trigger('change');
                }
            }
            
            scope.browseWindow = function() {
                if (scope.settings.browse.url) {
                    var dialogId = 'formEngineBrowseWindow',
                        windowDefaultOptions = {
                            title: scope.settings.browse.title ? feService.translate(scope.settings.browse.title, scope.settings.browse.titleKey) : feService.translate('Browse', 'Browse'),
                            modal: true,
                            width: 800,
                            maxHeight: 400,
                            height: "90%",
                            content: {
                                url: scope.settings.browse.url
                            }
                        },
                        windowSettings = angular.merge(windowDefaultOptions, scope.settings.browse.windowOptions);

                    var windowData = {
                        form: scope.settings,
                        model: ngModelCtrl.$viewValue
                    };

                    if ($templateCache.get(scope.settings.browse.url)) {
                        delete windowSettings.content.url;
                        windowSettings.content.template = $templateCache.get(scope.options.browse.url);
                    }

                    kendoWindowService.create(dialogId, windowData, windowSettings);
                } else {
                    throw new Error("Form Engine: Browse window url undefined.");
                }
            };

        }
    }

})();