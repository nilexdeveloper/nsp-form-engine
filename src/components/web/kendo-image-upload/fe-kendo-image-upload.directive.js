(function() {
    'use strict';
    angular.module('formEngine').directive('feKendoImageUpload', feKendoImageUpload);

    /* @ngInject */
    function feKendoImageUpload($templateCache, $http, $q, $timeout, $compile, feService) {
        var directive = {
            link: link,
            restrict: 'AE',
            require: 'ngModel',
            scope: {
                options: '=feOptions',
                ngModel: '=ngModel'
            }
        };
        return directive;

        function link(scope, element, attrs, ngModelCtrl) {

            var defaultOptions = {
                template: '/web/kendo-image-upload/fe-kendo-image-upload.template.html',
                allowedExtensions: ['jpg', 'jpeg', 'png', 'ico'], 
                kendoOptions: {
                    multiple: false,
                    showFileList: false,
                    async: {
                        saveUrl: '/NSPFormEngine/SaveBlobImage',
                        saveField: 'blobImages',
                        autoUpload: true
                    },
                    localization: {
                        select: feService.translate("Select files...", 'Common.SelectFilesDotDotDot')
                    }
                },
                validationMessage: {}
            };

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = feService.translate;
            scope.remove = remove;
            scope.browseFile = browseFile;
            scope.uploading = false;

            if (!scope.options.validationMessage) {
                if (!scope.settings.validationMessage.extensionNotValid) {
                    scope.settings.validationMessage.extensionNotValid = feService.translate('File type not valid. Allowed extensions', 'Common.FileTypeNotValid') + ': ' + scope.settings.allowedExtensions.join(', ') + '.';
                }
                if (!scope.settings.validationMessage.uploadFailed) {
                    scope.settings.validationMessage.uploadFailed = feService.translate('Upload failed.', 'Common.UploadFailed');
                }

                if (!scope.settings.validationMessage.uploadPending) {
                    scope.settings.validationMessage.uploadPending = feService.translate('Upload pending.', 'Common.UploadPending');
                }
            }

            scope.options.validationMessage = scope.settings.validationMessage;

            if (scope.ngModel && scope.ngModel.id) {
                scope.selectedImage = angular.copy(scope.ngModel);
            }

            scope.$on('kendoWidgetCreated', function(event) {  
                scope.widget.bind('select', onSelect);
                scope.widget.bind('success', onSuccess);
                scope.widget.bind('error', onError);
            });

            scope.$on('$destroy', function() {
                if (scope.settings.condition) {
                    ngModelCtrl.$setViewValue(undefined);
                }
            });

            scope.$on('schemaFormValidate', function(event) {
                // Set uploadPending error only if form has been submitted
                if (event.targetScope.$id !== scope.$id) {
                    ngModelCtrl.$setValidity('uploadPending', !scope.uploading);
                }
            });

            element.html($templateCache.get(scope.settings.template));
            $compile(element.contents())(scope);

            function browseFile() {
                element.find('input[type="file"]').trigger('click');
            }

            function onSelect(event) {
                var files = event.files;
                angular.forEach(files, function(file, key) {
                    scope.selectedImage = {
                        name: file.name
                    };
                    if (validateFileExtension(file)) {
                        ngModelCtrl.$setViewValue(scope.selectedImage);
                        ngModelCtrl.$setValidity('extensionNotValid', true);
                        scope.uploading = true;
                    } else {
                        ngModelCtrl.$setViewValue(scope.selectedImage);
                        ngModelCtrl.$setValidity('extensionNotValid', false);
                        event.preventDefault();
                        scope.uploading = false;
                    }
                });
                ngModelCtrl.$setValidity('uploadFailed', true);
                scope.$emit('schemaFormValidate');
            }

            function onSuccess(event) {
                var response = event.response;
                if (scope.selectedImage) {
                    scope.selectedImage.id = response.pathToBeStored;
                    scope.selectedImage.preview = response.pathToBeDisplay;
                }
                ngModelCtrl.$setValidity('uploadPending', true);
                ngModelCtrl.$setViewValue(scope.selectedImage);
                scope.uploading = false;
                $timeout(function() {
                    scope.$digest();
                });
            }

            function validateFileExtension(file) {
                var valid = false,
                    extension = file.extension.replace('.', ''),
                    extensions = {};
                for (var i = 0; i < scope.settings.allowedExtensions.length; i++) {
                    extensions[scope.settings.allowedExtensions[i]] = 1;
                }
                if (!!extensions[extension]) {
                    valid = true;
                } else {
                    valid = false;
                }
                return valid;
            }

            function remove() {
                scope.selectedImage = null;
                ngModelCtrl.$setValidity('extensionNotValid', true);
                ngModelCtrl.$setValidity('uploadFailed', true);
                ngModelCtrl.$setViewValue(undefined);
                element.find('.k-upload-action .k-cancel').trigger('click');
                scope.uploading = false;
                scope.$emit('schemaFormValidate');
            }

            function onError(event) {
                ngModelCtrl.$setValidity('uploadFailed', false);
                scope.uploading = false;
                scope.$emit('schemaFormValidate');
            }
        }
    }
})();