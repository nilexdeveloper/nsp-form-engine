(function() {
    'use strict';
    angular.module('formEngine').directive('feKendoNumericTextBox', feKendoNumericTextBox);

    feKendoNumericTextBox.$inject = ['$templateCache', '$http', '$q', '$timeout', 'feService'];

    function feKendoNumericTextBox($templateCache, $http, $q, $timeout, feService) {
        var directive = {
                link: link,
                templateUrl: '/web/kendo-numeric-text-box/fe-kendo-numeric-text-box.template.html',
                restrict: 'AE',
                require: 'ngModel',
                scope: {
                    options: '=feOptions',
                    ngModel: '=ngModel'
                }
            };

        return directive;

        function link(scope, element, attrs, ngModelCtrl) {

            var defaultOptions = {
                    kendoOptions: {
                        format: "{n:0}",
                        decimals: 0,
                        spinners: false,
                        step: 1,
                        selectOnFocus: false
                    }
                };

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = feService.translate;
            scope.widgetModel = "";
            scope.changeHandler = changeHandler;

            if (scope.settings.kendoOptions && scope.settings.readonly === true) {
                scope.settings.kendoOptions.enable = false;
            }

            if (scope.ngModel !== null && !angular.isUndefined(scope.ngModel) && scope.ngModel.toString()) {
                scope.widgetModel = scope.ngModel;
            } else {
                scope.ngModel = undefined;
                ngModelCtrl.$setViewValue(undefined);
            }

            if (scope.settings.placeholder && scope.settings.kendoOptions && !scope.settings.kendoOptions.placeholder) {
                if (scope.settings.isTranslatedLabels) {
                    scope.settings.kendoOptions.placeholder = scope.settings.placeholder;
                } else {
                    scope.settings.kendoOptions.placeholder = translate(scope.settings.placeholder, scope.settings.placeholderKey);
                }
            }

            scope.$on('$destroy', function() {
                if (scope.settings.condition) {
                    ngModelCtrl.$setViewValue(undefined);
                }
            });

            ngModelCtrl.$viewChangeListeners.push(function() {
                scope.widgetModel = ngModelCtrl.$viewValue;
            });

            scope.$on('kendoWidgetCreated', function(event) {
                var input = element.find('.k-input');
                scope.widget.bind('change', changeHandler);
                if (feService.propertyExists(scope.settings, 'kendoOptions.selectOnFocus') && scope.settings.kendoOptions.selectOnFocus) {
                    element.find('.k-input').on('focus', function(event) {
                        var that = $(this);
                        var selectTimeId = setTimeout(function() {
                            that.select();
                        });
                    }).blur(function(e) {
                        clearTimeout($(this).data("selectTimeId"));
                    });
                }
            });

            function changeHandler() {
                var widgetValue = scope.widget.value(),
                    modelValue;

                if (widgetValue !== null) {
                    modelValue = widgetValue.toString();
                } else {
                    modelValue = undefined;
                }
                
                ngModelCtrl.$setViewValue(modelValue);
            }

        }
    }

})();