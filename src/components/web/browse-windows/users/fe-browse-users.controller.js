(function() {
    "use strict";
    
    angular
        .module('formEngine')
        .controller('FeBrowseUsersController', FeBrowseUsersController);

    /* @ngInject */
    function FeBrowseUsersController($scope, $templateCache, $timeout, nspCoreService, kendoWindowService) {
        var vm = this,
            dialogId = 'formEngineBrowseWindow',
            dialog = $('#' + dialogId).data('kendoWindow'),
            dialogData = kendoWindowService.read(dialogId),
            filters = dialogData.form.browse.initialFilters ? dialogData.form.browse.initialFilters : [];

        vm.selectedRow = null;

        if (dialogData.model) {
            vm.selectedRow = {
                id: dialogData.model[dialogData.form.kendoOptions.dataValueField],
                name: dialogData.model[dialogData.form.kendoOptions.dataTextField]
            };
        }

        if (dialogData.form.browse && dialogData.form.browse.filterCondition && dialogData.form.browse.filterCondition.filterValue) {
            filters.push({
                field: dialogData.form.browse.filterCondition.fieldName,
                operator: 'eq',
                value: dialogData.form.browse.filterCondition.filterValue
            });
        }

        vm.grid = {
            gridId: 'feBrowseUsersGrid',
            gridOptions: {
                dataSource: {
                    transport: {
                        read: {
                            url: '/Person/GetDataSource'
                        }
                    },
                    schema: {
                        model: {
                            fields: {
                                id: {
                                    type: "number",
                                    dbType: "integer"
                                },
                                name: {
                                    type: "string",
                                    dbType: "string"
                                },
                                imagePath: {
                                    type: "string",
                                    dbType: "string"
                                },
                                mail: {
                                    type: "string",
                                    dbType: "string"
                                },
                                organizationName: {
                                    type: "string",
                                    dbType: "string"
                                }
                            }
                        }
                    },
                    filter: filters,
                    pageSize: 5
                },
                columns: [{
                    field: 'id',
                    title: nspCoreService.translate('Id', 'Id'),
                    width: 120,
                    searchable: false,
                    hidden: true
                },
                {
                    field: 'name',
                    title: nspCoreService.translate('Agent', 'Common.Agent'),
                    searchable: true,
                    template: '#= data.name ? data.name : "-" #'
                    // template: $templateCache.get('/web/browse-windows/users/fe-browse-users-row.html')
                },
                {
                    field: 'organizationName',
                    title: nspCoreService.translate('Organization', 'Common.Organization'),
                    searchable: true,
                    template: '#= data.organizationName ? data.organizationName : "-"#'
                }],
                change: function(event) {
                    var grid = event.sender,
                        row = grid.select(),
                        rowData = grid.dataItem(row);
                    vm.selectedRow = rowData;
                    $timeout(function() {
                        $scope.$digest();
                    }, 0);
                },
                dataBound: function(event) {
                    var grid = event.sender,
                        items = grid.items();
                    items.each(function(idx, row) {
                        var dataItem = grid.dataItem(row);
                        if (vm.selectedRow && dataItem.id === vm.selectedRow.id) {
                            grid.select(row);
                        }
                    });
                }
            },
            searchOptions: {
                dataSource: {
                    transport: {
                        read: {
                            url: '/Person/GetAutoCompleteDataSource'
                        }
                    }
                }
            }
        };

        vm.t = nspCoreService.translate;
        vm.select = select;
        vm.cancel = cancel;

        function select() {
            var form = dialogData.form,
                formElement = $('#' + form.key[0]),
                formElementScope = formElement.scope(),
                dataValueField = formElementScope.settings.kendoOptions.dataValueField,
                dataTextField = formElementScope.settings.kendoOptions.dataTextField,
                dataItem = {};

            dataItem[dataValueField] = vm.selectedRow.id;
            dataItem[dataTextField] = vm.selectedRow.name;

            formElementScope.$broadcast('setValue', dataItem);
            dialog.close();
        }

        function cancel() {
            dialog.close();
        }
    }

})();