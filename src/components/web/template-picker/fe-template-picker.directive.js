(function() {
    'use strict';
    angular.module('formEngine').directive('feTemplatePicker', feTemplatePicker);

    feTemplatePicker.$inject = ['$templateCache', '$http', '$q', '$timeout', 'feService', 'kendoWindowService'];

    function feTemplatePicker($templateCache, $http, $q, $timeout, feService, kendoWindowService) {
        var directive = {
            link: link,
            templateUrl: '/web/template-picker/templates/fe-template-picker.template.html',
            restrict: 'AE',
            require: 'ngModel',
            scope: {
                options: '=feOptions',
                ngModel: '=ngModel'
            }
        };

        return directive;

        function link(scope, element, attrs, ngModelCtrl) {
            var defaultOptions = {
                'readonly': true
            };
            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = feService.translate;
            scope.langId = scope.settings.currentLang;

            function setViewVal(model) {
                if (model.length === 0) {
                    scope.viewVal = '';
                } else {
                    for (var i = 0; i < model.length; i++) {
                        if (model[i].languageId === scope.settings.currentLang) {
                            scope.viewVal = model[i].subject;
                        }
                    }
                }
            }

            scope.openPicker = function() {
                var dialogId = 'templatePickerWindow',
                    windowDefaultOptions = {
                        title: scope.settings.window.title ? feService.translate(scope.settings.window.title, scope.settings.window.titleKey) : feService.translate('ChooseTemplate', 'Common.ChooseTemplate'),
                        modal: true,
                        width: 800,
                        maxHeight: 555,
                        height: "90%",
                        content: {
                            url: scope.settings.window.url
                        }
                    },
                    windowSettings = angular.merge(windowDefaultOptions, scope.settings.window.windowOptions);

                var windowData = {
                    form: scope.settings,
                    model: ngModelCtrl.$viewValue && ngModelCtrl.$viewValue.length > 0 ? ngModelCtrl.$viewValue : [{
                        "languageId": 1,
                        "languageName": "English",
                        "languageNameKey": "SysLanguage.en-US",
                        "subject": "",
                    }]
                };
                kendoWindowService.create(dialogId, windowData, windowSettings);
            };

            setViewVal(scope.ngModel);

            scope.$on('templateSet', function(event, model) {
                setViewVal(model);
                if (scope.viewVal === "" && model.length === 1) {
                    ngModelCtrl.$setViewValue(undefined);
                } else {
                    ngModelCtrl.$setViewValue(model);
                }
            });
        }
    }

})();