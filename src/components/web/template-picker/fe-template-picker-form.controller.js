(function() {
    'use strict';

    angular
        .module('formEngine')
        .controller('FeTemplatePickerFormController', FeTemplatePickerFormController);

    FeTemplatePickerFormController.$inject = ['$scope', '$rootScope', '$http', '$timeout', 'feService', 'kendoWindowService'];

    /* @ngInject */
    function FeTemplatePickerFormController($scope, $rootScope, $http, $timeout, feService, kendoWindowService) {
        var vm = this;
        vm.t = feService.translate;
        vm.dialogData = kendoWindowService.read('templatePickerWindow');
        vm.dialog = $('#templatePickerWindow').data('kendoWindow');
        vm.title = 'TemplatePickerForm';
        vm.activeLang = 0;
        vm.languages = vm.dialogData.model;

        var params = {
            parentPlaceholder: "",
            tableId: vm.dialogData.form.window.tableId,
            typeId: vm.dialogData.form.window.entityTypeId,
            tableRepository: "app.Tables"
        };

        var treeOptions = {
            dataTextField: "name",
            dataValueField: "id",
            loadOnDemand: true,
            dataSource: new kendo.data.HierarchicalDataSource({
                transport: {
                    read: {
                        url: vm.dialogData.form.window.placeholdersUrl,
                        dataType: "json",
                        data: params
                    },
                    parameterMap: function(options, type) {
                        options.parentPlaceholder = params.parentPlaceholder;
                        options.tableId = params.tableId;
                        options.typeId = params.typeId;
                        options.tableRepository = params.tableRepository;
                        return options;
                    }
                },
                schema: {
                    model: {
                        id: "id",
                        hasChildren: "hasSubPlaceholders"
                    }
                }
            }),
            expand: function(event){
                params.parentPlaceholder = this.dataItem(event.node).placeholder;
                params.tableId = this.dataItem(event.node).expandTableId;
                params.tableRepository = this.dataItem(event.node).expandRepository;
            }
        };

        $scope.treeOptions = treeOptions;

        $scope.selectTreeViewItem = function(event) {
            if (vm.activeLang === null) {
                return;
            }
            var dataItem = event.sender.dataItem(event.sender.select());
            var treeView = event.sender;            
            vm.languages[vm.activeLang].subject += dataItem.placeholder;
        };

        $scope.setActiveLang = function(index) {
            vm.activeLang = index === vm.activeLang ? null : index;
        };

        $scope.openLanguageList = function(){
            vm.langListOpened = true;
            $http({
                method: 'GET',
                url: vm.dialogData.form.window.languageListUrl ? vm.dialogData.form.window.languageListUrl : '/LanguageType/GetAllLanguagesExceptDefault',                
                headers: {
                    'Content-Type': 'application/json; charset=utf8'
                }
            }).success(function(response) {
                vm.langDefinitionList = response;
            });
        };

        $scope.addLanguageDefinition = function(language){
            for (var i = 0; i < vm.languages.length; i++) {
                if(language.id === vm.languages[i].languageId){
                    return;
                }
            }            
            vm.languages.push({
                languageId: language.id,
                languageName: language.languageName,
                languageNameKey: language.languageNameKey,
                subject: ""
            });
            vm.langListOpened = false;
        };

        $scope.removeLanguage = function(index){
            vm.languages.splice(index, 1);
        };

        function collapseDropDown(event) {
            if (vm.langListOpened && $(event.target).closest('.lang-list').length === 0 && $(event.target).closest('.add-lang-definition .label').length === 0) {
                vm.langListOpened = false;
                $timeout(function() {
                    $scope.$digest();
                }, 0);
            }
        }       

        $scope.cancel = function() {
            vm.dialog.close();
        };

        $scope.saveTemplateForm = function() {
            vm.dialogData.model = vm.languages;
            $rootScope.$broadcast('templateSet', vm.dialogData.model);
            $scope.cancel();
        };

        $(document).on('click', collapseDropDown);
        $scope.$on('$destroy', function() {
            $(document).off('click', collapseDropDown);
        });
    }
})();