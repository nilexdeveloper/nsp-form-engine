
/****************************************
    FILE: src/form-engine.module.js
****************************************/

/**
 * Form Engine
 */
angular.module('formEngine',
    []
);

/****************************************
    FILE: src/form-engine.service.js
****************************************/

(function() {
    'use strict';

    angular
        .module('formEngine')
        .factory('feService', feService);

    feService.$inject = ['$http', '$sce'];

    /* @ngInject */
    function feService($http, $sce) {
        var factory = {
            updateDependants: updateDependants,
            autoCompleteParameterMap: autoCompleteParameterMap,
            propertyExists: propertyExists,
            translate: translate,
            getContrast: getContrast,
            rgbToHex: rgbToHex,
            stripTags: stripTags,
            hierarchicalDataSource: hierarchicalDataSource,
            getMimeTypeName: getMimeTypeName,
            decodeHtml: decodeHtml,
            htmlEncode: htmlEncode,
            htmlDecode: htmlDecode,
            secondsToHoursAndMinutes: secondsToHoursAndMinutes,
            getBrowser: getBrowser
        },
        tempDocument = null, // A temporary document used for stripTags method. Value is assigned when stripTags is executed the first time.
        mimeTypes = {
            "image/jpeg": 'Common.JPEGimage',
            "image/png": 'Common.PNGimage',
            "image/bmp": 'Common.BMPimage',
            "image/tiff": 'Common.TIFFfile',
            "application/msword": 'Common.MicrosoftWord',
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document": 'Common.MicrosoftWord',
            "application/vnd.ms-excel": 'Common.MicrosoftExcel',
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet": 'Common.MicrosoftExcel',
            "application/vnd.ms-powerpoint": 'Common.MicrosoftPowerPoint',
            "application/vnd.openxmlformats-officedocument.presentationml.presentation": 'Common.MicrosoftPowerPoint',
            "text/plain": 'Common.TextFile',
            "application/pdf": 'Common.PDF',
            "application/zip": 'Common.ZipArchive',
            "application/x-rar-compressed": 'Common.RARArchive',
            "video/x-ms-wmv": 'Common.MicrosoftWindowsMediaVideo',
            "application/mp4": 'Common.MPEG4',
            "video/mp4": 'Common.MPEG-4Video',
            "audio/mp3": 'Common.MP3',
            "video/quicktime": 'Common.QuickTimeVideo',
            "text/csv": 'Common.Comma-SeperatedValues',
            "application/vnd.oasis.opendocument.text": 'Common.OpenDocumentText',
            "text/tab-separated-values": 'Common.TabSeperatedValues',
            "application/xml": 'Common.XML',
            "message/rfc822": 'Common.EmailMessage'
        };

        return factory;

        function propertyExists(obj, key) {
            return key.split(".").every(function(x) {
                if (typeof obj != "object" || obj === null || !(x in obj))
                    return false;
                obj = obj[x];
                return true;
            });
        }

        function updateDependants(settings, model, sfFormElement) {
            var dependants = settings.kendoOptions.dependants,
                dataValueField = settings.kendoOptions.dataValueField,
                dataTextField = settings.kendoOptions.dataTextField,
                filterCondition = null,
                sfFormScope = sfFormElement.scope(),
                sfFormAttribute = sfFormElement.attr('sf-form'),
                sfModelAttribute = sfFormElement.attr('sf-model'),
                sfSchemaAttribute = sfFormElement.attr('sf-schema'),
                sfFormName = sfFormElement.attr('name');

            angular.forEach(dependants, function(dependant, key) {

                var dependantElement = $('#' + dependant.key),
                    dependantScope = dependantElement ? dependantElement.scope() : null,
                    dependantWidget = kendo.widgetInstance(dependantElement),
                    updatedSfFormScope = null;

                filterCondition = {
                    fieldId: dependant.filterFieldId,
                    fieldName: dependant.filterFieldName,
                    isStatic: dependant.filterFieldIsStatic,
                    filterValue: ''
                };

                if (model && model[dataTextField] && model[dataTextField]) {
                    filterCondition.filterValue = model[dataValueField];
                }

                updatedSfFormScope = updateDependantScope(sfFormScope[sfFormAttribute], dependant.key, filterCondition);

                if (dependantScope) {
                    dependantScope.settings = updatedSfFormScope.dependant;
                }

                sfFormScope[sfFormAttribute] = updatedSfFormScope.form;

                if (sfFormScope[sfFormName].$dirty) {
                    sfFormScope[sfModelAttribute][dependant.key] = undefined;
                    if (dependantWidget && dependantWidget.options.dataSource) {
                        dependantScope.clearHandler(false);
                    }
                }

            });
        }

        function updateDependantScope(formData, key, filterCondition) {
            var dependant = null;
            function iterateForm(data, keyValue) {
                for (var i = 0; i < data.length; i++) {
                    var current = data[i];
                    if (current.key && current.key.slice(-1)[0] === key) {
                        dependant = data[i];
                        if (dependant.kendoOptions.dataSource && dependant.kendoOptions.dataSource.transport.read) {
                            if (!dependant.kendoOptions.dataSource.transport.read.data) {
                                dependant.kendoOptions.dataSource.transport.read.data = {};
                            }
                            if(filterCondition.filterValue){
                                dependant.kendoOptions.dataSource.transport.read.data.dependencyFilterConditions = JSON.stringify([filterCondition]);
                            }else{
                                dependant.kendoOptions.dataSource.transport.read.data.dependencyFilterConditions = [];
                            }                            
                        }
                        if (dependant.browse) {
                            dependant.browse.filterCondition = filterCondition;
                        }
                        break;
                    }
                    if (current.items && current.items.length) {
                        iterateForm(current.items, keyValue);
                    }
                }
            }
            iterateForm(formData, key);
            dependant.kendoOptions.dataSource.transport.parameterMap = function(data, type) {
                return autoCompleteParameterMap(dependant, data);
            };
            return {
                form: formData,
                dependant: dependant
            };
        }

        function autoCompleteParameterMap(form, data) {
            var result = data;
            if (data && data.filter && data.filter.filters.length) {
              result.searchText = data.filter.filters[0].value;
              result.filterCriteria = angular.toJson(data.filter);
            } else {
              result.searchText = "";
              result.getAll = true;
            }
            if (propertyExists(form, 'kendoOptions.dataSource.pageSize')) {
              result.pageSize = form.kendoOptions.dataSource.pageSize;
            }
            return result;
        }

        function translate(text, key) {
            var result = key;
            if (NSP && NSP.Translate && NSP.Translate.t(key)) {
                result = NSP.Translate.t(key);
            }
            return result;
        }

        function getContrast(hexcolor) {
            if (hexcolor) {
                var r = parseInt(hexcolor.substr(0, 2), 16);
                var g = parseInt(hexcolor.substr(2, 2), 16);
                var b = parseInt(hexcolor.substr(4, 2), 16);
                var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
                return (yiq >= 192) ? '#565656' : '#ffffff';
            }
        }

        function rgbToHex(rgb) {
            rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);

            function hex(x) {
                return ("0" + parseInt(x).toString(16)).slice(-2);
            }

            return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
        }

        function stripTags(htmlString) {

            // tempDocument is cached for performance and created only when required
            if (!tempDocument) {
                // creates a temporary document object used for striping html
                tempDocument = document.implementation.createHTMLDocument("New");
            }

            // htmlString is inserted into tempDocument body and text is pulled out from native innerText
            var tmp = tempDocument.body;
            tmp.innerHTML = htmlString;
            var result = tmp.textContent || tmp.innerText || "";

            // Fix for Edge putting string 'null' instead of actual null
            return result == 'null' ? "" : result;
        }

        function hierarchicalDataSource(data, idField, foreignKey, rootLevel, children) {
            var hash = {};
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                var id = item[idField];
                var parentId = item[foreignKey];

                hash[id] = hash[id] || [];
                hash[parentId] = hash[parentId] || [];

                item[children ? children : 'items'] = hash[id];
                hash[parentId].push(item);
            }
            return hash[rootLevel];
        }

        function getMimeTypeName(type) {
            return mimeTypes.hasOwnProperty(type) ? translate(mimeTypes[type], mimeTypes[type]) : type;
        }

        function decodeHtml(html) {
            var txt = document.createElement("textarea");
            txt.innerHTML = html;
            return txt.value;
        }

        function htmlEncode(value) {
            return $('<div/>').text(value).html();
        }

        function htmlDecode(value){
          return $sce.trustAsHtml($('<div/>').html(value).text());
        }

        function secondsToHoursAndMinutes(seconds) {
            var result = "",
                hours = seconds ? Math.floor(seconds / 3600) : 0,
                minutes = seconds ? Math.floor((seconds - (hours * 3600)) / 60) : 0;
            if (hours.toString().length === 1) {
                hours = '0' + hours;
            }
            if (minutes.toString().length === 1) {
                minutes = '0' + minutes;
            }
            result = hours + ":" + minutes;
            return result;
        }

        function getBrowser() {
            var ua = navigator.userAgent,
                tem,
                M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
            if (/trident/i.test(M[1])) {
                tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
                return 'IE ' + (tem[1] || '');
            }
            if (M[1] === 'Chrome') {
                tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
                if (tem !== null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
            }
            M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
            if ((tem = ua.match(/version\/(\d+)/i)) !== null) M.splice(1, 1, tem[1]);
            return M.join(' ');
        }


    }
})();

/****************************************
    FILE: src/form-engine.templates.js
****************************************/

angular.module('formEngine').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('/web/accordion/accordion-header.template.html',
    "<div ng-click=\"toggle($event)\" class=\"accordion-title\" nsp-t><span class=\"f-nsp\" ng-class=\"{'f-nsp-angle-up': !settings.collapsed, 'f-nsp-angle-down': settings.collapsed}\"></span> <span>{{settings.isTranslatedLabels ? settings.title : t(settings.title, settings.titleKey)}}</span></div>"
  );


  $templateCache.put('/web/accordion/accordion.decorator.html',
    "<fieldset ng-disabled=\"form.readonly\" class=\"schema-form-accordion {{form.htmlClass}}\" ng-class=\"{'collapsed': form.collapsed}\" nsp-t><h3 accordion-header options=\"form\"></h3><div class=\"help-block\" ng-show=\"form.description\" ng-bind-html=\"form.isTranslatedLabels ? form.description : t(form.description, form.descriptionKey)\"></div><div class=\"accordion-content\"><sf-decorator ng-repeat=\"item in form.items\" form=\"item\" class=\"sf-decorator-item\"></sf-decorator></div></fieldset>"
  );


  $templateCache.put('/web/actions-trcl/actions-trcl.decorator.html',
    "<div class=\"btn-group schema-form-actions {{form.htmlClass}}\" ng-transclude></div>"
  );


  $templateCache.put('/web/actions/actions.decorator.html',
    "<div class=\"form-group btn-group schema-form-actions {{form.htmlClass}}\" nsp-t><input ng-repeat-start=\"item in form.items\" type=\"submit\" class=\"btn {{ item.style || 'btn-default' }} {{form.fieldHtmlClass}}\" value=\"{{form.isTranslatedLabels ? item.title : t(item.title, item.titleKey)}}\" ng-if=\"item.type === 'submit'\" nsp-theme=\"{'background-color': theme.color, 'color': theme.reverseColor}\"> <button ng-repeat-end class=\"btn {{ item.style || 'btn-default' }} {{form.fieldHtmlClass}}\" type=\"button\" ng-disabled=\"form.readonly\" ng-if=\"item.type !== 'submit'\" ng-click=\"buttonClick($event,item)\"><span ng-if=\"item.icon\" class=\"{{item.icon}}\"></span>{{form.isTranslatedLabels ? item.title : t(item.title, item.titleKey)}}</button></div>"
  );


  $templateCache.put('/web/array/array.decorator.html',
    "<div class=\"schema-form-array {{form.htmlClass}}\" sf-field-model=\"sf-new-array\" sf-new-array nsp-t><label class=\"control-label\" ng-show=\"showTitle()\">{{form.isTranslatedLabels ? form.title : t(form.title, form.titleKey)}}</label><ol class=\"list-group\" sf-field-model ui-sortable=\"form.sortOptions\"><li class=\"list-group-item {{form.fieldHtmlClass}}\" schema-form-array-items sf-field-model=\"ng-repeat\" ng-repeat=\"item in $$value$$ track by $index\"><button ng-hide=\"form.readonly || form.remove === null\" ng-click=\"deleteFromArray($index)\" ng-disabled=\"form.schema.minItems >= modelArray.length\" style=\"position: relative; z-index: 20\" type=\"button\" class=\"close pull-right\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">{{t('Close', 'Common.Close')}}</span></button></li></ol><div class=\"clearfix\" style=\"padding: 15px\" ng-model=\"modelArray\" schema-validate=\"form\"><div class=\"help-block\" ng-show=\"(hasError() && errorMessage(schemaError())) || form.description\" ng-bind-html=\"(hasError() && errorMessage(schemaError())) || form.description\"></div><button ng-hide=\"form.readonly || form.add === null\" ng-click=\"appendToArray()\" ng-disabled=\"form.schema.maxItems <= modelArray.length\" type=\"button\" class=\"btn {{ form.style.add || 'btn-default' }} pull-right\"><i class=\"glyphicon glyphicon-plus\"></i> {{ t(form.add, form.addKey) || t('Add', 'Common.Add')}}</button></div></div>"
  );


  $templateCache.put('/web/browse-windows/users/fe-browse-users-row.html',
    "<div class=\"browse-user-row\"><div class=\"user-details\"><h3>{{dataItem.name}}</h3><p>{{dataItem.mail}}</p></div><div class=\"user-image\" ng-class=\"{'default-image' : !dataItem.imageUrl}\"><div ng-if=\"dataItem.imageUrl\"><img ng-src=\"{{dataItem.imageUrl}}\" alt=\"User image\"></div></div></div>"
  );


  $templateCache.put('/web/browse-windows/users/fe-browse-users.html',
    "<div ng-controller=\"FeBrowseUsersController as vm\" kendo-window-content class=\"browse-window\"><div nsp-grid options=\"vm.grid\"></div><div class=\"form-actions\"><input type=\"submit\" value=\"{{vm.t('Save', 'Save')}}\" ng-disabled=\"!vm.selectedRow\" class=\"btn btn-default\" ng-click=\"vm.select()\"> <button class=\"btn btn-default\" ng-click=\"vm.cancel()\">{{vm.t('Cancel', 'Cancel')}}</button></div></div>"
  );


  $templateCache.put('/web/checkbox/checkbox.decorator.html',
    "<div class=\"form-group checkbox schema-form-checkbox {{form.htmlClass}}\" ng-class=\"{'has-error': form.disableErrorState !== true &&  hasError(), 'has-success': form.disableSuccessState !== true &&  hasSuccess(), 'required': form.required, 'read-only': form.readonly }\" ng-style=\"{'width': form.width + '%'}\" nsp-t><label class=\"{{form.labelHtmlClass}}\"><input type=\"checkbox\" sf-changed=\"form\" ng-disabled=\"form.readonly\" sf-field-model ng-model=\"$$value$$\" schema-validate=\"form\" class=\"{{form.fieldHtmlClass}}\" name=\"{{form.key.slice(-1)[0]}}\"> <span ng-bind-html=\"form.isTranslatedLabels ? form.title : t(form.title, form.titleKey)\"></span></label><div class=\"help-block\" sf-message=\"form.isTranslatedLabels ? form.description : t(form.description, form.descriptionKey)\"></div></div>"
  );


  $templateCache.put('/web/checkboxes/checkboxes.decorator.html',
    "<div sf-array=\"form\" ng-model=\"$$value$$\" class=\"form-group schema-form-checkboxes {{form.htmlClass}}\" ng-class=\"{'has-error': form.disableErrorState !== true &&  hasError(), 'has-success': form.disableSuccessState !== true &&  hasSuccess(), 'required': form.required, 'read-only': form.readonly }\" ng-style=\"{'width': form.width + '%'}\" nsp-t><label class=\"control-label {{form.labelHtmlClass}}\" ng-show=\"showTitle()\">{{form.isTranslatedLabels ? form.title : t(form.title, form.titleKey)}}</label><div class=\"checkbox\" ng-repeat=\"val in titleMapValues track by $index\"><label><input type=\"checkbox\" ng-disabled=\"form.readonly\" sf-changed=\"form\" class=\"{{form.fieldHtmlClass}}\" ng-model=\"titleMapValues[$index]\" name=\"{{form.key.slice(-1)[0]}}\"> <span ng-bind-html=\"form.isTranslatedLabels ? form.titleMap[$index].title : t(form.titleMap[$index].title, form.titleMap[$index].titleKey)\"></span></label></div><div class=\"help-block\" sf-message=\"form.isTranslatedLabels ? form.description : t(form.description, form.descriptionKey)\"></div></div>"
  );


  $templateCache.put('/web/default/default.decorator.html',
    "<div class=\"form-group {{form.htmlClass}}\" ng-class=\"{ '{{'schema-form-' + form.type}}': true, 'has-error': form.disableErrorState !== true && hasError(), 'has-success': form.disableSuccessState !== true && hasSuccess(), 'has-feedback': form.feedback !== false, 'required': form.required, 'read-only': form.readonly }\" ng-style=\"{'width': form.width + '%'}\" nsp-t><label class=\"control-label {{form.labelHtmlClass}}\" ng-class=\"{'sr-only': !showTitle()}\" for=\"{{form.key.slice(-1)[0]}}\">{{form.isTranslatedLabels ? form.title : t(form.title, form.titleKey)}}</label><input ng-if=\"!form.fieldAddonLeft && !form.fieldAddonRight\" ng-show=\"form.key\" type=\"{{form.type}}\" step=\"any\" sf-changed=\"form\" placeholder=\"{{form.isTranslatedLabels ? form.placeholder : t(form.placeholder, form.placeholderKey)}}\" class=\"form-control {{form.fieldHtmlClass}}\" id=\"{{form.key.slice(-1)[0]}}\" sf-field-model ng-model=\"$$value$$\" ng-model-options=\"form.ngModelOptions\" ng-disabled=\"form.readonly\" schema-validate=\"form\" name=\"{{form.key.slice(-1)[0]}}\" aria-describedby=\"{{form.key.slice(-1)[0] + 'Status'}}\"><div ng-if=\"form.fieldAddonLeft || form.fieldAddonRight\" ng-class=\"{'input-group': (form.fieldAddonLeft || form.fieldAddonRight)}\"><span ng-if=\"form.fieldAddonLeft\" class=\"input-group-addon\" ng-bind-html=\"form.fieldAddonLeft\"></span> <input ng-show=\"form.key\" type=\"{{form.type}}\" step=\"any\" sf-changed=\"form\" placeholder=\"{{form.isTranslatedLabels ? form.placeholder : t(form.placeholder, form.placeholderKey)}}\" class=\"form-control {{form.fieldHtmlClass}}\" id=\"{{form.key.slice(-1)[0]}}\" sf-field-model ng-model=\"$$value$$\" ng-model-options=\"form.ngModelOptions\" ng-disabled=\"form.readonly\" schema-validate=\"form\" name=\"{{form.key.slice(-1)[0]}}\" aria-describedby=\"{{form.key.slice(-1)[0] + 'Status'}}\"> <span ng-if=\"form.fieldAddonRight\" class=\"input-group-addon\" ng-bind-html=\"form.fieldAddonRight\"></span></div><span ng-if=\"form.feedback !== false\" class=\"form-control-feedback\" ng-class=\"evalInScope(form.feedback) || {'glyphicon': true, 'glyphicon-ok': form.disableSuccessState !== true && hasSuccess(), 'glyphicon-remove': form.disableErrorState !== true && hasError() }\" aria-hidden=\"true\"></span> <span ng-if=\"hasError() || hasSuccess()\" id=\"{{form.key.slice(-1)[0] + 'Status'}}\" class=\"sr-only\">{{ hasSuccess() ? '(success)' : '(error)' }}</span><div class=\"help-block\" sf-message=\"form.isTranslatedLabels ? form.description : t(form.description, form.descriptionKey)\"></div></div>"
  );


  $templateCache.put('/web/fieldset/fieldset.decorator.html',
    "<fieldset ng-disabled=\"form.readonly\" class=\"schema-form-fieldset {{form.htmlClass}}\" nsp-t><legend ng-class=\"{'sr-only': !showTitle() }\">{{form.isTranslatedLabels ? form.title : t(form.title, form.titleKey)}}</legend><div class=\"help-block\" ng-show=\"form.description\" ng-bind-html=\"form.isTranslatedLabels ? form.description : t(form.description, form.descriptionKey)\"></div><sf-decorator ng-repeat=\"item in form.items\" form=\"item\"></sf-decorator></fieldset>"
  );


  $templateCache.put('/web/help/help.decorator.html',
    "<div class=\"form-group {{form.htmlClass}}\" ng-style=\"{'width': form.width + '%'}\"><label class=\"control-label {{form.labelHtmlClass}}\" ng-class=\"{'sr-only': !showTitle()}\" for=\"{{form.key.slice(-1)[0]}}\">{{form.isTranslatedLabels ? form.title : t(form.title, form.titleKey)}}</label><div class=\"helpvalue {{form.htmlClass}}\" fe-trust-as-html=\"form.helpvalue\" encoded=\"form.encoded\"></div></div>"
  );


  $templateCache.put('/web/kendo-combo-box/fe-kendo-combo-box.template.html',
    "<div class=\"input-group\"><div class=\"kendo-input\"><div class=\"inner\"><input type=\"text\" step=\"any\" id=\"{{settings.key.slice(-1)[0]}}\" name=\"{{settings.key.slice(-1)[0]}}\" class=\"form-control {{settings.fieldHtmlClass}}\" ng-disabled=\"settings.readonly\" kendo-combo-box=\"widget\" k-options=\"settings.kendoOptions\" k-rebind=\"settings.kendoOptions\" k-ng-model=\"widgetModel\" aria-describedby=\"{{settings.key.slice(-1)[0] + 'Status'}}\"> <span class=\"clear-button\" ng-click=\"clearHandler(true)\" ng-show=\"clearButton\"><i class=\"k-icon k-i-close\"></i></span></div></div><div ng-if=\"settings.browse\" class=\"schema-form-btn browse-btn\"><div class=\"btn btn-default f-nsp f-nsp-search\" ng-click=\"browseWindow()\" title=\"{{settings.isTranslatedLabels ? settings.browse.caption : t(settings.browse.caption, settings.browse.captionKey)}}\"></div></div></div>"
  );


  $templateCache.put('/web/kendo-combo-box/kendo-combo-box.decorator.html',
    "<div class=\"form-group schema-form-{{form.type}} {{form.htmlClass}}\" ng-class=\"{'has-browse': form.browseUrl, 'has-error': form.disableErrorState !== true && hasError(), 'has-success': form.disableSuccessState !== true && hasSuccess(), 'has-feedback': form.feedback !== false, 'required': form.required, 'read-only': form.readonly }\" ng-style=\"{'width': form.width + '%'}\" nsp-t><label class=\"control-label {{form.labelHtmlClass}}\" ng-class=\"{'sr-only': !showTitle()}\" for=\"{{form.key.slice(-1)[0]}}\">{{form.isTranslatedLabels ? form.title : t(form.title, form.titleKey)}}</label><div fe-kendo-combo-box fe-options=\"form\" fe-ng-model=\"ngModel\" sf-field-model ng-model=\"$$value$$\" ng-show=\"form.key\" sf-changed=\"form\" data-name=\"{{form.key.slice(-1)[0]}}\" schema-validate=\"form\"></div><span ng-if=\"form.feedback !== false\" class=\"form-control-feedback\" ng-class=\"evalInScope(form.feedback) || {'glyphicon': true, 'glyphicon-ok': hasSuccess(), 'glyphicon-remove': hasError() }\" aria-hidden=\"true\"></span> <span ng-if=\"hasError() || hasSuccess()\" id=\"{{form.key.slice(-1)[0] + 'Status'}}\" class=\"sr-only\">{{ hasSuccess() ? '(success)' : '(error)' }}</span><div class=\"help-block\" sf-message=\"form.isTranslatedLabels ? form.description : t(form.description, form.descriptionKey)\"></div></div>"
  );


  $templateCache.put('/web/kendo-date-time-picker/kendo-date-time-picker.decorator.html',
    "<div class=\"form-group schema-form-{{form.type}} {{form.htmlClass}}\" ng-class=\"{'has-error': form.disableErrorState !== true && hasError(), 'has-success': form.disableSuccessState !== true && hasSuccess(), 'has-feedback': form.feedback !== false, 'required': form.required !== false, 'required': form.required }\" ng-style=\"{'width': form.width + '%'}\" nsp-t><label class=\"control-label {{form.labelHtmlClass}}\" ng-class=\"{'sr-only': !showTitle()}\" for=\"{{form.key.slice(-1)[0]}}\">{{form.isTranslatedLabels ? form.title : t(form.title, form.titleKey)}}</label><div fe-kendo-date-time-picker fe-options=\"form\" fe-ng-model=\"ngModel\" type=\"{{form.type}}\" sf-field-model ng-model=\"$$value$$\" ng-show=\"form.key\" sf-changed=\"form\" data-name=\"{{form.key.slice(-1)[0]}}\" schema-validate=\"form\" placeholder=\"{{form.isTranslatedLabels ? form.placeholder : t(form.placeholder, form.placeholderKey)}}\"></div><span ng-if=\"form.feedback !== false\" class=\"form-control-feedback\" ng-class=\"evalInScope(form.feedback) || {'glyphicon': true, 'glyphicon-ok': hasSuccess(), 'glyphicon-remove': hasError() }\" aria-hidden=\"true\"></span> <span ng-if=\"hasError() || hasSuccess()\" id=\"{{form.key.slice(-1)[0] + 'Status'}}\" class=\"sr-only\">{{ hasSuccess() ? '(success)' : '(error)' }}</span><div class=\"help-block\" sf-message=\"form.isTranslatedLabels ? form.description : t(form.description, form.descriptionKey)\"></div></div>"
  );


  $templateCache.put('/web/kendo-date-time-picker/templates/fe-kendo-date-picker.template.html',
    "<input type=\"text\" step=\"any\" id=\"{{settings.key.slice(-1)[0]}}\" name=\"{{settings.key.slice(-1)[0]}}\" class=\"form-control {{settings.fieldHtmlClass}}\" ng-disabled=\"settings.readonly\" kendo-date-picker=\"widget\" k-options=\"settings.kendoOptions\" k-rebind=\"settings.kendoOptions\" ng-model=\"widgetText\" aria-describedby=\"{{settings.key.slice(-1)[0] + 'Status'}}\">"
  );


  $templateCache.put('/web/kendo-date-time-picker/templates/fe-kendo-date-time-picker.template.html',
    "<input type=\"text\" step=\"any\" id=\"{{settings.key.slice(-1)[0]}}\" name=\"{{settings.key.slice(-1)[0]}}\" class=\"form-control {{settings.fieldHtmlClass}}\" ng-disabled=\"settings.readonly\" kendo-date-time-picker=\"widget\" k-options=\"settings.kendoOptions\" k-rebind=\"settings.kendoOptions\" ng-model=\"widgetText\" aria-describedby=\"{{settings.key.slice(-1)[0] + 'Status'}}\">"
  );


  $templateCache.put('/web/kendo-date-time-picker/templates/fe-kendo-time-picker.template.html',
    "<input type=\"text\" step=\"any\" id=\"{{settings.key.slice(-1)[0]}}\" name=\"{{settings.key.slice(-1)[0]}}\" class=\"form-control {{settings.fieldHtmlClass}}\" ng-disabled=\"settings.readonly\" kendo-time-picker=\"widget\" k-options=\"settings.kendoOptions\" k-rebind=\"settings.kendoOptions\" ng-model=\"widgetText\" aria-describedby=\"{{settings.key.slice(-1)[0] + 'Status'}}\">"
  );


  $templateCache.put('/web/kendo-drop-down-list/fe-kendo-drop-down-list.template.html',
    "<input type=\"text\" step=\"any\" id=\"{{settings.key.slice(-1)[0]}}\" name=\"{{settings.key.slice(-1)[0]}}\" class=\"form-control {{settings.fieldHtmlClass}}\" ng-disabled=\"settings.readonly\" kendo-drop-down-list=\"widget\" k-options=\"settings.kendoOptions\" k-ng-model=\"widgetModel\" k-rebind=\"settings.kendoOptions\" aria-describedby=\"{{settings.key.slice(-1)[0] + 'Status'}}\">"
  );


  $templateCache.put('/web/kendo-drop-down-list/kendo-drop-down-list.decorator.html',
    "<div class=\"form-group schema-form-{{form.type}} {{form.htmlClass}}\" ng-class=\"{'has-browse': form.browseUrl, 'has-error': form.disableErrorState !== true && hasError(), 'has-success': form.disableSuccessState !== true && hasSuccess(), 'has-feedback': form.feedback !== false, 'required': form.required, 'read-only': form.readonly }\" ng-style=\"{'width': form.width + '%'}\" nsp-t><label class=\"control-label {{form.labelHtmlClass}}\" ng-class=\"{'sr-only': !showTitle()}\" for=\"{{form.key.slice(-1)[0]}}\">{{form.isTranslatedLabels ? form.title : t(form.title, form.titleKey)}}</label><div fe-kendo-drop-down-list fe-options=\"form\" fe-ng-model=\"ngModel\" sf-field-model ng-model=\"$$value$$\" ng-show=\"form.key\" sf-changed=\"form\" data-name=\"{{form.key.slice(-1)[0]}}\" schema-validate=\"form\"></div><span ng-if=\"form.feedback !== false\" class=\"form-control-feedback\" ng-class=\"evalInScope(form.feedback) || {'glyphicon': true, 'glyphicon-ok': hasSuccess(), 'glyphicon-remove': hasError() }\" aria-hidden=\"true\"></span> <span ng-if=\"hasError() || hasSuccess()\" id=\"{{form.key.slice(-1)[0] + 'Status'}}\" class=\"sr-only\">{{ hasSuccess() ? '(success)' : '(error)' }}</span><div class=\"help-block\" sf-message=\"form.isTranslatedLabels ? form.description : t(form.description, form.descriptionKey)\"></div></div>"
  );


  $templateCache.put('/web/kendo-editor/fe-kendo-editor.template.html',
    "<textarea id=\"{{settings.key.slice(-1)[0]}}\" name=\"{{settings.key.slice(-1)[0]}}\" class=\"form-control {{settings.fieldHtmlClass}}\" ng-disabled=\"settings.readonly\" kendo-editor=\"widget\" k-options=\"settings.kendoOptions\" k-ng-model=\"widgetValue\" k-rebind=\"settings.kendoOptions\" aria-describedby=\"{{settings.key.slice(-1)[0] + 'Status'}}\"></textarea>"
  );


  $templateCache.put('/web/kendo-editor/kendo-editor.decorator.html',
    "<div class=\"form-group schema-form-{{form.type}} {{form.htmlClass}}\" ng-class=\"{'has-error': form.disableErrorState !== true && hasError(), 'has-success': form.disableSuccessState !== true && hasSuccess(), 'has-feedback': form.feedback !== false, 'required': form.required }\" ng-style=\"{'width': form.width + '%'}\" nsp-t><label class=\"control-label {{form.labelHtmlClass}}\" ng-class=\"{'sr-only': !showTitle()}\" for=\"{{form.key.slice(-1)[0]}}\">{{form.isTranslatedLabels ? form.title : t(form.title, form.titleKey)}}{{form.titleSuffix}}</label><div fe-kendo-editor fe-options=\"form\" fe-ng-model=\"ngModel\" ng-model=\"$$value$$\" ng-show=\"form.key\" sf-changed=\"form\" data-name=\"{{form.key.slice(-1)[0]}}\" schema-validate=\"form\"></div><span ng-if=\"form.feedback !== false\" class=\"form-control-feedback\" ng-class=\"evalInScope(form.feedback) || {'glyphicon': true, 'glyphicon-ok': hasSuccess(), 'glyphicon-remove': hasError() }\" aria-hidden=\"true\"></span> <span ng-if=\"hasError() || hasSuccess()\" id=\"{{form.key.slice(-1)[0] + 'Status'}}\" class=\"sr-only\">{{ hasSuccess() ? '(success)' : '(error)' }}</span><div class=\"help-block\" sf-message=\"form.isTranslatedLabels ? form.description : t(form.description, form.descriptionKey)\"></div></div>"
  );


  $templateCache.put('/web/kendo-image-upload/fe-kendo-image-upload.template.html',
    "<div class=\"upload-container\"><div class=\"image-container\"><i class=\"icon f-nsp f-nsp-plus\" ng-if=\"!uploading && !selectedImage.preview\"></i> <span class=\"update-image\" ng-if=\"selectedImage && selectedImage.preview\">{{t('Change image', 'Common.ChangeImage')}}</span> <img ng-if=\"selectedImage && selectedImage.preview\" ng-src=\"{{selectedImage.preview}}\"> <span class=\"spinner\" ng-if=\"uploading\"></span> <input type=\"file\" id=\"{{settings.key.slice(-1)[0]}}\" name=\"{{settings.key.slice(-1)[0]}}\" ng-disabled=\"settings.readonly\" kendo-upload=\"widget\" k-options=\"settings.kendoOptions\" k-rebind=\"settings.kendoOptions\" title=\"&nbsp;\" aria-describedby=\"{{settings.key.slice(-1)[0] + 'Status'}}\"></div><div class=\"image-info\"><div class=\"file-name\" title=\"{{selectedImage.name}}\">{{selectedImage.name}}</div><div class=\"remove\" ng-if=\"selectedImage\"><button type=\"button\" class=\"remove-btn\" ng-click=\"remove()\">{{t('Remove', 'Common.Remove')}}</button></div></div></div>"
  );


  $templateCache.put('/web/kendo-image-upload/kendo-image-upload.decorator.html',
    "<div class=\"form-group schema-form-{{form.type}} {{form.htmlClass}}\" ng-class=\"{'has-browse': form.browseUrl, 'has-error': form.disableErrorState !== true && hasError(), 'has-success': form.disableSuccessState !== true && hasSuccess(), 'has-feedback': form.feedback !== false, 'required': form.required, 'read-only': form.readonly }\" ng-style=\"{'width': form.width + '%'}\" nsp-t><label class=\"control-label {{form.labelHtmlClass}}\" ng-class=\"{'sr-only': !showTitle()}\" for=\"{{form.key.slice(-1)[0]}}\">{{form.isTranslatedLabels ? form.title : t(form.title, form.titleKey)}}</label><div fe-kendo-image-upload fe-options=\"form\" fe-ng-model=\"ngModel\" sf-field-model ng-model=\"$$value$$\" ng-show=\"form.key\" sf-changed=\"form\" ng-model-options=\"{allowInvalid: true}\" data-name=\"{{form.key.slice(-1)[0]}}\" schema-validate=\"form\"></div><span ng-if=\"form.feedback !== false\" class=\"form-control-feedback\" ng-class=\"evalInScope(form.feedback) || {'glyphicon': true, 'glyphicon-ok': hasSuccess(), 'glyphicon-remove': hasError() }\" aria-hidden=\"true\"></span> <span ng-if=\"hasError() || hasSuccess()\" id=\"{{form.key.slice(-1)[0] + 'Status'}}\" class=\"sr-only\">{{ hasSuccess() ? '(success)' : '(error)' }}</span><div class=\"help-block\" sf-message=\"form.isTranslatedLabels ? form.description : t(form.description, form.descriptionKey)\"></div></div>"
  );


  $templateCache.put('/web/kendo-multi-select/fe-kendo-multi-select.template.html',
    "<select multiple=\"multiple\" id=\"{{settings.key.slice(-1)[0]}}\" name=\"{{settings.key.slice(-1)[0]}}\" ng-disabled=\"settings.readonly\" kendo-multi-select=\"widget\" k-options=\"settings.kendoOptions\" k-ng-model=\"widgetModel\" k-rebind=\"settings.kendoOptions\" aria-describedby=\"{{settings.key.slice(-1)[0] + 'Status'}}\"></select>"
  );


  $templateCache.put('/web/kendo-multi-select/kendo-multi-select.decorator.html',
    "<div class=\"form-group schema-form-{{form.type}} {{form.htmlClass}}\" ng-class=\"{'has-browse': form.browseUrl, 'has-error': form.disableErrorState !== true && hasError(), 'has-success': form.disableSuccessState !== true && hasSuccess(), 'has-feedback': form.feedback !== false, 'required': form.required, 'read-only': form.readonly }\" ng-style=\"{'width': form.width + '%'}\" nsp-t><label class=\"control-label {{form.labelHtmlClass}}\" ng-class=\"{'sr-only': !showTitle()}\" for=\"{{form.key.slice(-1)[0]}}\">{{form.isTranslatedLabels ? form.title : t(form.title, form.titleKey)}}</label><div fe-kendo-multi-select fe-options=\"form\" fe-ng-model=\"ngModel\" sf-field-model ng-model=\"$$value$$\" ng-show=\"form.key\" sf-changed=\"form\" data-name=\"{{form.key.slice(-1)[0]}}\" schema-validate=\"form\"></div><span ng-if=\"form.feedback !== false\" class=\"form-control-feedback\" ng-class=\"evalInScope(form.feedback) || {'glyphicon': true, 'glyphicon-ok': hasSuccess(), 'glyphicon-remove': hasError() }\" aria-hidden=\"true\"></span> <span ng-if=\"hasError() || hasSuccess()\" id=\"{{form.key.slice(-1)[0] + 'Status'}}\" class=\"sr-only\">{{ hasSuccess() ? '(success)' : '(error)' }}</span><div class=\"help-block\" sf-message=\"form.isTranslatedLabels ? form.description : t(form.description, form.descriptionKey)\"></div></div>"
  );


  $templateCache.put('/web/kendo-numeric-text-box/fe-kendo-numeric-text-box.template.html',
    "<input type=\"number\" id=\"{{settings.key.slice(-1)[0]}}\" name=\"{{settings.key.slice(-1)[0]}}\" class=\"form-control {{settings.fieldHtmlClass}}\" ng-disabled=\"settings.readonly\" kendo-numeric-text-box=\"widget\" k-options=\"settings.kendoOptions\" k-ng-model=\"widgetModel\" k-rebind=\"settings.kendoOptions\" aria-describedby=\"{{settings.key.slice(-1)[0] + 'Status'}}\">"
  );


  $templateCache.put('/web/kendo-numeric-text-box/kendo-numeric-text-box.decorator.html',
    "<div class=\"form-group schema-form-{{form.type}} {{form.htmlClass}}\" ng-class=\"{'has-error': form.disableErrorState !== true && hasError(), 'has-success': form.disableSuccessState !== true && hasSuccess(), 'has-feedback': form.feedback !== false, 'required': form.required }\" ng-style=\"{'width': form.width + '%'}\" nsp-t><label class=\"control-label {{form.labelHtmlClass}}\" ng-class=\"{'sr-only': !showTitle()}\" for=\"{{form.key.slice(-1)[0]}}\">{{form.isTranslatedLabels ? form.title : t(form.title, form.titleKey)}}{{form.titleSuffix}}</label><div fe-kendo-numeric-text-box fe-options=\"form\" fe-ng-model=\"ngModel\" ng-model=\"$$value$$\" ng-show=\"form.key\" sf-changed=\"form\" data-name=\"{{form.key.slice(-1)[0]}}\" schema-validate=\"form\"></div><span ng-if=\"form.feedback !== false\" class=\"form-control-feedback\" ng-class=\"evalInScope(form.feedback) || {'glyphicon': true, 'glyphicon-ok': hasSuccess(), 'glyphicon-remove': hasError() }\" aria-hidden=\"true\"></span> <span ng-if=\"hasError() || hasSuccess()\" id=\"{{form.key.slice(-1)[0] + 'Status'}}\" class=\"sr-only\">{{ hasSuccess() ? '(success)' : '(error)' }}</span><div class=\"help-block\" sf-message=\"form.isTranslatedLabels ? form.description : t(form.description, form.descriptionKey)\"></div></div>"
  );


  $templateCache.put('/web/number/number.decorator.html',
    "<div class=\"form-group {{form.htmlClass}}\" ng-class=\"{ '{{'schema-form-' + form.type}}': true, 'has-error': form.disableErrorState !== true && hasError(), 'has-success': form.disableSuccessState !== true && hasSuccess(), 'has-feedback': form.feedback !== false, 'required': form.required, 'read-only': form.readonly }\" ng-style=\"{'width': form.width + '%'}\" nsp-t><label class=\"control-label {{form.labelHtmlClass}}\" ng-class=\"{'sr-only': !showTitle()}\" for=\"{{form.key.slice(-1)[0]}}\">{{form.isTranslatedLabels ? form.title : t(form.title, form.titleKey)}}</label><input ng-if=\"!form.fieldAddonLeft && !form.fieldAddonRight\" ng-show=\"form.key\" type=\"{{form.type}}\" step=\"any\" sf-changed=\"form\" placeholder=\"{{form.isTranslatedLabels ? form.placeholder : t(form.placeholder, form.placeholderKey)}}\" class=\"form-control {{form.fieldHtmlClass}}\" id=\"{{form.key.slice(-1)[0]}}\" sf-field-model ng-model=\"$$value$$\" ng-model-options=\"form.ngModelOptions\" ng-disabled=\"form.readonly\" schema-validate=\"form\" name=\"{{form.key.slice(-1)[0]}}\" aria-describedby=\"{{form.key.slice(-1)[0] + 'Status'}}\"><div ng-if=\"form.fieldAddonLeft || form.fieldAddonRight\" ng-class=\"{'input-group': (form.fieldAddonLeft || form.fieldAddonRight)}\"><span ng-if=\"form.fieldAddonLeft\" class=\"input-group-addon\" ng-bind-html=\"form.fieldAddonLeft\"></span> <input ng-show=\"form.key\" type=\"{{form.type}}\" step=\"any\" sf-changed=\"form\" placeholder=\"{{form.isTranslatedLabels ? form.placeholder : t(form.placeholder, form.placeholderKey)}}\" class=\"form-control {{form.fieldHtmlClass}}\" id=\"{{form.key.slice(-1)[0]}}\" sf-field-model ng-model=\"$$value$$\" ng-model-options=\"form.ngModelOptions\" ng-disabled=\"form.readonly\" schema-validate=\"form\" name=\"{{form.key.slice(-1)[0]}}\" aria-describedby=\"{{form.key.slice(-1)[0] + 'Status'}}\"> <span ng-if=\"form.fieldAddonRight\" class=\"input-group-addon\" ng-bind-html=\"form.fieldAddonRight\"></span></div><span ng-if=\"form.feedback !== false\" class=\"form-control-feedback\" ng-class=\"evalInScope(form.feedback) || {'glyphicon': true, 'glyphicon-ok': form.disableSuccessState !== true && hasSuccess(), 'glyphicon-remove': form.disableErrorState !== true && hasError() }\" aria-hidden=\"true\"></span> <span ng-if=\"hasError() || hasSuccess()\" id=\"{{form.key.slice(-1)[0] + 'Status'}}\" class=\"sr-only\">{{ hasSuccess() ? '(success)' : '(error)' }}</span><div class=\"help-block\" sf-message=\"form.isTranslatedLabels ? form.description : t(form.description, form.descriptionKey)\"></div></div>"
  );


  $templateCache.put('/web/password/password.decorator.html',
    "<div class=\"form-group {{form.htmlClass}}\" ng-class=\"{ '{{'schema-form-' + form.type}}': true, 'has-error': form.disableErrorState !== true && hasError(), 'has-success': form.disableSuccessState !== true && hasSuccess(), 'has-feedback': form.feedback !== false, 'required': form.required, 'read-only': form.readonly }\" ng-style=\"{'width': form.width + '%'}\" nsp-t><label class=\"control-label {{form.labelHtmlClass}}\" ng-class=\"{'sr-only': !showTitle()}\" for=\"{{form.key.slice(-1)[0]}}\">{{form.isTranslatedLabels ? form.title : t(form.title, form.titleKey)}}</label><input ng-if=\"!form.fieldAddonLeft && !form.fieldAddonRight\" ng-show=\"form.key\" type=\"{{form.type}}\" step=\"any\" sf-changed=\"form\" placeholder=\"{{form.isTranslatedLabels ? form.placeholder : t(form.placeholder, form.placeholderKey)}}\" class=\"form-control {{form.fieldHtmlClass}}\" id=\"{{form.key.slice(-1)[0]}}\" sf-field-model ng-model=\"$$value$$\" ng-model-options=\"form.ngModelOptions\" ng-disabled=\"form.readonly\" schema-validate=\"form\" name=\"{{form.key.slice(-1)[0]}}\" aria-describedby=\"{{form.key.slice(-1)[0] + 'Status'}}\"><div ng-if=\"form.fieldAddonLeft || form.fieldAddonRight\" ng-class=\"{'input-group': (form.fieldAddonLeft || form.fieldAddonRight)}\"><span ng-if=\"form.fieldAddonLeft\" class=\"input-group-addon\" ng-bind-html=\"form.fieldAddonLeft\"></span> <input ng-show=\"form.key\" type=\"{{form.type}}\" step=\"any\" sf-changed=\"form\" placeholder=\"{{form.isTranslatedLabels ? form.placeholder : t(form.placeholder, form.placeholderKey)}}\" class=\"form-control {{form.fieldHtmlClass}}\" id=\"{{form.key.slice(-1)[0]}}\" sf-field-model ng-model=\"$$value$$\" ng-model-options=\"form.ngModelOptions\" ng-disabled=\"form.readonly\" schema-validate=\"form\" name=\"{{form.key.slice(-1)[0]}}\" aria-describedby=\"{{form.key.slice(-1)[0] + 'Status'}}\"> <span ng-if=\"form.fieldAddonRight\" class=\"input-group-addon\" ng-bind-html=\"form.fieldAddonRight\"></span></div><span ng-if=\"form.feedback !== false\" class=\"form-control-feedback\" ng-class=\"evalInScope(form.feedback) || {'glyphicon': true, 'glyphicon-ok': form.disableSuccessState !== true && hasSuccess(), 'glyphicon-remove': form.disableErrorState !== true && hasError() }\" aria-hidden=\"true\"></span> <span ng-if=\"hasError() || hasSuccess()\" id=\"{{form.key.slice(-1)[0] + 'Status'}}\" class=\"sr-only\">{{ hasSuccess() ? '(success)' : '(error)' }}</span><div class=\"help-block\" sf-message=\"form.isTranslatedLabels ? form.description : t(form.description, form.descriptionKey)\"></div></div>"
  );


  $templateCache.put('/web/radio-buttons/radio-buttons.decorator.html',
    "<div class=\"form-group schema-form-radiobuttons {{form.htmlClass}}\" ng-class=\"{'has-error': form.disableErrorState !== true &&  hasError(), 'has-success': form.disableSuccessState !== true &&  hasSuccess(), 'required': form.required, 'read-only': form.readonly }\" ng-model=\"$$value$$\" ng-style=\"{'width': form.width + '%'}\" nsp-t><div><label class=\"control-label {{form.labelHtmlClass}}\" ng-show=\"showTitle()\">{{form.isTranslatedLabels ? form.title : t(form.title, form.titleKey)}}</label></div><div class=\"btn-group\"><label sf-field-model=\"replaceAll\" class=\"btn {{ (item.value === $$value$$) ? form.style.selected || 'btn-default' : form.style.unselected || 'btn-default'; }}\" ng-class=\"{ active: item.value === $$value$$ }\" ng-repeat=\"item in form.titleMap\"><input type=\"radio\" class=\"{{form.fieldHtmlClass}}\" sf-changed=\"form\" style=\"display: none\" ng-disabled=\"form.readonly\" sf-field-model ng-model=\"$$value$$\" schema-validate=\"form\" ng-value=\"item.value\" name=\"{{form.key.join('.')}}\"> <span ng-bind-html=\"form.isTranslatedLabels ? item.title : t(item.title, item.titleKey)\"></span></label></div><div class=\"help-block\" sf-message=\"form.isTranslatedLabels ? form.descriptions : t(form.description, form.descriptionKey)\"></div></div>"
  );


  $templateCache.put('/web/radios-inline/radios-inline.decorator.html',
    "<div class=\"form-group schema-form-radios-inline {{form.htmlClass}}\" ng-class=\"{'has-error': form.disableErrorState !== true &&  hasError(), 'has-success': form.disableSuccessState !== true && hasSuccess(), 'required': form.required, 'read-only': form.readonly }\" ng-style=\"{'width': form.width + '%'}\" nsp-t><label class=\"control-label {{form.labelHtmlClass}}\" ng-show=\"showTitle()\">{{form.isTranslatedLabels ? form.title : t(form.title, form.titleKey)}}</label><div><label class=\"radio-inline\" ng-repeat=\"item in form.titleMap\"><input type=\"radio\" class=\"{{form.fieldHtmlClass}}\" sf-changed=\"form\" ng-disabled=\"form.readonly\" sf-field-model ng-model=\"$$value$$\" ng-value=\"item.value\" name=\"{{form.key.join('.')}}\"> <span ng-bind-html=\"form.isTranslatedLabels ? form.item : t(form.item, form.item)\"></span></label></div><div class=\"help-block\" sf-message=\"form.isTranslatedLabels ? form.description : t(form.description, form.descriptionKey)\"></div></div>"
  );


  $templateCache.put('/web/radios/radios.decorator.html',
    "<div class=\"form-group schema-form-radios {{form.htmlClass}}\" ng-class=\"{'has-error': form.disableErrorState !== true &&  hasError(), 'has-success': form.disableSuccessState !== true && hasSuccess(), 'required': form.required, 'read-only': form.readonly }\" ng-style=\"{'width': form.width + '%'}\" nsp-t><label class=\"control-label {{form.labelHtmlClass}}\" ng-show=\"showTitle()\">{{form.isTranslatedLabels ? form.title : t(form.title, form.titleKey)}}</label><div class=\"radio\" ng-repeat=\"item in form.titleMap\"><label><input type=\"radio\" class=\"{{form.fieldHtmlClass}}\" sf-changed=\"form\" ng-disabled=\"form.readonly\" sf-field-model ng-model=\"$$value$$\" ng-value=\"item.value\" name=\"{{form.key.join('.')}}\"> <span ng-bind-html=\"form.isTranslatedLabels ? item.title : t(item.title, item.titleKey)\"></span></label></div><div class=\"help-block\" sf-message=\"form.isTranslatedLabels ? form.description : t(form.description, form.descriptionKey)\"></div></div>"
  );


  $templateCache.put('/web/section/section.decorator.html',
    "<div class=\"schema-form-section {{form.htmlClass}}\"><sf-decorator ng-repeat=\"item in form.items\" form=\"item\" class=\"sf-decorator-item\"></sf-decorator></div>"
  );


  $templateCache.put('/web/select/select.decorator.html',
    "<div class=\"form-group {{form.htmlClass}} schema-form-select\" ng-class=\"{'has-error': form.disableErrorState !== true && hasError(), 'has-success': form.disableSuccessState !== true && hasSuccess(), 'has-feedback': form.feedback !== false, 'required': form.required, 'read-only': form.readonly }\" nsp-t ng-style=\"{'width': form.width + '%'}\"><label class=\"control-label {{form.labelHtmlClass}}\" ng-show=\"showTitle()\">{{form.isTranslatedLabels ? form.title : t(form.title, form.titleKey)}}</label><select sf-field-model ng-model=\"$$value$$\" ng-disabled=\"form.readonly\" sf-changed=\"form\" class=\"form-control {{form.fieldHtmlClass}}\" schema-validate=\"form\" ng-options=\"item.value as item.name group by item.group for item in form.titleMap\" name=\"{{form.key.slice(-1)[0]}}\"></select><div class=\"help-block\" sf-message=\"form.isTranslatedLabels ? form.description : t(form.description, form.descriptionKey)\"></div></div>"
  );


  $templateCache.put('/web/submit/submit.decorator.html',
    "<div class=\"form-group schema-form-submit {{form.htmlClass}}\" nsp-t ng-style=\"{'width': form.width + '%'}\"><input type=\"submit\" class=\"btn {{ form.style || 'btn-primary' }} {{form.fieldHtmlClass}}\" value=\"{{form.isTranslatedLabels ? form.title : t(form.title, form.titleKey)}}\" ng-disabled=\"form.readonly\" ng-if=\"form.type === 'submit'\"> <button class=\"btn {{ form.style || 'btn-default' }}\" type=\"button\" ng-click=\"buttonClick($event,form)\" ng-disabled=\"form.readonly\" ng-if=\"form.type !== 'submit'\"><span ng-if=\"form.icon\" class=\"{{form.icon}}\"></span> {{form.isTranslatedLabels ? form.title : t(form.title, form.titleKey)}}</button></div>"
  );


  $templateCache.put('/web/tabarray/tabarray.decorator.html',
    "<div ng-init=\"selected = { tab: 0 }\" ng-model=\"modelArray\" schema-validate=\"form\" sf-field-model=\"sf-new-array\" sf-new-array class=\"clearfix schema-form-tabarray schema-form-tabarray-{{form.tabType || 'left'}} {{form.htmlClass}}\" nsp-t><div ng-if=\"!form.tabType || form.tabType !== 'right'\" ng-class=\"{'col-xs-3': !form.tabType || form.tabType === 'left'}\"><ol class=\"nav nav-tabs\" ng-class=\"{ 'tabs-left': !form.tabType || form.tabType === 'left'}\" sf-field-model ui-sortable=\"form.sortOptions\"><li sf-field-model=\"ng-repeat\" ng-repeat=\"item in $$value$$ track by $index\" ng-click=\"$event.preventDefault() || (selected.tab = $index)\" ng-class=\"{active: selected.tab === $index}\"><a href=\"#\">{{interp(form.title,{'$index':$index, value: item}) || $index}}</a></li><li ng-hide=\"form.readonly || form.add === null\" ng-disabled=\"form.schema.maxItems <= modelArray.length\" ng-click=\"$event.preventDefault() || (selected.tab = appendToArray().length - 1)\"><a href=\"#\"><i class=\"glyphicon glyphicon-plus\"></i> {{ form.add || 'Add'}}</a></li></ol></div><div ng-class=\"{'col-xs-9': !form.tabType || form.tabType === 'left' || form.tabType === 'right'}\"><div class=\"tab-content {{form.fieldHtmlClass}}\"><div class=\"tab-pane clearfix tab{{selected.tab}} index{{$index}}\" sf-field-model=\"ng-repeat\" ng-repeat=\"item in $$value$$ track by $index\" ng-show=\"selected.tab === $index\" ng-class=\"{active: selected.tab === $index}\"><div schema-form-array-items></div><button ng-hide=\"form.readonly || form.remove === null\" ng-click=\"selected.tab = deleteFromArray($index).length - 1\" ng-disabled=\"form.schema.minItems >= modelArray.length\" type=\"button\" class=\"btn {{ form.style.remove || 'btn-default' }} pull-right\"><i class=\"glyphicon glyphicon-trash\"></i> {{ form.remove || 'Remove'}}</button></div><div class=\"help-block\" ng-show=\"(hasError() && errorMessage(schemaError())) || form.description\" ng-bind-html=\"(hasError() && errorMessage(schemaError())) || t(form.description, form.descriptionKey)\"></div></div></div></div><div ng-if=\"form.tabType === 'right'\" class=\"col-xs-3\"><ul class=\"nav nav-tabs tabs-right\"><li sf-field-model=\"ng-repeat\" ng-repeat=\"item in $$value$$ track by $index\" ng-click=\"$event.preventDefault() || (selected.tab = $index)\" ng-class=\"{active: selected.tab === $index}\"><a href=\"#\">{{interp(form.title,{'$index':$index, value: item}) || $index}}</a></li><li ng-hide=\"form.readonly || form.add === null\" ng-disabled=\"form.schema.maxItems <= modelArray.length\" ng-click=\"$event.preventDefault() || (selected.tab = appendToArray().length - 1)\"><a href=\"#\"><i class=\"glyphicon glyphicon-plus\"></i> {{ form.add || 'Add'}}</a></li></ul></div>"
  );


  $templateCache.put('/web/tabs/tabs.decorator.html',
    "<div ng-init=\"selected = { tab: 0 }\" class=\"schema-form-tabs {{form.htmlClass}}\" nsp-t><ul class=\"nav nav-tabs\"><li ng-repeat=\"tab in form.tabs\" ng-disabled=\"form.readonly\" ng-click=\"$event.preventDefault() || (selected.tab = $index)\" ng-class=\"{active: selected.tab === $index}\"><a href=\"#\">{{form.isTranslatedLabels ? tab.title : t(tab.title, tab.titleKey)}}</a></li></ul><div class=\"tab-content {{form.fieldHtmlClass}}\"></div></div>"
  );


  $templateCache.put('/web/template-picker/fe-template-picker-form.html',
    "<div ng-controller=\"FeTemplatePickerFormController as vm\" class=\"template-picker\" kendo-window-content><div class=\"template-picker-form\"><div class=\"language-element\"><span class=\"label\">{{vm.t('Templates', 'Common.Templates')}}</span><div class=\"language-wrapper\"><div class=\"language\" ng-repeat=\"lang in vm.languages\"><div class=\"language-title\" ng-click=\"setActiveLang($index)\"><i class=\"f-nsp\" ng-class=\"{'f-nsp-angle-up': $index === vm.activeLang, 'f-nsp-angle-down': $index !== vm.activeLang}\"></i> <span ng-class=\"{'italic': lang.subject === ''}\">{{vm.t(lang.languageName, lang.languageNameKey)}}</span> <span class=\"default-lang\" ng-if=\"vm.dialogData.form.currentLang === lang.languageId\">({{vm.t('DefaultLang', 'Common.DefaultLanguage')}})</span> <span class=\"remove-lang\" ng-if=\"vm.dialogData.form.currentLang !== lang.languageId\" ng-click=\"removeLanguage($index)\">{{vm.t('Remove', 'Common.Remove')}}</span></div><div class=\"language-template\" ng-show=\"$index === vm.activeLang\"><textarea class=\"editor\" ng-model=\"lang.subject\"></textarea></div></div></div></div><div class=\"placeholder-element\"><span class=\"label\">{{vm.t('Placeholders', 'Common.Placeholders')}}</span><div class=\"placeholders-wrapper\"><div class=\"placeholders-treeview\" kendo-tree-view=\"tree\" k-options=\"treeOptions\" k-on-change=\"selectTreeViewItem(kendoEvent)\"></div></div></div></div><div class=\"btn-group form-engine\"><div class=\"lang-list\" ng-if=\"vm.langListOpened\"><div class=\"lang\" ng-repeat=\"langDef in vm.langDefinitionList\" ng-click=\"addLanguageDefinition(langDef)\">{{vm.t(langDef.languageName, langDef.languageNameKey)}}</div></div><div class=\"add-lang-definition\" ng-click=\"openLanguageList()\"><span class=\"label\">{{vm.t('AddLangDefinition', 'Common.AddLanguageDefinition')}}</span></div><button class=\"btn btn-default\" ng-click=\"saveTemplateForm()\">{{vm.t('Save', 'Common.Save')}}</button> <button class=\"btn btn-default\" ng-click=\"cancel()\">{{vm.t('Cancel', 'Common.Cancel')}}</button></div></div>"
  );


  $templateCache.put('/web/template-picker/template-picker.decorator.html',
    "<div class=\"form-group schema-form-{{form.type}} {{form.htmlClass}}\" ng-class=\"{'has-error': form.disableErrorState !== true && hasError(), 'has-success': form.disableSuccessState !== true && hasSuccess(), 'has-feedback': form.feedback !== false, 'required': form.required !== false, 'required': form.required }\" ng-style=\"{'width': form.width + '%'}\" nsp-t><label class=\"control-label {{form.labelHtmlClass}}\" ng-class=\"{'sr-only': !showTitle()}\" for=\"{{form.key.slice(-1)[0]}}\">{{form.isTranslatedLabels ? form.title : t(form.title, form.titleKey)}}</label><div fe-template-picker fe-options=\"form\" fe-ng-model=\"ngModel\" type=\"{{form.type}}\" sf-field-model ng-model=\"$$value$$\" ng-show=\"form.key\" sf-changed=\"form\" data-name=\"{{form.key.slice(-1)[0]}}\" schema-validate=\"form\" placeholder=\"{{form.isTranslatedLabels ? form.placeholder : t(form.placeholder, form.placeholderKey)}}\"></div><span ng-if=\"form.feedback !== false\" class=\"form-control-feedback\" ng-class=\"evalInScope(form.feedback) || {'glyphicon': true, 'glyphicon-ok': hasSuccess(), 'glyphicon-remove': hasError() }\" aria-hidden=\"true\"></span> <span ng-if=\"hasError() || hasSuccess()\" id=\"{{form.key.slice(-1)[0] + 'Status'}}\" class=\"sr-only\">{{ hasSuccess() ? '(success)' : '(error)' }}</span><div class=\"help-block\" sf-message=\"form.isTranslatedLabels ? form.description : t(form.description, form.descriptionKey)\"></div></div>"
  );


  $templateCache.put('/web/template-picker/templates/fe-template-picker.template.html',
    "<div class=\"template-picker\"><div class=\"input-group\"><div class=\"kendo-input\"><div class=\"inner\" title=\"{{viewVal}}\"><input type=\"text\" ng-disabled=\"true\" class=\"form-control\" ng-model=\"viewVal\"></div></div><div class=\"schema-form-btn add-btn\"><div class=\"btn btn-default f-nsp f-nsp-plus\" ng-click=\"openPicker()\"></div></div></div></div>"
  );


  $templateCache.put('/web/textarea/textarea.decorator.html',
    "<div class=\"form-group has-feedback {{form.htmlClass}} schema-form-textarea\" ng-class=\"{'has-error': form.disableErrorState !== true && hasError(), 'has-success': form.disableSuccessState !== true &&  hasSuccess(), 'required': form.required, 'read-only': form.readonly }\" ng-style=\"{'width': form.width + '%'}\" nsp-t><label class=\"control-label {{form.labelHtmlClass}}\" ng-class=\"{'sr-only': !showTitle()}\" for=\"{{form.key.slice(-1)[0]}}\">{{form.isTranslatedLabels ? form.title : t(form.title, form.titleKey)}}{{form.titleSuffix}}</label><textarea ng-if=\"!form.fieldAddonLeft && !form.fieldAddonRight\" class=\"form-control {{form.fieldHtmlClass}}\" id=\"{{form.key.slice(-1)[0]}}\" sf-changed=\"form\" placeholder=\"{{form.placeholder}}\" ng-disabled=\"form.readonly\" sf-field-model ng-model=\"$$value$$\" schema-validate=\"form\" name=\"{{form.key.slice(-1)[0]}}\"></textarea><div ng-if=\"form.fieldAddonLeft || form.fieldAddonRight\" ng-class=\"{'input-group': (form.fieldAddonLeft || form.fieldAddonRight)}\"><span ng-if=\"form.fieldAddonLeft\" class=\"input-group-addon\" ng-bind-html=\"form.fieldAddonLeft\"></span><textarea class=\"form-control {{form.fieldHtmlClass}}\" id=\"{{form.key.slice(-1)[0]}}\" sf-changed=\"form\" placeholder=\"{{form.placeholder}}\" ng-disabled=\"form.readonly\" sf-field-model ng-model=\"$$value$$\" schema-validate=\"form\" name=\"{{form.key.slice(-1)[0]}}\"></textarea><span ng-if=\"form.fieldAddonRight\" class=\"input-group-addon\" ng-bind-html=\"form.fieldAddonRight\"></span></div><span class=\"help-block\" sf-message=\"form.isTranslatedLabels ? form.description : t(form.description, form.descriptionKey)\"></span></div>"
  );


  $templateCache.put('/web/ticket-description/fe-ticket-description.template.html',
    "<textarea id=\"{{settings.key.slice(-1)[0]}}\" name=\"{{settings.key.slice(-1)[0]}}\" class=\"form-control {{settings.fieldHtmlClass}}\" ng-disabled=\"settings.readonly\" kendo-editor=\"widget\" k-options=\"settings.editorOptions\" k-ng-model=\"editorValue\" k-rebind=\"settings.editorOptions\" aria-describedby=\"{{settings.key.slice(-1)[0] + 'Status'}}\"></textarea><div file-drop file-drop-options=\"settings.attachmentsOptions\" class=\"file-drop\" ng-model=\"attachments\"></div>"
  );


  $templateCache.put('/web/ticket-description/ticket-description.decorator.html',
    "<div class=\"form-group schema-form-{{form.type}} {{form.htmlClass}}\" ng-class=\"{'has-error': form.disableErrorState !== true && hasError(), 'has-success': form.disableSuccessState !== true && hasSuccess(), 'has-feedback': form.feedback !== false, 'required': form.required, 'read-only': form.readonly}\"><label class=\"control-label {{form.labelHtmlClass}}\" ng-class=\"{'sr-only': !showTitle()}\" for=\"{{form.key.slice(-1)[0]}}\">{{form.isTranslatedLabels ? form.title : translate(form.title, form.titleKey)}}</label><div fe-ticket-description fe-options=\"form\" ng-model=\"$$value$$\" ng-model-options=\"form.ngModelOptions\" ng-disabled=\"form.readonly\" sf-changed=\"form\" id=\"{{form.key.slice(-1)[0]}}\" class=\"form-control {{form.fieldHtmlClass}}\" schema-validate=\"form\" name=\"{{form.key.slice(-1)[0]}}\"></div><div class=\"help-block\" sf-message=\"form.isTranslatedLabels ? form.description : translate(form.description, form.descriptionKey)\"></div></div>"
  );

}]);


/****************************************
    FILE: src/components/web/sf-error-message.service.js
****************************************/

/**
 * This file overrides sfErrorMessage from Angular Schema Form and
 * it enables translation of error messages (using NSP.Translate.t method).
 */

angular.module('schemaForm').provider('sfErrorMessage', function() {

  // The codes are tv4 error codes.
  // Not all of these can actually happen in a field, but for
  // we never know when one might pop up so it's best to cover them all.

  // TODO: Humanize these.
  var defaultMessages = {
    'default': NSP.Translate.t('SchemaFormValidate'),
    0: NSP.Translate.t('SchemaFormInvalidType'),
    1: NSP.Translate.t('SchemaFormNoEnumMatch'),
    10: NSP.Translate.t('SchemaFormDataDoesNotMatchAnyOf'),
    11: NSP.Translate.t('SchemaFormDataDoesNotMatchOneOf'),
    12: NSP.Translate.t('SchemaFormDataIsValidAgainstOneOf'),
    13: NSP.Translate.t('SchemaFormDataMatchesSchemFromNot'),
    // Numeric errors
    100: NSP.Translate.t('SchemaFormNotMultipleOf'),
    101: NSP.Translate.t('SchemaFormValueIsLessThan'),
    102: NSP.Translate.t('SchemaFormValueIsEqualThan'),
    103: NSP.Translate.t('SchemaFormValueIsGreaterThan'),
    104: NSP.Translate.t('SchemaFormValueIsEqualTo'),
    105: NSP.Translate.t('SchemaFormNotAValidNumber'),
    // String errors
    200: NSP.Translate.t('SchemaFormStringTooShort'),
    201: NSP.Translate.t('SchemaFormStringTooLong'),
    202: NSP.Translate.t('SchemaFormStringDoesNotMatchPattern'),
    // Object errors
    300: NSP.Translate.t('SchemaFormTooFewProperties'),
    301: NSP.Translate.t('SchemaFormTooManyProperties'),
    302: NSP.Translate.t('SchemaFormRequired'),
    303: NSP.Translate.t('SchemaFormAdditionalPropertiesNotAllowed'),
    304: NSP.Translate.t('SchemaFormDependencyFailed'),
    // Array errors
    400: NSP.Translate.t('SchemaFormArrayTooShort'),
    401: NSP.Translate.t('SchemaFormArrayTooLong'),
    402: NSP.Translate.t('SchemaFormArrayItemsNotUnique'),
    403: NSP.Translate.t('SchemaFormAdditionalItemsNotAllowed'),
    // Format errors
    500: NSP.Translate.t('SchemaFormValidationFailed'),
    501: NSP.Translate.t('SchemaFormKeywordFailed'),
    // Schema structure
    600: NSP.Translate.t('SchemaFormCurcular'),
    // Non-standard validation options
    1000: NSP.Translate.t('SchemaFormUnknowProperty')
  };

  // In some cases we get hit with an angular validation error
  defaultMessages.number    = defaultMessages[105];
  defaultMessages.required  = defaultMessages[302];
  defaultMessages.min       = defaultMessages[101];
  defaultMessages.max       = defaultMessages[103];
  defaultMessages.maxlength = defaultMessages[201];
  defaultMessages.minlength = defaultMessages[200];
  defaultMessages.pattern   = defaultMessages[202];

  this.setDefaultMessages = function(messages) {
    defaultMessages = messages;
  };

  this.getDefaultMessages = function() {
    return defaultMessages;
  };

  this.setDefaultMessage = function(error, msg) {
    defaultMessages[error] = msg;
  };

  this.$get = ['$interpolate', function($interpolate) {

    var service = {};
    service.defaultMessages = defaultMessages;

    /**
     * Interpolate and return proper error for an eror code.
     * Validation message on form trumps global error messages.
     * and if the message is a function instead of a string that function will be called instead.
     * @param {string} error the error code, i.e. tv4-xxx for tv4 errors, otherwise it's whats on
     *                       ngModel.$error for custom errors.
     * @param {Any} value the actual model value.
     * @param {Any} viewValue the viewValue
     * @param {Object} form a form definition object for this field
     * @param  {Object} global the global validation messages object (even though its called global
     *                         its actually just shared in one instance of sf-schema)
     * @return {string} The error message.
     */
    service.interpolate = function(error, value, viewValue, form, global) {
      global = global || {};
      var validationMessage = form.validationMessage || {};

      // Drop tv4 prefix so only the code is left.
      if (error.indexOf('tv4-') === 0) {
        error = error.substring(4);
      }

      // First find apropriate message or function
      var message = validationMessage['default'] || global['default'] || '';

      [validationMessage, global, defaultMessages].some(function(val) {
        if (angular.isString(val) || angular.isFunction(val)) {
          message = val;
          return true;
        }
        if (val && val[error]) {
          message = val[error];
          return true;
        }
      });

      var context = {
        error: error,
        value: value,
        viewValue: viewValue,
        form: form,
        schema: form.schema,
        title: form.title || (form.schema && form.schema.title)
      };
      if (angular.isFunction(message)) {
        return message(context);
      } else {
        return $interpolate(message)(context);
      }
    };

    return service;
  }];

});


/****************************************
    FILE: src/components/web/accordion/accordion-header.directive.js
****************************************/

angular.module('formEngine')
    .directive('accordionHeader', ['feService', function(feService) {
        return {
            restrict: 'AE',
            scope: {
                options: "="
            },
            templateUrl: '/web/accordion/accordion-header.template.html',
            link: function(scope, element, attrs, ngModel) {

                var defaultOptions = {
                        title: '',
                        titleKey: '',
                        collapsed: false
                    },
                    fieldsetElement = element.closest('fieldset');

                scope.settings = angular.merge({}, defaultOptions, scope.options);

                scope.toggle = function($event) {
                    fieldsetElement.toggleClass('collapsed');
                    scope.settings.collapsed = !scope.settings.collapsed;
                };
            }
        };
    }]);

/****************************************
    FILE: src/components/web/help/fe-trust-as-html.directive.js
****************************************/

(function() {
    'use strict';
    angular.module('formEngine').directive('feTrustAsHtml', feTrustAsHtml);

    feTrustAsHtml.$inject = ['$compile', 'feService'];

    function feTrustAsHtml($compile, feService) {
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;
        function link(scope, element, attrs, ngModel) {
            scope.decode = feService.htmlDecode;
            var data = attrs.feTrustAsHtml;
            var isEncoded = scope.$eval(attrs.encoded);
            if(isEncoded === false){
                attrs.$set('ngBindHtml', data);
            }else{
                attrs.$set('ngBindHtml', 'decode(' + data + ')');
            }
            element.removeAttr('fe-trust-as-html');
            $compile(element)(scope);
        }
    }

})();

/****************************************
    FILE: src/components/web/kendo-combo-box/fe-kendo-combo-box.directive.js
****************************************/

(function() {
    'use strict';
    angular.module('formEngine').directive('feKendoComboBox', feKendoComboBox);

    feKendoComboBox.$inject = ['$templateCache', '$http', '$q', '$timeout', 'feService', 'kendoWindowService'];

    function feKendoComboBox($templateCache, $http, $q, $timeout, feService, kendoWindowService) {
        var directive = {
                link: link,
                templateUrl: '/web/kendo-combo-box/fe-kendo-combo-box.template.html',
                restrict: 'AE',
                require: 'ngModel',
                scope: {
                    options: '=feOptions',
                    ngModel: '=ngModel'
                }
            };

        return directive;

        function link(scope, element, attrs, ngModelCtrl) {
            var defaultOptions = {
                    allowNewValue: false,
                    kendoOptions: {
                        enable: true,
                        filter: 'contains',
                        text: '',
                        autoBind: false
                    }
                },
                sfFormElement = element.closest('[sf-form]'),
                sfFormScope = sfFormElement.scope(),
                dataValueField = 'id',
                dataTextField = 'title';

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = feService.translate;
            scope.widgetModel = angular.copy(scope.ngModel);
            scope.changeHandler = changeHandler;
            scope.clearHandler = clearHandler;
            scope.setValue = setValue;
            scope.clearButton = false;
            dataValueField = scope.settings.kendoOptions.dataValueField;
            dataTextField = scope.settings.kendoOptions.dataTextField;

            if (feService.propertyExists(scope.settings, 'kendoOptions.dataSource.transport.read') &&
                feService.propertyExists(scope.settings, 'kendoOptions.dataSource.serverFiltering') &&
                scope.settings.kendoOptions.dataSource.serverFiltering &&
                !feService.propertyExists(scope.settings, 'kendoOptions.dataSource.transport.parameterMap')) {
                scope.settings.kendoOptions.dataSource.transport.parameterMap = function(data, type) {
                    return feService.autoCompleteParameterMap(scope.settings, data);
                };
            }

            if (scope.ngModel && !angular.isUndefined(scope.ngModel[dataValueField]) && scope.ngModel[dataValueField] !== null && scope.ngModel[dataValueField].toString()) {
                scope.settings.kendoOptions.value = scope.ngModel[dataValueField];
                scope.settings.kendoOptions.text = scope.ngModel[dataTextField];
                scope.clearButton = true;
            } else {
                ngModelCtrl.$setViewValue(undefined);
                scope.ngModel = undefined;
            }

            if (scope.settings.kendoOptions && !scope.settings.kendoOptions.template && scope.settings.kendoOptions.templateCache) {
                scope.settings.kendoOptions.template = $templateCache.get(scope.settings.kendoOptions.templateCache);
            }

            if (scope.settings.kendoOptions && scope.settings.readonly === true) {
                scope.settings.kendoOptions.enable = false;
            }

            if (scope.settings.placeholder && scope.settings.kendoOptions && !scope.settings.kendoOptions.placeholder) {
                if (scope.settings.isTranslatedLabels) {
                    scope.settings.kendoOptions.placeholder = scope.settings.placeholder;
                } else {
                    scope.settings.kendoOptions.placeholder = translate(scope.settings.placeholder, scope.settings.placeholderKey);
                }
            }

            scope.$on('kendoWidgetCreated', function(event) {
                scope.widget.bind('change', changeHandler);
                if (feService.propertyExists(scope.settings, 'kendoOptions.dataSource.transport.read')) {
                    scope.widget.bind('dataBound', initialDataBound);
                }
                element.find('.form-control.k-input').on('focusout', focusOutHandler);
                element.find('.form-control.k-input').on('keyup', function() {
                    var value = $(this).val();
                    if (scope.settings.required) {
                        ngModelCtrl.$setValidity('tv4-302', value ? true : false);
                    }
                    scope.clearButton = value ? true : false;
                    $timeout(function(){
                        scope.$digest();
                    }, 0);
                });
            });

            scope.$on('$destroy', function() {
                if (scope.settings.condition) {
                    ngModelCtrl.$setViewValue(undefined);
                }
            });

            scope.$on('setValue', function(event, dataItem) {
                setValue(dataItem);
            });

            feService.updateDependants(scope.settings, scope.ngModel, sfFormElement);

            function changeHandler() {
                var widgetValue = scope.widget.value(),
                    inputValue = element.find('.form-control.k-input').val(),
                    dataItem = scope.widget.dataItem(),
                    modelValue = scope.ngModel ? angular.copy(scope.ngModel) : {};

                if (widgetValue && dataItem) {
                    modelValue[dataValueField] = dataItem[dataValueField];
                    modelValue[dataTextField] = dataItem[dataTextField];
                    if (scope.settings.kendoOptions.dataAdditionalFields) {
                        angular.forEach(scope.settings.kendoOptions.dataAdditionalFields, function(additionalField, key) {
                            modelValue[additionalField] = dataItem[additionalField];
                        });
                    }
                    scope.clearButton = true;
                } else if (scope.settings.allowNewValue && (widgetValue || inputValue)) {
                    modelValue[dataValueField] = 0;
                    modelValue[dataTextField] = inputValue;
                    scope.clearButton = true;
                } else {
                    scope.clearButton = false;
                    modelValue = undefined;
                    scope.widget.value('');
                }
                ngModelCtrl.$setViewValue(modelValue);
                if (scope.settings.kendoOptions.dependants && scope.settings.kendoOptions.dependants.length) {
                    feService.updateDependants(scope.settings, modelValue, sfFormElement);
                }
                scope.$emit('schemaFormValidate');
            }

            function setValue(dataItem) {
                var widgetData = scope.widget.dataSource.data(),
                    itemExists = false;

                angular.forEach(widgetData, function(item, key) {
                    if (item[dataValueField] === dataItem[dataValueField]) {
                        itemExists = true;
                    }
                });

                if (!itemExists) {
                    scope.widget.dataSource.add(dataItem);
                }

                ngModelCtrl.$setViewValue(dataItem);
                scope.widget.value(dataItem[dataValueField]);
                scope.$emit('schemaFormValidate');
                feService.updateDependants(scope.settings, ngModelCtrl.$viewValue, sfFormElement);
                scope.clearButton = true;
            }

            function initialDataBound() {
                var data = scope.widget.dataSource.data(),
                    modelValueFound = false;
                if (scope.ngModel && scope.ngModel[dataValueField] && scope.ngModel[dataValueField]) {
                    if (data && data.length) {
                        for (var i = 0; i < data.length; i++) {
                            var dataItem = data[i];
                            if (dataItem[dataValueField] === scope.ngModel[dataValueField]) {
                                modelValueFound = true;
                                break;
                            }
                        }
                    }
                    if (!modelValueFound) {
                        scope.widget.dataSource.insert(0, scope.ngModel);
                        scope.widget.value(scope.ngModel[dataValueField]);
                    }
                }
                scope.widget.unbind('dataBound', initialDataBound);
            }

            function clearHandler(validate) {
                ngModelCtrl.$setViewValue(undefined);
                scope.widgetModel = null;
                scope.widget.value('');
                scope.clearButton = false;
                feService.updateDependants(scope.settings, ngModelCtrl.$viewValue, sfFormElement);
                if (validate) {
                    scope.$emit('schemaFormValidate');
                }
            }

            function focusOutHandler(event) {
                var inputValue = element.find('.form-control.k-input').val(),
                    modelText = ngModelCtrl.$viewValue ? ngModelCtrl.$viewValue[dataTextField] : "";
                if (inputValue && !modelText) {
                    scope.widget.trigger('change');
                }
            }
            
            scope.browseWindow = function() {
                if (scope.settings.browse.url) {
                    var dialogId = 'formEngineBrowseWindow',
                        windowDefaultOptions = {
                            title: scope.settings.browse.title ? feService.translate(scope.settings.browse.title, scope.settings.browse.titleKey) : feService.translate('Browse', 'Browse'),
                            modal: true,
                            width: 800,
                            maxHeight: 400,
                            height: "90%",
                            content: {
                                url: scope.settings.browse.url
                            }
                        },
                        windowSettings = angular.merge(windowDefaultOptions, scope.settings.browse.windowOptions);

                    var windowData = {
                        form: scope.settings,
                        model: ngModelCtrl.$viewValue
                    };

                    if ($templateCache.get(scope.settings.browse.url)) {
                        delete windowSettings.content.url;
                        windowSettings.content.template = $templateCache.get(scope.options.browse.url);
                    }

                    kendoWindowService.create(dialogId, windowData, windowSettings);
                } else {
                    throw new Error("Form Engine: Browse window url undefined.");
                }
            };

        }
    }

})();

/****************************************
    FILE: src/components/web/kendo-date-time-picker/fe-kendo-date-time-picker.directive.js
****************************************/

(function() {
    'use strict';

    angular
        .module('formEngine')
        .directive('feKendoDateTimePicker', feKendoDateTimePicker);

    feKendoDateTimePicker.$inject = ['$templateCache', '$compile', 'feService'];

    /* @ngInject */
    function feKendoDateTimePicker($templateCache, $compile, feService) {
        var directive = {
            link: link,
            restrict: 'AE',
            require: 'ngModel',
            scope: {
                options: '=feOptions',
                ngModel: '=ngModel'
            }
        };
        return directive;

        function link(scope, element, attrs, ngModelCtrl) {
            var templates = {
                    'kendo-date-picker': '/web/kendo-date-time-picker/templates/fe-kendo-date-picker.template.html',
                    'kendo-time-picker': '/web/kendo-date-time-picker/templates/fe-kendo-time-picker.template.html',
                    'kendo-date-time-picker': '/web/kendo-date-time-picker/templates/fe-kendo-date-time-picker.template.html'
                },
                compiledTemplate = '',
                formats = {
                    'kendo-date-picker': 'd',
                    'kendo-time-picker': 't',
                    'kendo-date-time-picker': 'g'
                },
                defaultOptions = {
                    template: templates[scope.options.type] ? templates[scope.options.type] : "",
                    valuePrefix: "#",
                    validationMessage: {
                        formatNotValid: feService.translate('Format not allowed', 'Common.FormatNotAllowed') + ". " + feService.translate('Allowed format', 'Common.AllowedFormat') + ': ' + kendo.culture().calendar.patterns[formats[scope.options.type]]
                    },
                    kendoOptions: {
                        format: formats[scope.options.type]
                    }
                };

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = feService.translate;
            scope.widgetText = scope.ngModel ? scope.ngModel.text : "";
            scope.options.validationMessage = scope.settings.validationMessage;

            if (scope.settings.placeholder && scope.settings.kendoOptions && !scope.settings.kendoOptions.placeholder) {
                if (scope.settings.isTranslatedLabels) {
                    scope.settings.kendoOptions.placeholder = scope.settings.placeholder;
                } else {
                    scope.settings.kendoOptions.placeholder = translate(scope.settings.placeholder, scope.settings.placeholderKey);
                }
            }

            if (scope.ngModel && !angular.isUndefined(scope.ngModel.value) && scope.ngModel.value !== null && scope.ngModel.value !== '' && scope.ngModel.text !== '') {
                scope.widgetText = kendo.toString(kendo.parseDate(scope.ngModel.value), scope.settings.kendoOptions.format);
            } else {
                ngModelCtrl.$setViewValue(undefined);
                scope.ngModel = undefined;
            }

            scope.$watch('widgetText', function(newValue, oldValue) {
                var inputValue = $(scope.widget.element[0]).val();
                if (scope.settings.required && ngModelCtrl.$dirty) {
                    if (newValue) {
                        ngModelCtrl.$setValidity('tv4-302', true);
                    } else {
                        ngModelCtrl.$setValidity('tv4-302', false);
                    }
                }
                if (newValue !== oldValue) {
                    change(inputValue);
                }
            });

            scope.$on('kendoWidgetCreated', function(event) {
                scope.widget.bind('close', function(event) {
                    var value = kendo.toString(this.value(), scope.settings.kendoOptions.format);
                    if (value !== scope.widgetText && ngModelCtrl.$dirty) {
                        scope.widgetText = value;
                        change(value);
                    }
                });
            });

            element.html($templateCache.get(templates[scope.settings.type]));
            $compile(element.contents())(scope);

            function change(widgetValue) {
                var modelValue;
                if (!angular.isUndefined(widgetValue) && widgetValue !== null && widgetValue !== '') {
                    if (validDate(widgetValue)) {
                        modelValue = {
                            text: kendo.toString(widgetValue, scope.settings.kendoOptions.format),
                            value: scope.settings.valuePrefix + kendo.parseDate(widgetValue, scope.settings.kendoOptions.format).toISOString()
                        };
                        ngModelCtrl.$setValidity('formatNotValid', true);
                    } else {
                        ngModelCtrl.$setValidity('formatNotValid', false);
                    }
                } else {
                    ngModelCtrl.$setValidity('formatNotValid', true);
                }
                ngModelCtrl.$setViewValue(modelValue);
                scope.$emit('schemaFormValidate');
            }

            function validDate(value) {
                if (!angular.isUndefined(value) && value !== null && kendo.parseDate(value, scope.settings.kendoOptions.format)) {
                    return true;
                }
            }

        }

    }
})();

/****************************************
    FILE: src/components/web/kendo-drop-down-list/fe-kendo-drop-down-list.directive.js
****************************************/

(function() {
    'use strict';
    angular.module('formEngine').directive('feKendoDropDownList', feKendoDropDownList);

    feKendoDropDownList.$inject = ['$templateCache', '$http', '$q', '$timeout', 'feService'];

    function feKendoDropDownList($templateCache, $http, $q, $timeout, feService) {
        var directive = {
                link: link,
                templateUrl: '/web/kendo-drop-down-list/fe-kendo-drop-down-list.template.html',
                restrict: 'AE',
                require: 'ngModel',
                scope: {
                    options: '=feOptions',
                    ngModel: '=ngModel'
                }
            };

        return directive;

        function link(scope, element, attrs, ngModelCtrl) {

            var defaultOptions = {
                    kendoOptions: {
                        optionLabel: feService.translate('Select...', 'SelectDotDotDot'),
                        enable: true,
                        autoBind: false
                    }
                },
                sfFormElement = element.closest('[sf-form]'),
                sfFormScope = sfFormElement.scope(),
                dataValueField = 'id',
                dataTextField = 'title';

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = feService.translate;
            scope.widgetModel = angular.copy(scope.ngModel);
            scope.changeHandler = changeHandler;
            scope.clearHandler = clearHandler;
            scope.setValue = setValue;
            dataValueField = scope.settings.kendoOptions.dataValueField;
            dataTextField = scope.settings.kendoOptions.dataTextField;

            if (feService.propertyExists(scope.settings, 'kendoOptions.dataSource.transport.read') &&
                feService.propertyExists(scope.settings, 'kendoOptions.dataSource.serverFiltering') &&
                scope.settings.kendoOptions.dataSource.serverFiltering &&
                !feService.propertyExists(scope.settings, 'kendoOptions.dataSource.transport.parameterMap')) {
                scope.settings.kendoOptions.dataSource.transport.parameterMap = function(data, type) {
                    return feService.autoCompleteParameterMap(scope.settings, data);
                };
            }

            if (scope.ngModel && !angular.isUndefined(scope.ngModel[dataValueField]) && scope.ngModel[dataValueField] !== null && scope.ngModel[dataValueField].toString()) {
                scope.settings.kendoOptions.value = scope.ngModel[dataValueField];
                scope.settings.kendoOptions.text = scope.ngModel[dataTextField];
            } else {
                ngModelCtrl.$setViewValue(undefined);
                scope.ngModel = undefined;
            }

            if (scope.settings.kendoOptions && !scope.settings.kendoOptions.template && scope.settings.kendoOptions.templateCache) {
                scope.settings.kendoOptions.template = $templateCache.get(scope.settings.kendoOptions.templateCache);
            }

            if (scope.settings.kendoOptions && scope.settings.readonly === true) {
                scope.settings.kendoOptions.enable = false;
            }

            if (scope.settings.kendoOptions && scope.settings.kendoOptions.optionLabelKey) {
                scope.settings.kendoOptions.optionLabel = feService.translate(scope.settings.kendoOptions.optionLabel, scope.settings.kendoOptions.optionLabelKey);
            }

            scope.$on('kendoWidgetCreated', function(event) {
                scope.widget.bind('change', changeHandler);
                if (!scope.settings.kendoOptions.filter) {
                    scope.widget.bind('dataBound', dropDownSearchFieldOption);
                }
                if (feService.propertyExists(scope.settings, 'kendoOptions.dataSource.transport.read')) {
                    scope.widget.bind('dataBound', initialDataBound);
                }                
            });

            scope.$on('$destroy', function() {
                if (scope.settings.condition) {
                    ngModelCtrl.$setViewValue(undefined);
                }
            });

            scope.$on('setValue', function(event, dataItem) {
                setValue(dataItem);
            });

            feService.updateDependants(scope.settings, scope.ngModel, sfFormElement);

            function changeHandler() {
                var widgetValue = scope.widget.value(),
                    dataItem = scope.widget.dataItem(),
                    modelValue = scope.ngModel ? angular.copy(scope.ngModel) : {};

                if (widgetValue && dataItem) {
                    modelValue[dataValueField] = dataItem[dataValueField];
                    modelValue[dataTextField] = dataItem[dataTextField];
                    if (scope.settings.kendoOptions.dataAdditionalFields) {
                        angular.forEach(scope.settings.kendoOptions.dataAdditionalFields, function(additionalField, key) {
                            modelValue[additionalField] = dataItem[additionalField];
                        });
                    }
                } else {
                    modelValue = undefined;
                }
                ngModelCtrl.$setViewValue(modelValue);
                if (scope.settings.kendoOptions.dependants && scope.settings.kendoOptions.dependants.length) {
                    feService.updateDependants(scope.settings, modelValue, sfFormElement);
                }
            }

            function setValue(dataItem) {
                var widgetData = scope.widget.dataSource.data(),
                    itemExists = false;

                angular.forEach(widgetData, function(item, key) {
                    if (item[dataValueField] === dataItem[dataValueField]) {
                        itemExists = true;
                    }
                });

                if (!itemExists) {
                    scope.widget.dataSource.add(dataItem);
                }

                ngModelCtrl.$setViewValue(dataItem);
                scope.widget.value(dataItem[dataValueField]);
                scope.$emit('schemaFormValidate');
                feService.updateDependants(scope.settings, ngModelCtrl.$viewValue, sfFormElement);
            }

            function dropDownSearchFieldOption(event) {
                var total = event.sender.dataSource.total();
                scope.widget.setOptions({
                    filter: total > 10 ? "contains" : "none"
                });
                scope.widget.unbind('dataBound', dropDownSearchFieldOption);
                if (total > 10) {
                    scope.widget.refresh();
                }
            }

            function initialDataBound() {
                var data = scope.widget.dataSource.data(),
                    modelValueFound = false;
                if (scope.ngModel && scope.ngModel[dataValueField] && scope.ngModel[dataValueField]) {
                    if (data && data.length) {
                        for (var i = 0; i < data.length; i++) {
                            var dataItem = data[i];
                            if (dataItem[dataValueField] === scope.ngModel[dataValueField]) {
                                modelValueFound = true;
                                break;
                            }
                        }
                    }
                    if (!modelValueFound) {
                        scope.widget.dataSource.insert(0, scope.ngModel);
                        scope.widget.value(scope.ngModel[dataValueField]);
                    }
                }
                scope.widget.unbind('dataBound', initialDataBound);
            }

            function clearHandler() {
                ngModelCtrl.$setViewValue(undefined);
                scope.widgetModel = null;
                scope.widget.value('');
                feService.updateDependants(scope.settings, ngModelCtrl.$viewValue, sfFormElement);
            }

        }
    }

})();

/****************************************
    FILE: src/components/web/kendo-editor/fe-kendo-editor.directive.js
****************************************/

(function() {
    'use strict';
    angular.module('formEngine').directive('feKendoEditor', feKendoEditor);

    feKendoEditor.$inject = ['$templateCache', '$http', '$q', '$timeout', 'feService'];

    function feKendoEditor($templateCache, $http, $q, $timeout, feService) {
        var directive = {
                link: link,
                templateUrl: '/web/kendo-editor/fe-kendo-editor.template.html',
                restrict: 'AE',
                require: 'ngModel',
                scope: {
                    options: '=feOptions',
                    ngModel: '=ngModel'
                }
            };

        return directive;

        function link(scope, element, attrs, ngModelCtrl) {

            var defaultOptions = {
                    kendoOptions: {
                        encoded: true,
                        resizable: {
                            min: 150
                        }
                    }
                };

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = feService.translate;

            if (scope.settings.kendoOptions && scope.settings.readonly === true) {
                scope.settings.kendoOptions.enable = false;
            }

            if (scope.ngModel) {
                scope.widgetValue = scope.ngModel;
                if (scope.settings.kendoOptions.encoded) {
                    ngModelCtrl.$setViewValue(feService.htmlEncode(scope.ngModel));
                }
            }

            scope.$on('kendoWidgetCreated', function(event) {
                scope.widget.bind('change', change);
            });

            function change(event) {
                if (scope.widgetValue) {
                    ngModelCtrl.$setViewValue(scope.widget.textarea.val());
                } else {
                    ngModelCtrl.$setViewValue(undefined);
                }
            }

        }
    }

})();

/****************************************
    FILE: src/components/web/kendo-image-upload/fe-kendo-image-upload.directive.js
****************************************/

(function() {
    'use strict';
    feKendoImageUpload.$inject = ["$templateCache", "$http", "$q", "$timeout", "$compile", "feService"];
    angular.module('formEngine').directive('feKendoImageUpload', feKendoImageUpload);

    /* @ngInject */
    function feKendoImageUpload($templateCache, $http, $q, $timeout, $compile, feService) {
        var directive = {
            link: link,
            restrict: 'AE',
            require: 'ngModel',
            scope: {
                options: '=feOptions',
                ngModel: '=ngModel'
            }
        };
        return directive;

        function link(scope, element, attrs, ngModelCtrl) {

            var defaultOptions = {
                template: '/web/kendo-image-upload/fe-kendo-image-upload.template.html',
                allowedExtensions: ['jpg', 'jpeg', 'png', 'ico'], 
                kendoOptions: {
                    multiple: false,
                    showFileList: false,
                    async: {
                        saveUrl: '/NSPFormEngine/SaveBlobImage',
                        saveField: 'blobImages',
                        autoUpload: true
                    },
                    localization: {
                        select: feService.translate("Select files...", 'Common.SelectFilesDotDotDot')
                    }
                },
                validationMessage: {}
            };

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = feService.translate;
            scope.remove = remove;
            scope.browseFile = browseFile;
            scope.uploading = false;

            if (!scope.options.validationMessage) {
                if (!scope.settings.validationMessage.extensionNotValid) {
                    scope.settings.validationMessage.extensionNotValid = feService.translate('File type not valid. Allowed extensions', 'Common.FileTypeNotValid') + ': ' + scope.settings.allowedExtensions.join(', ') + '.';
                }
                if (!scope.settings.validationMessage.uploadFailed) {
                    scope.settings.validationMessage.uploadFailed = feService.translate('Upload failed.', 'Common.UploadFailed');
                }

                if (!scope.settings.validationMessage.uploadPending) {
                    scope.settings.validationMessage.uploadPending = feService.translate('Upload pending.', 'Common.UploadPending');
                }
            }

            scope.options.validationMessage = scope.settings.validationMessage;

            if (scope.ngModel && scope.ngModel.id) {
                scope.selectedImage = angular.copy(scope.ngModel);
            }

            scope.$on('kendoWidgetCreated', function(event) {  
                scope.widget.bind('select', onSelect);
                scope.widget.bind('success', onSuccess);
                scope.widget.bind('error', onError);
            });

            scope.$on('$destroy', function() {
                if (scope.settings.condition) {
                    ngModelCtrl.$setViewValue(undefined);
                }
            });

            scope.$on('schemaFormValidate', function(event) {
                // Set uploadPending error only if form has been submitted
                if (event.targetScope.$id !== scope.$id) {
                    ngModelCtrl.$setValidity('uploadPending', !scope.uploading);
                }
            });

            element.html($templateCache.get(scope.settings.template));
            $compile(element.contents())(scope);

            function browseFile() {
                element.find('input[type="file"]').trigger('click');
            }

            function onSelect(event) {
                var files = event.files;
                angular.forEach(files, function(file, key) {
                    scope.selectedImage = {
                        name: file.name
                    };
                    if (validateFileExtension(file)) {
                        ngModelCtrl.$setViewValue(scope.selectedImage);
                        ngModelCtrl.$setValidity('extensionNotValid', true);
                        scope.uploading = true;
                    } else {
                        ngModelCtrl.$setViewValue(scope.selectedImage);
                        ngModelCtrl.$setValidity('extensionNotValid', false);
                        event.preventDefault();
                        scope.uploading = false;
                    }
                });
                ngModelCtrl.$setValidity('uploadFailed', true);
                scope.$emit('schemaFormValidate');
            }

            function onSuccess(event) {
                var response = event.response;
                if (scope.selectedImage) {
                    scope.selectedImage.id = response.pathToBeStored;
                    scope.selectedImage.preview = response.pathToBeDisplay;
                }
                ngModelCtrl.$setValidity('uploadPending', true);
                ngModelCtrl.$setViewValue(scope.selectedImage);
                scope.uploading = false;
                $timeout(function() {
                    scope.$digest();
                });
            }

            function validateFileExtension(file) {
                var valid = false,
                    extension = file.extension.replace('.', ''),
                    extensions = {};
                for (var i = 0; i < scope.settings.allowedExtensions.length; i++) {
                    extensions[scope.settings.allowedExtensions[i]] = 1;
                }
                if (!!extensions[extension]) {
                    valid = true;
                } else {
                    valid = false;
                }
                return valid;
            }

            function remove() {
                scope.selectedImage = null;
                ngModelCtrl.$setValidity('extensionNotValid', true);
                ngModelCtrl.$setValidity('uploadFailed', true);
                ngModelCtrl.$setViewValue(undefined);
                element.find('.k-upload-action .k-cancel').trigger('click');
                scope.uploading = false;
                scope.$emit('schemaFormValidate');
            }

            function onError(event) {
                ngModelCtrl.$setValidity('uploadFailed', false);
                scope.uploading = false;
                scope.$emit('schemaFormValidate');
            }
        }
    }
})();

/****************************************
    FILE: src/components/web/kendo-multi-select/fe-kendo-multi-select.directive.js
****************************************/

(function() {
    'use strict';
    angular.module('formEngine').directive('feKendoMultiSelect', feKendoMultiSelect);

    feKendoMultiSelect.$inject = ['$templateCache', '$http', '$q', '$timeout', 'feService'];

    function feKendoMultiSelect($templateCache, $http, $q, $timeout, feService) {
        var directive = {
                link: link,
                templateUrl: '/web/kendo-multi-select/fe-kendo-multi-select.template.html',
                restrict: 'AE',
                require: 'ngModel',
                scope: {
                    options: '=feOptions',
                    ngModel: '=ngModel'
                }
            };

        return directive;

        function link(scope, element, attrs, ngModelCtrl) {

            var defaultOptions = {
                    kendoOptions: {
                        autoBind: false,
                        dataTextField: "Text",
                        dataValueField: "Id",
                        minLength: 3,
                        dataSource: {
                            data: scope.ngModel,
                            serverFiltering: true
                        }
                    }
                };

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = feService.translate;
            scope.onChange = onChange;
            scope.widgetModel = angular.copy(scope.ngModel);

            if (feService.propertyExists(scope.settings, 'kendoOptions.dataSource.transport.read') &&
                feService.propertyExists(scope.settings, 'kendoOptions.dataSource.serverFiltering') &&
                scope.settings.kendoOptions.dataSource.serverFiltering &&
                !feService.propertyExists(scope.settings, 'kendoOptions.dataSource.transport.parameterMap')) {
                scope.settings.kendoOptions.dataSource.transport.parameterMap = function(data, type) {
                    return feService.autoCompleteParameterMap(scope.settings, data);
                };
            }

            if (scope.ngModel && scope.ngModel.length) {
                
            } else {
                ngModelCtrl.$setViewValue(undefined);
                scope.ngModel = undefined;
            }

            if (scope.settings.kendoOptions && !scope.settings.kendoOptions.template && scope.settings.kendoOptions.templateCache) {
                scope.settings.kendoOptions.template = $templateCache.get(scope.settings.kendoOptions.templateCache);
            }

            if (scope.settings.kendoOptions && scope.settings.readonly === true) {
                scope.settings.kendoOptions.enable = false;
            }

            scope.$on('kendoWidgetCreated', function(event) {
                scope.widget.bind('change', onChange);
                scope.widget.bind('dataBound', initialDataBound);
            });

            scope.$on('$destroy', function() {
                if (scope.settings.condition) {
                    ngModelCtrl.$setViewValue(undefined);
                }
            });

            function onChange(event) {
                var widgetValue = scope.widget.value(),
                    dataItems = scope.widget.dataItems(),
                    modelValue = [];

                if (widgetValue && dataItems && dataItems.length) {
                    angular.forEach(dataItems, function(item, key) {
                        var dataItem = {};
                        dataItem[scope.settings.kendoOptions.dataValueField] = item[scope.settings.kendoOptions.dataValueField];
                        dataItem[scope.settings.kendoOptions.dataTextField] = item[scope.settings.kendoOptions.dataTextField];
                        if (scope.settings.kendoOptions.dataAdditionalFields) {
                            angular.forEach(scope.settings.kendoOptions.dataAdditionalFields, function(additionalField, key) {
                                dataItem[additionalField] = item[additionalField];
                            });
                        }
                        modelValue.push(dataItem);
                    });
                } else {
                    modelValue = undefined;
                }
                ngModelCtrl.$setViewValue(modelValue);
                scope.$emit('schemaFormValidate');
            }

            function initialDataBound() {
                scope.widget.unbind('dataBound', initialDataBound);
                scope.widget.dataSource.data(scope.ngModel);
                scope.widget.value(scope.ngModel);
            }
        }
    }
})();

/****************************************
    FILE: src/components/web/kendo-numeric-text-box/fe-kendo-numeric-text-box.directive.js
****************************************/

(function() {
    'use strict';
    angular.module('formEngine').directive('feKendoNumericTextBox', feKendoNumericTextBox);

    feKendoNumericTextBox.$inject = ['$templateCache', '$http', '$q', '$timeout', 'feService'];

    function feKendoNumericTextBox($templateCache, $http, $q, $timeout, feService) {
        var directive = {
                link: link,
                templateUrl: '/web/kendo-numeric-text-box/fe-kendo-numeric-text-box.template.html',
                restrict: 'AE',
                require: 'ngModel',
                scope: {
                    options: '=feOptions',
                    ngModel: '=ngModel'
                }
            };

        return directive;

        function link(scope, element, attrs, ngModelCtrl) {

            var defaultOptions = {
                    kendoOptions: {
                        format: "{n:0}",
                        decimals: 0,
                        spinners: false,
                        step: 1,
                        selectOnFocus: false
                    }
                };

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = feService.translate;
            scope.widgetModel = "";
            scope.changeHandler = changeHandler;

            if (scope.settings.kendoOptions && scope.settings.readonly === true) {
                scope.settings.kendoOptions.enable = false;
            }

            if (scope.ngModel !== null && !angular.isUndefined(scope.ngModel) && scope.ngModel.toString()) {
                scope.widgetModel = scope.ngModel;
            } else {
                scope.ngModel = undefined;
                ngModelCtrl.$setViewValue(undefined);
            }

            if (scope.settings.placeholder && scope.settings.kendoOptions && !scope.settings.kendoOptions.placeholder) {
                if (scope.settings.isTranslatedLabels) {
                    scope.settings.kendoOptions.placeholder = scope.settings.placeholder;
                } else {
                    scope.settings.kendoOptions.placeholder = translate(scope.settings.placeholder, scope.settings.placeholderKey);
                }
            }

            scope.$on('$destroy', function() {
                if (scope.settings.condition) {
                    ngModelCtrl.$setViewValue(undefined);
                }
            });

            ngModelCtrl.$viewChangeListeners.push(function() {
                scope.widgetModel = ngModelCtrl.$viewValue;
            });

            scope.$on('kendoWidgetCreated', function(event) {
                var input = element.find('.k-input');
                scope.widget.bind('change', changeHandler);
                if (feService.propertyExists(scope.settings, 'kendoOptions.selectOnFocus') && scope.settings.kendoOptions.selectOnFocus) {
                    element.find('.k-input').on('focus', function(event) {
                        var that = $(this);
                        var selectTimeId = setTimeout(function() {
                            that.select();
                        });
                    }).blur(function(e) {
                        clearTimeout($(this).data("selectTimeId"));
                    });
                }
            });

            function changeHandler() {
                var widgetValue = scope.widget.value(),
                    modelValue;

                if (widgetValue !== null) {
                    modelValue = widgetValue.toString();
                } else {
                    modelValue = undefined;
                }
                
                ngModelCtrl.$setViewValue(modelValue);
            }

        }
    }

})();

/****************************************
    FILE: src/components/web/template-picker/fe-template-picker.directive.js
****************************************/

(function() {
    'use strict';
    angular.module('formEngine').directive('feTemplatePicker', feTemplatePicker);

    feTemplatePicker.$inject = ['$templateCache', '$http', '$q', '$timeout', 'feService', 'kendoWindowService'];

    function feTemplatePicker($templateCache, $http, $q, $timeout, feService, kendoWindowService) {
        var directive = {
            link: link,
            templateUrl: '/web/template-picker/templates/fe-template-picker.template.html',
            restrict: 'AE',
            require: 'ngModel',
            scope: {
                options: '=feOptions',
                ngModel: '=ngModel'
            }
        };

        return directive;

        function link(scope, element, attrs, ngModelCtrl) {
            var defaultOptions = {
                'readonly': true
            };
            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = feService.translate;
            scope.langId = scope.settings.currentLang;

            function setViewVal(model) {
                if (model.length === 0) {
                    scope.viewVal = '';
                } else {
                    for (var i = 0; i < model.length; i++) {
                        if (model[i].languageId === scope.settings.currentLang) {
                            scope.viewVal = model[i].subject;
                        }
                    }
                }
            }

            scope.openPicker = function() {
                var dialogId = 'templatePickerWindow',
                    windowDefaultOptions = {
                        title: scope.settings.window.title ? feService.translate(scope.settings.window.title, scope.settings.window.titleKey) : feService.translate('ChooseTemplate', 'Common.ChooseTemplate'),
                        modal: true,
                        width: 800,
                        maxHeight: 555,
                        height: "90%",
                        content: {
                            url: scope.settings.window.url
                        }
                    },
                    windowSettings = angular.merge(windowDefaultOptions, scope.settings.window.windowOptions);

                var windowData = {
                    form: scope.settings,
                    model: ngModelCtrl.$viewValue && ngModelCtrl.$viewValue.length > 0 ? ngModelCtrl.$viewValue : [{
                        "languageId": 1,
                        "languageName": "English",
                        "languageNameKey": "SysLanguage.en-US",
                        "subject": "",
                    }]
                };
                kendoWindowService.create(dialogId, windowData, windowSettings);
            };

            setViewVal(scope.ngModel);

            scope.$on('templateSet', function(event, model) {
                setViewVal(model);
                if (scope.viewVal === "" && model.length === 1) {
                    ngModelCtrl.$setViewValue(undefined);
                } else {
                    ngModelCtrl.$setViewValue(model);
                }
            });
        }
    }

})();

/****************************************
    FILE: src/components/web/ticket-description/fe-ticket-description.directive.js
****************************************/

(function() {
    'use strict';
    feTicketDescription.$inject = ["$templateCache", "$http", "$q", "$timeout", "$rootScope", "feService"];
    angular.module('formEngine').directive('feTicketDescription', feTicketDescription);

    function feTicketDescription($templateCache, $http, $q, $timeout, $rootScope, feService) {
        var directive = {
            link: link,
            templateUrl: '/web/ticket-description/fe-ticket-description.template.html',
            restrict: 'AE',
            require: 'ngModel',
            scope: {
                options: '=feOptions',
                ngModel: '=ngModel'
            }
        };

        return directive;

        function link(scope, element, attrs, ngModelCtrl) {

            var defaultOptions = {
                editorOptions: {
                    encoded: true,
                    resizable: {
                        min: 150
                    }
                }
            };

            scope.settings = angular.merge({}, defaultOptions, scope.options);
            scope.t = feService.translate;
            scope.editorValue = "";
            scope.attachments = [];

            if (scope.settings.editorOptions && scope.settings.readonly === true) {
                scope.settings.editorOptions.enable = false;
            }

            if (scope.settings.key) {
                scope.settings.attachmentsOptions.fallback_id = "ticketDescriptionFileUpload" + scope.settings.key.slice(-1)[0];
            }

            if (scope.ngModel) {
                scope.editorValue = scope.ngModel.text;
                ngModelCtrl.$setViewValue({
                    text: scope.settings.editorOptions.encoded ? feService.htmlEncode(scope.ngModel.text) : scope.ngModel.text,
                    attachments: scope.ngModel.attachments
                });
            }

            scope.$on('kendoWidgetCreated', function(event) {
                scope.widget.bind('change', change);
            });

            scope.$watch('attachments', function(newValue) {
                ngModelCtrl.$setViewValue({
                    text: scope.widget.textarea.val(),
                    attachments: newValue
                });
            });

            function change(event) {
                if (scope.editorValue || scope.attachments.length) {
                    ngModelCtrl.$setViewValue({
                        text: scope.widget.textarea.val(),
                        attachments: scope.attachments
                    });
                } else {
                    ngModelCtrl.$setViewValue(undefined);
                }
            }

        }
    }
})();

/****************************************
    FILE: src/components/mobile/mobile.decorator.js
****************************************/



/****************************************
    FILE: src/components/web/web.decorator.js
****************************************/

/**
 * Angular Schema Form - Web Decorators
 */

(function() {
    'use strict';

    angular.module('schemaForm').config(config);

    config.$inject = ['schemaFormDecoratorsProvider', 'sfBuilderProvider', 'sfPathProvider'];

    function config(decoratorsProvider, sfBuilderProvider, sfPathProvider) {

        var base = '/web/',
            simpleTransclusion  = sfBuilderProvider.builders.simpleTransclusion,
            ngModelOptions      = sfBuilderProvider.builders.ngModelOptions,
            ngModel             = sfBuilderProvider.builders.ngModel,
            sfField             = sfBuilderProvider.builders.sfField,
            condition           = sfBuilderProvider.builders.condition,
            array               = sfBuilderProvider.builders.array;


        // Tabs is so bootstrap specific that it stays here.
        var tabs = function(args) {
            if (args.form.tabs && args.form.tabs.length > 0) {
                var tabContent = args.fieldFrag.querySelector('.tab-content');
                args.form.tabs.forEach(function(tab, index) {
                    var div = document.createElement('div');
                    div.className = 'tab-pane';
                    div.setAttribute('ng-disabled', 'form.readonly');
                    div.setAttribute('ng-show', 'selected.tab === ' + index);
                    div.setAttribute('ng-class', '{active: selected.tab === ' + index + '}');
                    var childFrag = args.build(tab.items, args.path + '.tabs[' + index + '].items', args.state);
                    div.appendChild(childFrag);
                    tabContent.appendChild(div);
                });
            }
        };

        var defaults = [sfField, ngModel, ngModelOptions, condition];

        decoratorsProvider.defineDecorator('feDecorator', {
            'default': {
                template: base + 'default/default.decorator.html',
                builder: defaults,
                replace: false
            },
            textarea: {
                template: base + 'textarea/textarea.decorator.html',
                builder: defaults,
                replace: false
            },
            fieldset: {
                template: base + 'fieldset/fieldset.decorator.html',
                builder: [sfField, simpleTransclusion, condition],
                replace: false
            },
            array: {
                template: base + 'array/array.decorator.html',
                builder: [sfField, ngModelOptions, ngModel, array, condition],
                replace: false
            },
            tabarray: {
                template: base + 'tabarray/tabarray.decorator.html',
                builder: [sfField, ngModelOptions, ngModel, array, condition],
                replace: false
            },
            tabs: {
                template: base + 'tabs/tabs.decorator.html',
                builder: [sfField, ngModelOptions, tabs, condition],
                replace: false
            },
            section: {
                template: base + 'section/section.decorator.html',
                builder: [sfField, simpleTransclusion, condition],
                replace: false
            },
            conditional: {
                template: base + 'section/section.decorator.html',
                builder: [sfField, simpleTransclusion, condition],
                replace: false
            },
            actions: {
                template: base + 'actions/actions.decorator.html',
                builder: defaults,
                replace: false
            },
            select: {
                template: base + 'select/select.decorator.html',
                builder: defaults,
                replace: false
            },
            checkbox: {
                template: base + 'checkbox/checkbox.decorator.html',
                builder: defaults,
                replace: false
            },
            checkboxes: {
                template: base + 'checkboxes/checkboxes.decorator.html',
                builder: [sfField, ngModelOptions, ngModel, array, condition],
                replace: false
            },
            number: {
                template: base + 'default/default.decorator.html',
                builder: defaults,
                replace: false
            },
            password: {
                template: base + 'default/default.decorator.html',
                builder: defaults,
                replace: false
            },
            submit: {
                template: base + 'submit/submit.decorator.html',
                builder: defaults,
                replace: false
            },
            button: {
                template: base + 'submit/submit.decorator.html',
                builder: defaults,
                replace: false
            },
            radios: {
                template: base + 'radios/radios.decorator.html',
                builder: defaults,
                replace: false
            },
            'radios-inline': {
                template: base + 'radios-inline/radios-inline.decorator.html',
                builder: defaults,
                replace: false
            },
            radiobuttons: {
                template: base + 'radio-buttons/radio-buttons.decorator.html',
                builder: defaults,
                replace: false
            },
            help: {
                template: base + 'help/help.decorator.html',
                builder: defaults,
                replace: false
            },
            accordion: {
                template: base + 'accordion/accordion.decorator.html',
                builder: defaults,
                replace: false
            },
            'kendo-combo-box': {
                template: base + 'kendo-combo-box/kendo-combo-box.decorator.html',
                builder: defaults,
                replace: false
            },
            // Note: kendo-auto-complete is using kendo-combo-box decorator and directives
            'kendo-auto-complete': {
                template: base + 'kendo-combo-box/kendo-combo-box.decorator.html',
                builder: defaults,
                replace: false
            },
            'kendo-drop-down-list': {
                template: base + 'kendo-drop-down-list/kendo-drop-down-list.decorator.html',
                builder: defaults,
                replace: false
            },
            'kendo-multi-select': {
                template: base + 'kendo-multi-select/kendo-multi-select.decorator.html',
                builder: defaults,
                replace: false
            },
            // Note: kendo-date-picker is using kendo-date-time-picker decorator and directives
            'kendo-date-picker': {
                template: base + 'kendo-date-time-picker/kendo-date-time-picker.decorator.html',
                builder: defaults,
                replace: false
            },
            // Note: kendo-time-picker is using kendo-date-time-picker decorator and directives
            'kendo-time-picker': {
                template: base + 'kendo-date-time-picker/kendo-date-time-picker.decorator.html',
                builder: defaults,
                replace: false
            },
            'kendo-date-time-picker': {
                template: base + 'kendo-date-time-picker/kendo-date-time-picker.decorator.html',
                builder: defaults,
                replace: false
            },
            'kendo-numeric-text-box': {
                template: base + 'kendo-numeric-text-box/kendo-numeric-text-box.decorator.html',
                builder: defaults,
                replace: false
            },
            'kendo-editor': {
                template: base + 'kendo-editor/kendo-editor.decorator.html',
                builder: defaults,
                replace: false
            },
            'ticket-description': {
                template: base + 'ticket-description/ticket-description.decorator.html',
                builder: defaults,
                replace: false
            },
            'template-picker': {
                template: base + 'template-picker/template-picker.decorator.html',
                builder: defaults,
                replace: false
            },
            'kendo-image-upload': {
                template: base + 'kendo-image-upload/kendo-image-upload.decorator.html',
                builder: defaults,
                replace: false
            }
        }, []);
    }
})();

/****************************************
    FILE: src/components/web/browse-windows/users/fe-browse-users.controller.js
****************************************/

(function() {
    "use strict";
    
    FeBrowseUsersController.$inject = ["$scope", "$templateCache", "$timeout", "nspCoreService", "kendoWindowService"];
    angular
        .module('formEngine')
        .controller('FeBrowseUsersController', FeBrowseUsersController);

    /* @ngInject */
    function FeBrowseUsersController($scope, $templateCache, $timeout, nspCoreService, kendoWindowService) {
        var vm = this,
            dialogId = 'formEngineBrowseWindow',
            dialog = $('#' + dialogId).data('kendoWindow'),
            dialogData = kendoWindowService.read(dialogId),
            filters = dialogData.form.browse.initialFilters ? dialogData.form.browse.initialFilters : [];

        vm.selectedRow = null;

        if (dialogData.model) {
            vm.selectedRow = {
                id: dialogData.model[dialogData.form.kendoOptions.dataValueField],
                name: dialogData.model[dialogData.form.kendoOptions.dataTextField]
            };
        }

        if (dialogData.form.browse && dialogData.form.browse.filterCondition && dialogData.form.browse.filterCondition.filterValue) {
            filters.push({
                field: dialogData.form.browse.filterCondition.fieldName,
                operator: 'eq',
                value: dialogData.form.browse.filterCondition.filterValue
            });
        }

        vm.grid = {
            gridId: 'feBrowseUsersGrid',
            gridOptions: {
                dataSource: {
                    transport: {
                        read: {
                            url: '/Person/GetDataSource'
                        }
                    },
                    schema: {
                        model: {
                            fields: {
                                id: {
                                    type: "number",
                                    dbType: "integer"
                                },
                                name: {
                                    type: "string",
                                    dbType: "string"
                                },
                                imagePath: {
                                    type: "string",
                                    dbType: "string"
                                },
                                mail: {
                                    type: "string",
                                    dbType: "string"
                                },
                                organizationName: {
                                    type: "string",
                                    dbType: "string"
                                }
                            }
                        }
                    },
                    filter: filters,
                    pageSize: 5
                },
                columns: [{
                    field: 'id',
                    title: nspCoreService.translate('Id', 'Id'),
                    width: 120,
                    searchable: false,
                    hidden: true
                },
                {
                    field: 'name',
                    title: nspCoreService.translate('Agent', 'Common.Agent'),
                    searchable: true,
                    template: '#= data.name ? data.name : "-" #'
                    // template: $templateCache.get('/web/browse-windows/users/fe-browse-users-row.html')
                },
                {
                    field: 'organizationName',
                    title: nspCoreService.translate('Organization', 'Common.Organization'),
                    searchable: true,
                    template: '#= data.organizationName ? data.organizationName : "-"#'
                }],
                change: function(event) {
                    var grid = event.sender,
                        row = grid.select(),
                        rowData = grid.dataItem(row);
                    vm.selectedRow = rowData;
                    $timeout(function() {
                        $scope.$digest();
                    }, 0);
                },
                dataBound: function(event) {
                    var grid = event.sender,
                        items = grid.items();
                    items.each(function(idx, row) {
                        var dataItem = grid.dataItem(row);
                        if (vm.selectedRow && dataItem.id === vm.selectedRow.id) {
                            grid.select(row);
                        }
                    });
                }
            },
            searchOptions: {
                dataSource: {
                    transport: {
                        read: {
                            url: '/Person/GetAutoCompleteDataSource'
                        }
                    }
                }
            }
        };

        vm.t = nspCoreService.translate;
        vm.select = select;
        vm.cancel = cancel;

        function select() {
            var form = dialogData.form,
                formElement = $('#' + form.key[0]),
                formElementScope = formElement.scope(),
                dataValueField = formElementScope.settings.kendoOptions.dataValueField,
                dataTextField = formElementScope.settings.kendoOptions.dataTextField,
                dataItem = {};

            dataItem[dataValueField] = vm.selectedRow.id;
            dataItem[dataTextField] = vm.selectedRow.name;

            formElementScope.$broadcast('setValue', dataItem);
            dialog.close();
        }

        function cancel() {
            dialog.close();
        }
    }

})();

/****************************************
    FILE: src/components/web/template-picker/fe-template-picker-form.controller.js
****************************************/

(function() {
    'use strict';

    angular
        .module('formEngine')
        .controller('FeTemplatePickerFormController', FeTemplatePickerFormController);

    FeTemplatePickerFormController.$inject = ['$scope', '$rootScope', '$http', '$timeout', 'feService', 'kendoWindowService'];

    /* @ngInject */
    function FeTemplatePickerFormController($scope, $rootScope, $http, $timeout, feService, kendoWindowService) {
        var vm = this;
        vm.t = feService.translate;
        vm.dialogData = kendoWindowService.read('templatePickerWindow');
        vm.dialog = $('#templatePickerWindow').data('kendoWindow');
        vm.title = 'TemplatePickerForm';
        vm.activeLang = 0;
        vm.languages = vm.dialogData.model;

        var params = {
            parentPlaceholder: "",
            tableId: vm.dialogData.form.window.tableId,
            typeId: vm.dialogData.form.window.entityTypeId,
            tableRepository: "app.Tables"
        };

        var treeOptions = {
            dataTextField: "name",
            dataValueField: "id",
            loadOnDemand: true,
            dataSource: new kendo.data.HierarchicalDataSource({
                transport: {
                    read: {
                        url: vm.dialogData.form.window.placeholdersUrl,
                        dataType: "json",
                        data: params
                    },
                    parameterMap: function(options, type) {
                        options.parentPlaceholder = params.parentPlaceholder;
                        options.tableId = params.tableId;
                        options.typeId = params.typeId;
                        options.tableRepository = params.tableRepository;
                        return options;
                    }
                },
                schema: {
                    model: {
                        id: "id",
                        hasChildren: "hasSubPlaceholders"
                    }
                }
            }),
            expand: function(event){
                params.parentPlaceholder = this.dataItem(event.node).placeholder;
                params.tableId = this.dataItem(event.node).expandTableId;
                params.tableRepository = this.dataItem(event.node).expandRepository;
            }
        };

        $scope.treeOptions = treeOptions;

        $scope.selectTreeViewItem = function(event) {
            if (vm.activeLang === null) {
                return;
            }
            var dataItem = event.sender.dataItem(event.sender.select());
            var treeView = event.sender;            
            vm.languages[vm.activeLang].subject += dataItem.placeholder;
        };

        $scope.setActiveLang = function(index) {
            vm.activeLang = index === vm.activeLang ? null : index;
        };

        $scope.openLanguageList = function(){
            vm.langListOpened = true;
            $http({
                method: 'GET',
                url: vm.dialogData.form.window.languageListUrl ? vm.dialogData.form.window.languageListUrl : '/LanguageType/GetAllLanguagesExceptDefault',                
                headers: {
                    'Content-Type': 'application/json; charset=utf8'
                }
            }).success(function(response) {
                vm.langDefinitionList = response;
            });
        };

        $scope.addLanguageDefinition = function(language){
            for (var i = 0; i < vm.languages.length; i++) {
                if(language.id === vm.languages[i].languageId){
                    return;
                }
            }            
            vm.languages.push({
                languageId: language.id,
                languageName: language.languageName,
                languageNameKey: language.languageNameKey,
                subject: ""
            });
            vm.langListOpened = false;
        };

        $scope.removeLanguage = function(index){
            vm.languages.splice(index, 1);
        };

        function collapseDropDown(event) {
            if (vm.langListOpened && $(event.target).closest('.lang-list').length === 0 && $(event.target).closest('.add-lang-definition .label').length === 0) {
                vm.langListOpened = false;
                $timeout(function() {
                    $scope.$digest();
                }, 0);
            }
        }       

        $scope.cancel = function() {
            vm.dialog.close();
        };

        $scope.saveTemplateForm = function() {
            vm.dialogData.model = vm.languages;
            $rootScope.$broadcast('templateSet', vm.dialogData.model);
            $scope.cancel();
        };

        $(document).on('click', collapseDropDown);
        $scope.$on('$destroy', function() {
            $(document).off('click', collapseDropDown);
        });
    }
})();